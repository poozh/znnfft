#include "znn/asm/avx512_complex_with_scatter.hpp"
#include "znn/intrin.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
#include "znn/util/kernel_launcher.hpp"
#include <chrono>
#include <iostream>
#include <thread>
#include <type_traits>

static const int S = 4;

using namespace znn;
using namespace znn::fft;

inline double bench_gemm(long_t M, long_t N, long_t K, long_t mats, bool apf,
                         bool bpf)
{
    auto gemm = avx512::get_znn_cgemm(M, N, K, N * 2, K * 2, K * 2, 1, 1, 0, 1,
                                      apf, bpf);
    long_t           offset = 1;
    hbw_array<float> a(M * N * mats * offset * 2);
    hbw_array<float> b(N * K * 2);
    hbw_array<float> c(M * K * mats * offset * 2);

    long_t iters = 50;

    for (long_t i = 0; i < iters; ++i)
    {
        for (long_t j = 0; j < mats; ++j)
        {
            gemm(a.data() + M * N * j * offset * 2, b.data(),
                 c.data() + M * K * j * offset * 2,
                 a.data() + M * N * (j + 1) * offset * 2, nullptr,
                 c.data() + M * K * (j + 1) * offset * 2, nullptr);
        }
    }

    auto begin = std::chrono::high_resolution_clock::now();

    for (long_t i = 0; i < iters; ++i)
    {
        for (long_t j = 0; j < mats; ++j)
        {
            gemm(a.data() + M * N * j * offset * 2, b.data(),
                 c.data() + M * K * j * offset * 2,
                 a.data() + M * N * (j + 1) * offset * 2, nullptr,
                 c.data() + M * K * (j + 1) * offset * 2, nullptr);
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
            .count();

    double gflops = M * N * K * 2 * 4;
    gflops /= 1000000000;

    double secs  = static_cast<double>(duration) / 1000000;
    double FLOPS = (gflops * iters * mats / secs);
    // std::cout << M << ' ' << N << ' ' << K << ' ' << mats << ' ';
    // std::cout << "GFLOPS: " << FLOPS << std::endl;
    return FLOPS;
}

int main()
{
    for (long_t N = 96; N <= 256; N += 32)
    {
        for (long_t K = 96; K <= 256; K += 32)
        {
            if (N * K <= 128 * 128)
            {
                double t = 0.0;
                for (long_t M = 6; M <= 32; ++M)
                {
                    t = std::max(t, bench_gemm(M, N, K, 100, false, false));
                    t = std::max(t, bench_gemm(M, N, K, 100, false, true));
                    t = std::max(t, bench_gemm(M, N, K, 100, true, false));
                    t = std::max(t, bench_gemm(M, N, K, 100, true, true));
                    std::cout << " IF " << N << " OF " << K << " max flops is "
                              << t << std::endl;
                }
            }
        }
    }
}
