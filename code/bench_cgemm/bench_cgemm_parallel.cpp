#include "znn/asm/avx512_complex_with_scatter.hpp"
#include "znn/intrin.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
#include "znn/util/kernel_launcher.hpp"
#include <chrono>
#include <iostream>
#include <thread>
#include <type_traits>

static const int S = 4;

using namespace znn;
using namespace znn::fft;

inline double bench_gemm(long_t M, long_t N, long_t K, long_t mats, bool apf,
                         bool bpf, bool& HT)

{
    auto gemm = avx512::get_znn_cgemm(M, N, K, N * 2, K * 2, K * 2, 1, 1, 0, 1,
                                      apf, bpf);
    long_t           offset = 6;
    hbw_array<float> a((M * N * mats + 16) * offset * 2);
    hbw_array<float> b(2 * ZNN_NUM_CORES * N * K * 2);
    hbw_array<float> c(2 * ZNN_NUM_CORES * (M * K * mats + 16) * offset * 2);

    kernel_launcher launcher(ZNN_NUM_CORES, 2);

    std::vector<std::function<void()>> fns(ZNN_NUM_CORES * 2);

    for (long_t i = 0; i < 20; ++i)
    {
        fns[i] = [
            &, a = a.data(), b = b.data() + i * N * K,
            c = c.data() + i * (M * K * mats + 16) * offset
    ]()
        {
            for (long_t j = 0; j < mats; ++j)
            {
                gemm(a + (M * N * j + 16) * offset * 2, b,
                     c + (M * K * j + 16) * offset * 2,
                     a + (M * N * (j + 1) + 16) * offset * 2, nullptr,
                     c + (M * K * (j + 1) + 16) * offset * 2, nullptr);
            }
        };
    }

    long_t iters = 30;

    for (long_t i = 0; i < iters; ++i)
    {
        launcher.launch2<false>(&(fns[0]));
    }

    auto begin = std::chrono::high_resolution_clock::now();

    for (long_t i = 0; i < iters; ++i)
    {
        launcher.launch2<false>(&(fns[0]));
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
            .count();

    double gflops = M * N * K * 2 * ZNN_NUM_CORES * 4;
    gflops /= 1000000000;

    double secs = static_cast<double>(duration) / 1000000;

    double OneTflops = (gflops * iters * mats / secs);
    double TwoTflops;

    {
        for (long_t i = 0; i < iters; ++i)
        {
            launcher.launch2<true>(&(fns[0]));
        }

        auto begin = std::chrono::high_resolution_clock::now();

        for (long_t i = 0; i < iters; ++i)
        {
            launcher.launch2<true>(&(fns[0]));
        }

        auto end = std::chrono::high_resolution_clock::now();
        auto duration =
            std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
                .count();

        double gflops = M * N * K * 2 * ZNN_NUM_CORES * 2 * 4;
        gflops /= 1000000000;

        double secs = static_cast<double>(duration) / 1000000;
        TwoTflops   = (gflops * iters * mats / secs);
    }

    std::cout << M << ' ' << N << ' ' << K << ' ' << mats << ' ';
    std::cout << "GFLOPS: " << OneTflops << " :: " << TwoTflops << std::endl;

    if (OneTflops > TwoTflops)
    {
        HT = false;
        return OneTflops;
    }
    else
    {
        HT = true;
        return TwoTflops;
    }
}

int main()
{
    for (long_t N = 128; N <= 256; N += 32)
    {
        for (long_t K = 128; K <= 256; K += 32)
        {
            if (N * K <= 256 * 256)
            {
                double Flops = 0;
                long_t maxM  = 0;
                bool   HT    = false;
                bool   tmpHT = false;
                bool   Apre  = false;
                bool   Bpre  = false;
                for (long_t M = 6; M <= 32; ++M)
                {
                    double tmp = bench_gemm(M, N, K, 150, false, false, tmpHT);
                    if (Flops < tmp)
                    {
                        Flops = tmp;
                        HT    = tmpHT;
                        maxM  = M;
                        Apre  = false;
                        Bpre  = false;
                    }

                    tmp = bench_gemm(M, N, K, 150, true, false, tmpHT);
                    if (Flops < tmp)
                    {
                        Flops = tmp;
                        HT    = tmpHT;
                        maxM  = M;
                        Apre  = true;
                        Bpre  = false;
                    }

                    tmp = bench_gemm(M, N, K, 150, true, true, tmpHT);
                    if (Flops < tmp)
                    {
                        Flops = tmp;
                        HT    = tmpHT;
                        maxM  = M;
                        Apre  = true;
                        Bpre  = true;
                    }

                    tmp = bench_gemm(M, N, K, 150, false, true, tmpHT);
                    if (Flops < tmp)
                    {
                        Flops = tmp;
                        HT    = tmpHT;
                        maxM  = M;
                        Apre  = false;
                        Bpre  = true;
                    }
                }
                std::cout << " N: " << N << " K: " << K
                          << " max GFLOPS: " << Flops << " M is " << maxM
                          << " HT is " << HT << " A prefetch is " << Apre
                          << " B prefetch is " << Bpre << std::endl;
            }
        }
    }
}
