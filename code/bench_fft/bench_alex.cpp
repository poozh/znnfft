#include "znn/fft/bench.hpp"

using namespace znn::fft;

int main()
{
    do_bench<48, 64, 192, 384, 1, 15, 15, vek<1, 15, 15>, vek<1, 3, 3>, true>();
}
