#include "znn/fft/bench.hpp"

using namespace znn::fft;

int main()
{
    using M = vek<1, 14, 14>;
    using K = vek<1, 3, 3>;

    do_bench<10, 128, 96, 256, 1, 36, 36, M, vek<1, 7, 7>, true>("OverFeat 2");
    do_bench<10, 128, 256, 512, 1, 17, 17, M, K, true>("OverFeat 3");
    do_bench<10, 128, 512, 1024, 1, 17, 17, M, K, true>("OverFeat 4");
    do_bench<10, 128, 1024, 1024, 1, 17, 17, M, K, true>("OverFeat 5");

    do_bench<10, 64, 64, 192, 1, 59, 59, M, vek<1, 5, 5>, true>("AlexNet 2");
    do_bench<10, 64, 192, 384, 1, 15, 15, M, K, true>("AlexNet 3");
    do_bench<10, 64, 384, 256, 1, 15, 15, M, K, true>("AlexNet 4");
    do_bench<10, 64, 256, 256, 1, 15, 15, M, K, true>("AlexNet 5");
}
