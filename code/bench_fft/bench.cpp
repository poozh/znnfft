#include "znn/fft/bench.hpp"

using namespace znn::fft;

int main()
{
    // do_bench<10, 64, 64, 64, 1, 226, 226, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 1.2");

    // do_bench<10, 64, 64, 128, 1, 114, 114, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 2.1");

    // do_bench<10, 64, 128, 128, 1, 114, 114, vek<1, 8, 8>, vek<1, 3, 3>,
    // true>(
    //     "VGG 2.2");

    // do_bench<10, 64, 128, 256, 1, 58, 58, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 3.1");

    // do_bench<10, 64, 256, 256, 1, 58, 58, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 3.2");

    // do_bench<10, 64, 256, 512, 1, 30, 30, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 4.1");

    // do_bench<10, 64, 512, 512, 1, 30, 30, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 4.2");

    // do_bench<10, 256, 192, 384, 1, 16, 16, vek<1, 16, 16>, vek<1, 3, 3>,
    // true>(
    //     "AlexNet 3 x");

    // do_bench<10, 256, 96, 256, 1, 30, 30, vek<1, 17, 17>, vek<1, 5, 5>,
    // true>(
    //     "AlexNet 4 x");

    do_bench<10, 64, 64, 128, 30, 30, 30, vek<16, 16, 30>, vek<3, 3, 3>,
             false>();
    // do_bench<10, 64, 64, 128, 30, 30, 30, vek<30, 30, 30>, vek<3, 3, 3>,
    //          false>();

    // do_bench<10, 64, 128, 256, 14, 14, 14, vek<14, 14, 14>, vek<3, 3, 3>,
    //          true>();

    // do_bench<10, 256, 96, 256, 1, 32, 32, vek<1, 32, 32>, vek<1, 5, 5>,
    // true>(
    //     "AlexNet 4 x");

    // do_bench<10, 256, 96, 256, 1, 30, 30, vek<1, 30, 30>, vek<1, 5, 5>,
    // true>(
    //     "AlexNet 4 x");

    // do_bench<10, 256, 256, 256, 1, 16, 16, vek<1, 16, 16>, vek<1, 3, 3>,
    // true>(
    //     "AlexNet 4 x");

    // do_bench<10, 128, 256, 512, 1, 17, 17, vek<1, 17, 17>, vek<1, 3, 3>,
    // true>();

    // do_bench<10, 64, 64, 128, 1, 114, 114, vek<1, 21, 21>, vek<1, 3, 3>,
    //          true>();

    // do_bench<ZNN_NUM_CORES, 64, 256, 256, 1, 58, 58, vek<1, 30, 30>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 64, 512, 512, 1, 16, 16, vek<1, 16, 16>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 128, 256, 512, 1, 14, 14, vek<1, 14, 14>,
    //          vek<1, 3, 3>, true>();

    // do_bench<10, 64, 256, 256, 1, 58, 58, vek<1, 30, 30>,
    //         vek<1, 3, 3>, true>();

    // do_bench<10, 64, 256, 512, 1, 30, 30, vek<1, 30, 30>,
    //          vek<1, 3, 3>, true>();

    // do_bench<10, 64, 256, 512, 1, 30, 30, vek<1, 16, 16>,
    //          vek<1, 3, 3>, true>();

    // do_bench<10, 64, 512, 512, 1, 30, 30, vek<1, 30, 30>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 128, 512, 512, 1, 17, 17, vek<1, 17, 17>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 128, 1024, 1024, 1, 14, 14, vek<1, 14, 14>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, vek<1, 12, 12>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 1, 1024, 1024, 1, 40, 40, vek<1, 8, 5>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 64, 512, 512, 1, 16, 16, vek<1, 16, 16>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 64, 128, 128, 1, 114, 114, vek<1, 16, 30>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 64, 256, 256, 1, 58, 58, vek<1, 30, 30>,
    //          vek<1, 3, 3>, true>();

    // do_bench<ZNN_NUM_CORES, 64, 512, 512, 1, 30, 30, vek<1, 30, 30>,
    //          vek<1, 3, 3>, true>();

    // std::cout << "Now without Kernels" << std::endl;

    // do_bench<64, 64, 256, 256, 1, 58, 58, vek<1, 16, 16>, vek<1, 3, 3>,
    //          false>();

    // do_bench<64, 64, 256, 256, 1, 58, 58, vek<1, 9, 9>, vek<1, 3, 3>,
    // true>();

    // std::cout << "Now without Kernels" << std::endl;

    // do_bench<64, 64, 256, 256, 1, 58, 58, vek<1, 9, 9>, vek<1, 3, 3>,
    // false>();
}
