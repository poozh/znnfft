#include "znn/fft/bench.hpp"

using namespace znn::fft;

int main()
{
    do_bench<48, 64, 64, 64, 1, 226, 226, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 1.2");

    do_bench<48, 64, 64, 128, 1, 114, 114, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 2.1");

    do_bench<48, 64, 128, 128, 1, 114, 114, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 2.2");

    do_bench<48, 64, 128, 256, 1, 58, 58, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 3.1");

    do_bench<48, 64, 256, 256, 1, 58, 58, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 3.2");

    do_bench<48, 64, 256, 512, 1, 30, 30, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 4.1");

    do_bench<48, 64, 512, 512, 1, 30, 30, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 4.2");

    do_bench<48, 64, 512, 512, 1, 16, 16, vek<1, 12, 12>, vek<1, 3, 3>, true>(
        "VGG 5");
}
