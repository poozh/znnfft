#include "znn/fft/bench.hpp"

using namespace znn::fft;

int main()
{
    // do_bench<10, 64, 64, 64, 1, 226, 226, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 1.2");

    // do_bench<10, 64, 64, 128, 1, 114, 114, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 2.1");

    // do_bench<10, 64, 128, 128, 1, 114, 114, vek<1, 8, 8>, vek<1, 3, 3>,
    // true>(
    //     "VGG 2.2");

    // do_bench<10, 64, 128, 256, 1, 58, 58, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 3.1");

    // do_bench<10, 64, 256, 256, 1, 58, 58, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 3.2");

    // do_bench<10, 64, 256, 512, 1, 30, 30, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 4.1");

    // do_bench<10, 64, 512, 512, 1, 30, 30, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 4.2");

    // do_bench<10, 64, 512, 512, 1, 16, 16, vek<1, 8, 8>, vek<1, 3, 3>, true>(
    //     "VGG 5");

    using M = vek<1, 8, 8>;
    using K = vek<1, 3, 3>;

    do_bench<10, 128, 96, 256, 1, 36, 36, M, N, true>("OverFeat 2");
    do_bench<10, 128, 256, 512, 1, 17, 17, M, N, true>("OverFeat 3");
    do_bench<10, 128, 512, 1024, 1, 17, 17, M, N, true>("OverFeat 4");
    do_bench<10, 128, 1024, 1024, 1, 17, 17, M, N, true>("OverFeat 5");

    do_bench<10, 64, 64, 192, 1, 59, 59, M, N, true>("AlexNet 2");
    do_bench<10, 64, 192, 384, 1, 15, 15, M, N, true>("AlexNet 3");
    do_bench<10, 64, 384, 256, 1, 15, 15, M, N, true>("AlexNet 4");
    do_bench<10, 64, 256, 256, 1, 15, 15, M, N, true>("AlexNet 5");
}
