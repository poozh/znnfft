#include "znn/asm/avx512_complex_with_scatter.hpp"
#include "znn/intrin.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
#include <chrono>
#include <complex>
#include <iostream>
#include <thread>
#include <type_traits>

static const int S = 4;

using namespace znn;
using namespace znn::fft;

inline void naive_cgemm(long_t M, long_t N, long_t K, long_t LDA, long_t LDB,
                        long_t LDC, float const* a, float const* b, float* c)
{
    for (long_t m = 0; m < M; ++m)
    {
        for (long_t k = 0; k < K; ++k)
        {
            for (long_t n = 0; n < N; ++n)
            {
                long_t nr =
                    (n / SIMD_WIDTH) * (SIMD_WIDTH * 2) + (n % SIMD_WIDTH);
                long_t ni = nr + SIMD_WIDTH;
                long_t kr =
                    (k / SIMD_WIDTH) * (SIMD_WIDTH * 2) + (k % SIMD_WIDTH);
                long_t ki = kr + SIMD_WIDTH;

                c[m * LDC + nr] += a[m * LDA + kr] * b[k * LDB + nr];
                c[m * LDC + nr] -= a[m * LDA + ki] * b[k * LDB + ni];

                c[m * LDC + ni] += a[m * LDA + ki] * b[k * LDB + nr];
                c[m * LDC + ni] += a[m * LDA + kr] * b[k * LDB + ni];
            }
        }
    }
}

void verify_result(float* c1, float* c2, long_t len)
{
    float maxx = 0.f;
    float maxy = 0.f;

    for (long_t i = 0; i < len; i++)
    {
        maxx = std::max(std::abs(c1[i] - c2[i]), maxx);
        maxy = std::max(std::max(std::abs(c1[i]), std::abs(c2[i])), maxy);
        // if (c1[i] != c2[i])
        {
            //  printf("wrong --------c1 %d is %f, c2 is %f\n", i, c1[i],
            //  c2[i]);
        }
    }

    std::cout << "MAX DIFF: " << maxx << "\n";
    std::cout << "MAX NUMB: " << maxy << "\n";
}

inline void print_cgemm(long_t R, long_t C, long_t LD, float* d)
{
    for (long_t r = 0; r < R; ++r)
    {
        for (long_t c = 0; c < C; ++c)
        {
            std::cout << std::complex<float>(
                             d[r * LD + (c / SIMD_WIDTH) * (SIMD_WIDTH * 2) +
                               (c % SIMD_WIDTH)],
                             d[r * LD + (c / SIMD_WIDTH) * (SIMD_WIDTH * 2) +
                               (c % SIMD_WIDTH) + SIMD_WIDTH])
                      << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

inline void test_gemm(long_t M, long_t N, long_t K)
{
    auto cgemm =
        avx512::get_znn_cgemm(M, N, K, N * 2, K * 2, K * 2, 1, 1, 0, 1, 1, 1);

    hbw_array<float> a(rand_init, M * N * 2);
    hbw_array<float> b(rand_init, N * K * 2);
    hbw_array<float> c(zero_init, M * K * 2);

    hbw_array<float> c2(zero_init, M * K * 2);

    std::cout << "Test for: " << M << ' ' << N << ' ' << K << "\n";
    naive_cgemm(M, K, N, N * 2, K * 2, K * 2, a.data(), b.data(), c2.data());

    // print_cgemm(M, K, K * 2, c2.data());
    // verify_result(c.data(), c2.data(), K * M * 2);

    cgemm(a.data(), b.data(), c.data(), a.data(), b.data(), c.data(), nullptr);

    // print_cgemm(M, K, K * 2, c.data());

    // verify_result(c.data(), c2.data(), K * M * 2);

    verify_result(c.data(), c2.data(), K * M * 2);
}

int main()
{
    // test_gemm(2, 16, 16);
    for (long_t N = 96; N <= 256; N += 32)
    {
        for (long_t K = 96; K <= 256; K += 32)
        {
            if (N * K <= 128 * 128)
            {
                for (long_t M = 6; M <= 32; ++M)
                {
                    test_gemm(M, N, K);
                }
            }
        }
    }
}
