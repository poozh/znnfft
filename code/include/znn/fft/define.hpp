#ifdef ZNN_FFT_PROPAGATION_HEADER
#error "ZNN_FFT_PROPAGATION_HEADER already defined"
#endif

#ifdef ZNN_FFT_NAMESPACE
#error "ZNN_FFT_NAMESPACE already defined"
#endif

#define ZNN_FFT_PROPAGATION_HEADER "znn/fft/propagation.hpp"
#define ZNN_FFT_NAMESPACE znn::fft
#define ZNN_FFT_BYTES_PER_NUMBER 8
