#pragma once

#include "znn/intrin.hpp"
#include "znn/types.hpp"

namespace znn
{
namespace fft
{

template <long_t B, long_t C, long_t D, long_t H, long_t W>
struct tensor_size
{
    static constexpr long_t b = B;
    static constexpr long_t c = C;
    static constexpr long_t d = D;
    static constexpr long_t h = H;
    static constexpr long_t w = W;

    static constexpr long_t batch   = B;
    static constexpr long_t channel = C;
    static constexpr long_t depth   = D;
    static constexpr long_t height  = H;
    static constexpr long_t width   = W;
};

template <long_t D, long_t H, long_t W>
struct size3d
{
    static constexpr long_t d = D;
    static constexpr long_t h = H;
    static constexpr long_t w = W;
};

template <class Tiles>
struct in_mat_shape
{
    static constexpr long_t width  = Tiles::c;
    static constexpr long_t height = Tiles::b * Tiles::d * Tiles::h * Tiles::w;
};

// template <
}
} // namespace znn:phi

namespace znn
{
namespace phi
{

template <long_t N, long_t IS, long_t OS>
struct iodim_t
{
    static const long_t n  = N;
    static const long_t is = IS;
    static const long_t os = OS;
};

template <long_t N, long_t S>
struct dim_t
{
    static const long_t n = N;
    static const long_t s = S;
};
}
} // namespace znn:phi
