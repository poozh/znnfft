#pragma once

#include "znn/intrin.hpp"
#include "znn/types.hpp"
#include <type_traits>

namespace znn::fft_codelets
{

namespace detail_v
{

template <typename T>
inline void SIMD_MUL_D(T* out, const T* __restrict__ in, T n)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i] * n;
    }
}

template <typename T>
inline void SIMD_FMADD_D(T* out, T n, const T* __restrict__ in, T* in2)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i] * n + in2[i];
    }
}

template <typename T>
inline void SIMD_FMSUB_D(T* out, T n, const T* __restrict__ in, T* in2)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i] * n - in2[i];
    }
}

template <typename T>
inline void SIMD_SUB_D(T* out, const T* __restrict__ in, T* in2)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i] - in2[i];
    }
}

template <typename T>
inline void SIMD_ADD_D(T* out, const T* __restrict__ in, T* in2)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i] + in2[i];
    }
}

template <typename T>
inline void SIMD_FNMADD_D(T* out, T n, const T* __restrict__ in, T* in2)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in2[i] - n * in[i];
    }
}

template <typename T>
inline void equal(T* out, const T* in)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = in[i];
    }
}

template <typename T>
inline void nequal(T* out, const T* __restrict__ in)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = -in[i];
    }
}

template <long_t, long_t>
struct r2cf_traits;
template <long_t, long_t>
struct r2cb_traits;
template <long_t, long_t>
struct c2cf_traits;
template <long_t, long_t>
struct c2cb_traits;

#define DK(name, val) SIMD_FLOAT name = SIMD_SET1(val);
#define WS(a, b) ((a) * (b))

#include "detail_v/c2cf_16_v.hpp"
#include "detail_v/c2cf_32_v.hpp"
#include "detail_v/r2cf_16_v.hpp"
#include "detail_v/r2cf_32_v.hpp"

} // namespace detail

template <typename T>
inline void assign(T* out, T n)
{
    for (long_t i = 0; i < 16; i++)
    {
        out[i] = n;
    }
}

template <long_t TransformSize, long_t ProvidedElements, long_t rs, long_t cs,
          typename T>
inline __attribute__((always_inline)) void
r2cf_helper(T const* __restrict R0, T* __restrict Cr, T* __restrict Ci)
{
    detail_v::r2cf<TransformSize, ProvidedElements, rs, cs, T>(R0, Cr, Ci);

    if constexpr (ProvidedElements == 1)
    {
        static constexpr long_t to_fill = (TransformSize / 2) + 1;
#pragma unroll(to_fill)
        for (long_t i = 0; i < to_fill; ++i)
        {
            assign<T>(Ci + WS(cs, i) * SIMD_WIDTH, 0.0);
        }
    }
    else
    {
        assign<T>(Ci, 0.0);
        if constexpr ((TransformSize % 2) == 0)
        {
            assign<T>(Ci + WS(cs, TransformSize / 2) * SIMD_WIDTH, 0.0);
        }
    }
}

template <long_t TransformSize, long_t ProvidedElements, long_t rs, long_t cs,
          typename T>
inline __attribute__((always_inline)) void r2cf(T const* __restrict R,
                                                T* __restrict C)
{
    r2cf_helper<TransformSize, ProvidedElements, rs, cs, T>(R, C,
                                                            C + 1 * SIMD_WIDTH);
}

template <long_t TransformSize, long_t ProvidedElements, long_t ios, typename T>
inline __attribute__((always_inline)) void c2cf(T* __restrict d)
{
    detail_v::c2cf<TransformSize, ProvidedElements, ios, ios, T>(
        d, d + 1 * SIMD_WIDTH, d, d + 1 * SIMD_WIDTH);
}

#undef DK
#undef WS

} // namespace znn::fft_codelets
