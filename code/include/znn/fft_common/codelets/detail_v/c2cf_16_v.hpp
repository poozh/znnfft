#pragma once

template <>
struct c2cf_traits<16, 3>
{
    static constexpr long_t flops           = 92;
    static constexpr long_t operations      = 56;
    static constexpr long_t memory_accesses = 38;
    static constexpr long_t stack_vars      = 33;
    static constexpr long_t constants       = 3;
};

template <long_t TransformSize, long_t ProvidedElements, long_t is, long_t os,
          typename T>
inline __attribute__((always_inline))
typename std::enable_if<TransformSize == 16 && ProvidedElements == 3>::type
c2cf(T const* __restrict ri, T const* __restrict ii, T* __restrict ro,
     T* __restrict io)
{
    T KP923879532= +0.923879532511286756128183189396788286822416626;
    T KP707106781= +0.707106781186547524400844362104849039284835938;
    T KP414213562= +0.414213562373095048801688724209698078569671875;
    {
        T T4[SIMD_WIDTH], T9[SIMD_WIDTH], T1[SIMD_WIDTH], T5[SIMD_WIDTH],
            T2[SIMD_WIDTH], T6[SIMD_WIDTH], Td[SIMD_WIDTH], Th[SIMD_WIDTH],
            Tt[SIMD_WIDTH], Tq[SIMD_WIDTH], Tn[SIMD_WIDTH], Tk[SIMD_WIDTH],
            Tm[SIMD_WIDTH], Tj[SIMD_WIDTH], Ta[SIMD_WIDTH];
        T T3[SIMD_WIDTH], Te[SIMD_WIDTH], Tb[SIMD_WIDTH], To[SIMD_WIDTH],
            Tl[SIMD_WIDTH], T7[SIMD_WIDTH], T8[SIMD_WIDTH], Tu[SIMD_WIDTH],
            Ti[SIMD_WIDTH], Tc[SIMD_WIDTH], Tp[SIMD_WIDTH], Tg[SIMD_WIDTH],
            Ts[SIMD_WIDTH], Tr[SIMD_WIDTH], Tf[SIMD_WIDTH];

        equal<T>(T4, ri + WS(is, 1) * SIMD_WIDTH);
        equal<T>(T9, ii + WS(is, 1) * SIMD_WIDTH);
        equal<T>(T1, ri);
        equal<T>(T5, ii);
        equal<T>(T2, ri + WS(is, 2) * SIMD_WIDTH);
        equal<T>(T6, ii + WS(is, 2) * SIMD_WIDTH);
        SIMD_FMADD_D<T>(Td, KP414213562, T9, T4);
        SIMD_FMSUB_D<T>(Th, KP414213562, T9, T4);
        SIMD_FMADD_D<T>(Tt, KP414213562, T4, T9);
        SIMD_FNMADD_D<T>(Tq, KP414213562, T4, T9);

       // T4 = ri[WS(is, 1)];
       // T9 = ii[WS(is, 1)];
       // T1 = ri[0];
       // T5 = ii[0];
       // T2 = ri[WS(is, 2)];
       // T6 = ii[WS(is, 2)];
       // Td = SIMD_FMADD(KP414213562, T9, T4);
       // Th = SIMD_FMSUB(KP414213562, T9, T4);
       // Tt = SIMD_FMADD(KP414213562, T4, T9);
       // Tq = SIMD_FNMADD(KP414213562, T4, T9);
        SIMD_SUB_D<T>(Tn, T9, T4);
        SIMD_ADD_D<T>(Tk, T4, T9);
        SIMD_SUB_D<T>(Tm, T5, T2);
        SIMD_ADD_D<T>(Tj, T2, T5);
        SIMD_SUB_D<T>(Ta, T1, T2);
        SIMD_ADD_D<T>(T3, T1, T2);
        SIMD_ADD_D<T>(Te, T2, T6);
        SIMD_SUB_D<T>(Tb, T6, T2);
        SIMD_SUB_D<T>(To, T1, T6);
        SIMD_ADD_D<T>(Tl, T1, T6);
        SIMD_SUB_D<T>(T7, T5, T6);
        SIMD_ADD_D<T>(T8, T5, T6);

       // Tn = T9 - T4;
       // Tk = T4 + T9;
       // Tm = T5 - T2;
       // Tj = T2 + T5;
       // Ta = T1 - T2;
       // T3 = T1 + T2;
       // Te = T2 + T6;
       // Tb = T6 - T2;
       // To = T1 - T6;
       // Tl = T1 + T6;
       // T7 = T5 - T6;
       // T8 = T5 + T6;

        SIMD_FMADD_D<T>(io + WS(os, 14) * SIMD_WIDTH, KP707106781, Tk, Tj);
        SIMD_FNMADD_D<T>(io + WS(os, 6) * SIMD_WIDTH, KP707106781, Tk, Tj);
        SIMD_SUB_D<T>(ro + WS(os, 12) * SIMD_WIDTH, Ta, T9);
        SIMD_ADD_D<T>(ro + WS(os, 4) * SIMD_WIDTH, Ta, T9);
        SIMD_FMADD_D<T>(io + WS(os, 2) * SIMD_WIDTH, KP707106781, Tn, Tm);
        SIMD_FNMADD_D<T>(io + WS(os, 10) * SIMD_WIDTH, KP707106781, Tn, Tm);
        SIMD_ADD_D<T>(ro, T3, T4);
        SIMD_SUB_D<T>(ro + WS(os, 8) * SIMD_WIDTH, T3, T4);

   //     io[WS(os, 14)] = SIMD_FMADD(KP707106781, Tk, Tj);
   //     io[WS(os, 6)]  = SIMD_FNMADD(KP707106781, Tk, Tj);
   //     ro[WS(os, 12)] = Ta - T9;
   //     ro[WS(os, 4)]  = Ta + T9;
   //     io[WS(os, 2)]  = SIMD_FMADD(KP707106781, Tn, Tm);
   //     io[WS(os, 10)] = SIMD_FNMADD(KP707106781, Tn, Tm);
   //     ro[0] = T3 + T4;
   //     ro[WS(os, 8)] = T3 - T4;

        SIMD_FMADD_D<T>(Tu, KP707106781, Tb, T1);
        SIMD_FNMADD_D<T>(Ti, KP707106781, Tb, T1);
        SIMD_FNMADD_D<T>(Tc, KP707106781, Tb, T5);
        SIMD_FMADD_D<T>(Tp, KP707106781, Tb, T5);
        SIMD_FNMADD_D<T>(Tg, KP707106781, Te, T5);
        SIMD_FMADD_D<T>(Ts, KP707106781, Te, T5);
        SIMD_FNMADD_D<T>(Tr, KP707106781, Te, T1);
        SIMD_FMADD_D<T>(Tf, KP707106781, Te, T1);

       // Tu = SIMD_FMADD(KP707106781, Tb, T1);
       // Ti = SIMD_FNMADD(KP707106781, Tb, T1);
       // Tc = SIMD_FNMADD(KP707106781, Tb, T5);
       // Tp = SIMD_FMADD(KP707106781, Tb, T5);
       // Tg = SIMD_FNMADD(KP707106781, Te, T5);
       // Ts = SIMD_FMADD(KP707106781, Te, T5);
       // Tr = SIMD_FNMADD(KP707106781, Te, T1);
       // Tf = SIMD_FMADD(KP707106781, Te, T1);

        SIMD_FMADD_D<T>(ro + WS(os, 2) * SIMD_WIDTH, KP707106781, Tk, Tl);
        SIMD_FNMADD_D<T>(ro + WS(os, 10) * SIMD_WIDTH, KP707106781, Tk, Tl);
        SIMD_ADD_D<T>(io + WS(os, 12) * SIMD_WIDTH, T4, T7);
        SIMD_SUB_D<T>(io + WS(os, 4) * SIMD_WIDTH, T7, T4);
        SIMD_FNMADD_D<T>(ro + WS(os, 14) * SIMD_WIDTH, KP707106781, Tn, To);
        SIMD_FMADD_D<T>(ro + WS(os, 6) * SIMD_WIDTH, KP707106781, Tn, To);
        SIMD_ADD_D<T>(io, T8, T9);
        SIMD_SUB_D<T>(io + WS(os, 8) * SIMD_WIDTH, T8, T9);

       // ro[WS(os, 2)]  = SIMD_FMADD(KP707106781, Tk, Tl);
       // ro[WS(os, 10)] = SIMD_FNMADD(KP707106781, Tk, Tl);
       // io[WS(os, 12)] = T4 + T7;
       // io[WS(os, 4)]  = T7 - T4;
       // ro[WS(os, 14)] = SIMD_FNMADD(KP707106781, Tn, To);
       // ro[WS(os, 6)]  = SIMD_FMADD(KP707106781, Tn, To);
       // io[0] = T8 + T9;
       // io[WS(os, 8)]  = T8 - T9;

        SIMD_FMADD_D<T>(ro + WS(os, 7) * SIMD_WIDTH, KP923879532, Th, Ti);
        SIMD_FNMADD_D<T>(ro + WS(os, 15) * SIMD_WIDTH, KP923879532, Th, Ti);
        SIMD_FMADD_D<T>(ro + WS(os, 3) * SIMD_WIDTH, KP923879532, Tt, Tu);
        SIMD_FNMADD_D<T>(ro + WS(os, 11) * SIMD_WIDTH, KP923879532, Tt, Tu);
        SIMD_FMADD_D<T>(io + WS(os, 1) * SIMD_WIDTH, KP923879532, Tq, Tp);
        SIMD_FNMADD_D<T>(io + WS(os, 9) * SIMD_WIDTH, KP923879532, Tq, Tp);
        SIMD_FMADD_D<T>(io + WS(os, 13) * SIMD_WIDTH, KP923879532, Td, Tc);
        SIMD_FNMADD_D<T>(io + WS(os, 5) * SIMD_WIDTH, KP923879532, Td, Tc);

        SIMD_FMADD_D<T>(io + WS(os, 15) * SIMD_WIDTH, KP923879532, Tt, Ts);
        SIMD_FNMADD_D<T>(io + WS(os, 7) * SIMD_WIDTH, KP923879532, Tt, Ts);
        SIMD_FMADD_D<T>(io + WS(os, 3) * SIMD_WIDTH, KP923879532, Th, Tg);
        SIMD_FNMADD_D<T>(io + WS(os, 11) * SIMD_WIDTH, KP923879532, Th, Tg);
        SIMD_FMADD_D<T>(ro + WS(os, 1) * SIMD_WIDTH, KP923879532, Td, Tf);
        SIMD_FNMADD_D<T>(ro + WS(os, 9) * SIMD_WIDTH, KP923879532, Td, Tf);
        SIMD_FMADD_D<T>(ro + WS(os, 5) * SIMD_WIDTH, KP923879532, Tq, Tr);
        SIMD_FNMADD_D<T>(ro + WS(os, 13) * SIMD_WIDTH, KP923879532, Tq, Tr);

        // 	ro[WS(os, 7)]  = SIMD_FMADD(KP923879532, Th, Ti);
        //  ro[WS(os, 15)] = SIMD_FNMADD(KP923879532, Th, Ti);
        //  ro[WS(os, 3)]  = SIMD_FMADD(KP923879532, Tt, Tu);
        //  ro[WS(os, 11)] = SIMD_FNMADD(KP923879532, Tt, Tu);
        //  io[WS(os, 1)]  = SIMD_FMADD(KP923879532, Tq, Tp);
        //  io[WS(os, 9)]  = SIMD_FNMADD(KP923879532, Tq, Tp);
        //  io[WS(os, 13)] = SIMD_FMADD(KP923879532, Td, Tc);
        //  io[WS(os, 5)]  = SIMD_FNMADD(KP923879532, Td, Tc);
        //  io[WS(os, 15)] = SIMD_FMADD(KP923879532, Tt, Ts);
        //  io[WS(os, 7)]  = SIMD_FNMADD(KP923879532, Tt, Ts);
        //  io[WS(os, 3)]  = SIMD_FMADD(KP923879532, Th, Tg);
        //  io[WS(os, 11)] = SIMD_FNMADD(KP923879532, Th, Tg);
        //  ro[WS(os, 1)]  = SIMD_FMADD(KP923879532, Td, Tf);
        //  ro[WS(os, 9)]  = SIMD_FNMADD(KP923879532, Td, Tf);
        //  ro[WS(os, 5)]  = SIMD_FMADD(KP923879532, Tq, Tr);
        //  ro[WS(os, 13)] = SIMD_FNMADD(KP923879532, Tq, Tr);
    }
}


