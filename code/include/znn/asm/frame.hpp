#pragma once

#include "znn/asm/assembler.hpp"
#include "znn/asm/registers.hpp"
#include "znn/types.hpp"
#include <memory>

namespace znn
{
namespace fft
{

class frame
{
private:
    std::shared_ptr<assembler> asm_ = nullptr;
    std::set<std::string>      possible_regs;
    std::set<std::string>      used_regs;
    std::set<std::string>      pushed;
    std::vector<std::string>   pushed_ordered;

public:
    frame()
        : asm_(std::make_shared<assembler>())
        , possible_regs(registers.used())
    {
    }

    frame(std::shared_ptr<assembler> a)
        : asm_(a)
        , possible_regs(registers.used())
    {
    }

    std::shared_ptr<assembler> asmb() const { return asm_; }

    frame spawn() { return frame(asm_); }

    frame(frame const&) = delete;
    frame& operator=(frame const&) = delete;

    frame(frame&& other) { *this = std::move(other); }

    frame& operator=(frame&& other)
    {
        asm_          = std::move(other.asm_);
        possible_regs = std::move(other.possible_regs);
        used_regs     = std::move(other.used_regs);
        pushed        = std::move(other.pushed);
        return *this;
    }

    void mark_used(std::string reg)
    {
        STRONG_ASSERT(possible_regs.count(reg));
        possible_regs.erase(reg);
    }

    std::string get_register()
    {
        auto r = registers.get_register();
        if (r != "")
        {
            used_regs.insert(r);
            return r;
        }
        else if (possible_regs.size())
        {
            std::string r = *(possible_regs.rbegin());
            possible_regs.erase(r);
            asm_->push(r);
            pushed.insert(r);
            pushed_ordered.push_back(r);
            return r;
        }
        else
        {
            STRONG_ASSERT(false);
        }
    }

    void return_register(reg_t r)
    {
        STRONG_ASSERT(used_regs.count(r));
        used_regs.erase(r);
        registers.unuse(r);
    }

    ~frame()
    {
        if (asm_)
        {
            for (auto r = pushed.rbegin(); r != pushed.rend(); ++r)
            {
                asm_->pop(*r);
            }
            for (auto r : used_regs)
            {
                registers.unuse(r);
            }
        }
    }

    std::string str() { return asm_->str(); }
};
}
} // namespace znn::assembler
