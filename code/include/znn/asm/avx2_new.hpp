#pragma once

#include "znn/asm/assembler.hpp"
#include "znn/asm/frame.hpp"
#include "znn/assert.hpp"

#include <cstddef>
#include <dlfcn.h>
#include <fstream>
#include <mutex>
#include <string>
#include <zi/utility/singleton.hpp>

namespace znn
{
namespace assembler
{
namespace avx2
{

inline long_t ifelse(long_t a, long_t b) { return a ? a : b; }

inline std::pair<long_t, long_t> as_equal_as_possible(long_t m, long_t x)
{
    return std::make_pair(x, ifelse(m % x, x));
}

inline std::pair<long_t, long_t> blocking_m(long_t m, long_t n)
{
    // if (n == 1)
    // {
    //     return as_equal_as_possible(m, 7);
    // }

    // if (n == 2)
    // {
    //     return as_equal_as_possible(m, 4);
    // }

    // if (n % 4 == 0)
    // {
    //     return as_equal_as_possible(m, 2);
    // }

    return as_equal_as_possible(m, 5);
}

inline void avx2_M_K_kernel(frame& fr, long_t M, long_t K, long_t LDA,
                            long_t LDB, long_t LDC, long_t beta, reg_t A_reg,
                            reg_t B_reg, reg_t C_reg, reg_t Apf_reg,
                            reg_t Bpf_reg, reg_t Cpf_reg, long_t total_count,
                            long_t unroll_count = 16, bool revert = true)
{
    STRONG_ASSERT(M * K + M <= 16);

    auto asmb = fr.asmb();

    reg_t a[16];
    reg_t b[16];
    reg_t c[16][16];

    long_t yidx = 0;

    for (long_t m = 0; m < M; ++m)
    {
        for (long_t k = 0; k < K; ++k)
        {
            c[m][k] = ymm(yidx++);
            if (beta)
            {
                asmb->vmov(ptr(m * LDC * 4 + k * 8 * 4, C_reg), c[m][k]);
            }
            else
            {
                asmb->set0(c[m][k]);
            }
        }
    }

    for (long_t m = 0; m < M; ++m)
    {
        a[m] = ymm(yidx++);
    }

    // for (long_t k = 0; k < K; ++k)
    // {
    //     b[k] = ymm(yidx++);
    // }

    auto body = [&](long_t count) {

        for (long_t i = 0; i < count; ++i)
        {
            for (long_t m = 0; m < M; ++m)
            {
                asmb->vbroadcastss(ptr(i * 4 + LDA * m * 4, A_reg), a[m]);
                if (i == 0)
                {
                    asmb->prefetcht0(ptr(i * 4 + LDA * m * 4 + 16 * 4, A_reg));
                }
            }

            for (long_t k = 0; k < K; ++k)
            {
                //asmb->vmov(ptr(k * 8 * 4, B_reg), b[k]);
                if (Bpf_reg != no_reg)
                {
                    asmb->prefetcht1(ptr(k * 8 * 4, Bpf_reg));
                }
            }

            for (long_t k = 0, j = 0; k < K; ++k)
            {
                for (long_t m = 0; m < M; ++m, ++j)
                {
                    asmb->vfmadd231ps(ptr(k * 8 * 4, B_reg), a[m], c[m][k]);
                    // if (j == M * K / 2)
                }
            }
            {
                asmb->add(val(LDB * 4), B_reg);
                if (Bpf_reg != no_reg)
                {
                    asmb->add(val(LDB * 4), Bpf_reg);
                }
            }
        }
    };

    if (unroll_count * 2 >= total_count)
    {
        body(total_count);
    }
    else
    {

        long_t loops     = total_count / unroll_count;
        long_t remainder = total_count - loops * unroll_count;

        auto loop_reg = fr.get_register();
        asmb->mov(val(0), loop_reg);
        auto loop_lab = asmb->label();
        asmb->add(val(1), loop_reg);

        body(unroll_count);

        asmb->cmp(val(loops), loop_reg);
        asmb->jl(loop_lab);

        fr.return_register(loop_reg);

        body(remainder);
    }

    for (long_t m = 0; m < M; ++m)
    {
        for (long_t k = 0; k < K; ++k)
        {
            asmb->vmov(c[m][k], ptr(m * LDC * 4 + k * 8 * 4, C_reg));

            if (Apf_reg != no_reg)
            {
                asmb->prefetcht1(ptr(m * LDA * 4 + k * 8 * 4, Apf_reg));
            }
            if (Cpf_reg != no_reg)
            {
                asmb->prefetcht1(ptr(m * LDC * 4 + k * 8 * 4, Cpf_reg));
            }
        }
    }

    if (revert)
    {
        asmb->sub(val(total_count * LDB * 4), B_reg);
        if (Bpf_reg != no_reg)
        {
            asmb->sub(val(total_count * LDB * 4), Bpf_reg);
        }
    }
}

inline long_t k_block(long_t m)
{
    long_t full = (16 - m) / (m);
    return full;
}

inline void gemm_k_loop(frame& fr, long_t M, long_t N, long_t K, long_t LDA,
                        long_t LDB, long_t LDC, long_t beta, std::string A_reg,
                        std::string B_reg, std::string C_reg,
                        std::string Apf_reg, std::string Bpf_reg,
                        std::string Cpf_reg, bool last = false,
						long_t TILE_STRIDE = 0, long_t OF_STRIDE = 0)
{
    STRONG_ASSERT(K % 8 == 0);

    auto f = fr.spawn();
    auto a = f.asmb();

    f.mark_used(A_reg);
    f.mark_used(B_reg);
    f.mark_used(C_reg);

    if (Apf_reg != "")
        f.mark_used(Apf_reg);
    if (Bpf_reg != "")
        f.mark_used(Bpf_reg);
    if (Cpf_reg != "")
        f.mark_used(Cpf_reg);

    long_t block_size = k_block(M);

    long_t full = K / 8 / block_size;
    long_t part = (K / 8) % block_size;

    auto body = [&](long_t myK, bool inc) {
        avx2_M_K_kernel(f, M, myK, LDA, LDB, LDC, beta, A_reg, B_reg, C_reg,
                        Apf_reg, Bpf_reg, Cpf_reg, N);
        if (inc)
        {
            a->add(val(block_size * 8 * 4), B_reg);
            a->add(val(block_size * 8 * 4), C_reg);
            if (Bpf_reg != no_reg)
            {
                a->add(val(block_size * 8 * 4), Bpf_reg);
            }
        }
    };

    if (full > 1)
    {
        auto loop_reg = f.get_register();
        a->mov(val(0), loop_reg);
        auto loop_lab = a->label();

        a->add(val(1), loop_reg);

        body(block_size, true);

        a->cmp(val(full), loop_reg);
        a->jl(loop_lab);
        f.return_register(loop_reg);
    }
    else if (full == 1)
    {
        body(block_size, true);
    }

    if (part)
    {
        body(part, false);
    }

    a->sub(val(full * block_size * 8 * 4), B_reg);
    a->sub(val(full * block_size * 8 * 4), C_reg);
    if (Bpf_reg != no_reg)
    {
        a->sub(val(full * block_size * 8 * 4), Bpf_reg);
    }
}

inline std::pair<std::string, std::string>
znn_gemm(long_t M, long_t N, long_t K, long_t LDA, long_t LDB, long_t LDC,
         long_t beta, bool Apf, bool Bpf, bool Cpf, bool last = false,
		 long_t TILE_STRIDE = 0,long_t OF_STRIDE = 0)
{
    std::string name = "znn_win_gemm_avx2_" + std::to_string(M) + "_" +
                       std::to_string(N) + "_" + std::to_string(K) + "_" +
                       std::to_string(LDA) + "_" + std::to_string(LDB) + "_" +
                       std::to_string(LDC) + "_" + std::to_string(beta) + "_" +
                       std::to_string(Apf) + "_" + std::to_string(Bpf) + "_" +
                       std::to_string(Cpf) + "_" + std::to_string(last) + "_" +
					   std::to_string(TILE_STRIDE) + "_" + std::to_string(OF_STRIDE);

    std::map<std::string, std::shared_ptr<assembler>> functions;

    auto assm = std::make_shared<assembler>();

    // assm->jmp(name);
    // assm->put_label(name);

    frame main_frame(assm);

    STRONG_ASSERT(LDA % 8 == 0);
    STRONG_ASSERT(LDB % 8 == 0);
    STRONG_ASSERT(LDC % 8 == 0);
    STRONG_ASSERT(N % 8 == 0);
    STRONG_ASSERT(K % 8 == 0);

    reg_t A_reg = main_frame.get_register();
    reg_t B_reg = main_frame.get_register();
    reg_t C_reg = main_frame.get_register();

    assm->mov("\%0", A_reg);
    assm->mov("\%1", B_reg);
    assm->mov("\%2", C_reg);

    reg_t Apf_reg = no_reg;
    reg_t Bpf_reg = no_reg;
    reg_t Cpf_reg = no_reg;

    if (Apf)
    {
        Apf_reg = main_frame.get_register();
        assm->mov("\%3", Apf_reg);
    }

    if (Bpf)
    {
        Bpf_reg = main_frame.get_register();
        assm->mov("\%4", Bpf_reg);
    }

	if (last && beta)
	{
	   Bpf_reg = main_frame.get_register();
	   assm->mov("%4", Bpf_reg);
    }

    if (Cpf)
    {
        Cpf_reg = main_frame.get_register();
        assm->mov("\%5", Cpf_reg);
    }

    auto block = blocking_m(M, K / 8);

    long_t whole_loops = (M - block.second) / block.first;

    auto body = [&]() {

        gemm_k_loop(main_frame, block.first, N, K, LDA, LDB, LDC, beta, A_reg,
                    B_reg, C_reg, Apf_reg, no_reg, Cpf_reg);

        assm->add(val(block.first * LDA * 4), A_reg);
        assm->add(val(block.first * LDC * 4), C_reg);

        if (Apf)
        {
            assm->add(val(block.first * LDA * 4), Apf_reg);
        }

        if (Cpf)
        {
            assm->add(val(block.first * LDC * 4), Cpf_reg);
        }
    };

    if (whole_loops > 1)
    {
        auto loop_reg = main_frame.get_register();
        assm->mov(val(0), loop_reg);
        auto loop_lab = assm->label();

        assm->add(val(1), loop_reg);

        body();

        assm->cmp(val(whole_loops), loop_reg);
        assm->jl(loop_lab);
        main_frame.return_register(loop_reg);
    }
    else if (whole_loops == 1)
    {
        body();
    }

    gemm_k_loop(main_frame, block.second, N, K, LDA, LDB, LDC, beta, A_reg,
                B_reg, C_reg, Apf_reg, Bpf_reg, Cpf_reg);

    return std::make_pair(name, assm->str());
}

extern "C" {
typedef void (*znn_gemm_t)(const float*, const float*, float*, const float*,
                           const float*, const float*);
}

struct znn_gemm_cache_t
{
    std::mutex lock;
    std::map<std::string, znn_gemm_t> cache;
};

namespace
{
znn_gemm_cache_t& znn_gemm_cache = zi::singleton<znn_gemm_cache_t>::instance();
}

inline znn_gemm_t get_znn_gemm(long_t M, long_t N, long_t K, long_t LDA,
                               long_t LDB, long_t LDC, long_t beta, bool Apf,
                               bool Bpf, bool Cpf, bool Apf0 = true,
                               bool Bpf0 = true, bool Cpf0 = false,
							   bool last = false, long_t TILE_STRIDE = 0,
							   long_t OF_STRIDE = 0)

{
    static_cast<void>(Apf0);
    static_cast<void>(Bpf0);
    static_cast<void>(Cpf0);

    std::unique_lock<std::mutex> guard(znn_gemm_cache.lock);

    std::system("mkdir -p gen");

    auto fn = znn_gemm(M, N, K, LDA, LDB, LDC, beta, Apf, Bpf,
                       Cpf, last, TILE_STRIDE, OF_STRIDE); //, Apf0, Bpf0, Cpf0);

    if (znn_gemm_cache.cache.count(fn.first))
    {
        return znn_gemm_cache.cache[fn.first];
    }

    std::string fname = std::string("./gen/") + fn.first + ".c";

    std::ofstream ofs(fname.c_str());

    ofs << "void " + fn.first +
               "(const float* A, const float* B, float* C, const "
               "float* A_prefetch, const float* B_prefetch, const float* "
               "C_prefetch) { \n    __asm__ __volatile__ (\n";

    ofs << fn.second;

    ofs << "        :\n"
           "        : \"m\"(A), \"m\"(B), \"m\"(C), \"m\"(A_prefetch), "
           "\"m\"(B_prefetch), \"m\"(C_prefetch)\n"
           "        : \"rax\", \"rbx\", \"rcx\", \"rdx\", "
           "\"rdi\", \"rsi\", \"r8\", \"r9\", \"r10\",\n"
           "          \"r11\", \"r12\", \"r13\", \"r14\", \"r15\", "
           "\"ymm0\", \"ymm1\", \"ymm2\", \"ymm3\",\n"
           "          \"ymm4\", \"ymm5\", \"ymm6\", \"ymm7\", \"ymm8\", "
           "\"ymm9\", \"ymm10\", \"ymm11\",\n"
           "          \"ymm12\", \"ymm13\", \"ymm14\", \"ymm15\");\n}\n";

    std::string compile_command = std::string("gcc -shared -Wl,-soname,") +
                                  fn.first + ".so -O3 -DNDEBUG -fPIC " + fname +
                                  " -mavx2 -mfma -o ./gen/" + fn.first + ".so";

    ofs.flush();

    system(compile_command.c_str());

    std::string so_name = std::string("./gen/") + fn.first + ".so";

    void* myso = dlopen(so_name.c_str(), RTLD_NOW);

    if (!myso)
    {
        std::printf("%s\n", dlerror());
        exit(EXIT_FAILURE);
    }
    STRONG_ASSERT(myso);

    void* myfn = dlsym(myso, fn.first.c_str());

    STRONG_ASSERT(myfn);

    return znn_gemm_cache.cache[fn.first] = reinterpret_cast<znn_gemm_t>(myfn);

    return reinterpret_cast<znn_gemm_t>(myfn);
}

} // namespace avx512
} // namespace assembler
} // namespace znn
