#pragma once

#include "znn/asm/registers.hpp"

#include <sstream>

namespace znn::fft
{

inline std::string ptr(std::string base_reg) { return "(" + base_reg + ")"; }

inline std::string ptr(long_t s, std::string base_reg)
{
    return std::to_string(s) + "(" + base_reg + ")";
}

inline std::string ptr(long_t s, std::string base_reg, std::string index_reg,
                       long_t mult = 1)
{
    return std::to_string(s) + "(" + base_reg + "," + index_reg + "," +
           std::to_string(mult) + ")";
}

namespace
{
std::string _1to16 = "%{1to16%}";
}

inline std::string ptr_1to16(std::string base) { return ptr(base) + _1to16; }

inline std::string ptr_1to16(long_t s, std::string base)
{
    return ptr(s, base) + _1to16;
}

inline std::string ptr_1to16(long_t s, std::string base, std::string index,
                             long_t mult = 1)
{
    return ptr(s, base, index, mult) + _1to16;
}

inline std::string val(long_t v)
{
    std::string ret = std::string("$") + std::to_string(v);
    return ret;
}

class assembler : public std::ostringstream
{
private:
    std::string label_;

public:
    inline void vfmadd231ps(std::string a, std::string b, std::string c,
                            bool negate = false)
    {
        if (negate)
        {
            *this << "        \"vfnmadd231ps " + a + ", " + b + ", " + c +
                         "\\n\\t\"\n";
        }
        else
        {
            *this << "        \"vfmadd231ps " + a + ", " + b + ", " + c +
                         "\\n\\t\"\n";
        }
    }

    inline void vfnmadd231ps(std::string a, std::string b, std::string c)
    {
        *this << "        \"vfnmadd231ps " + a + ", " + b + ", " + c +
                     "\\n\\t\"\n";
    }

    inline void mov(std::string a, std::string b)
    {
        *this << "        \"movq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void vmov(std::string a, std::string b)
    {
        *this << "        \"vmovaps " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void vmovnt(std::string a, std::string b)
    {
        *this << "        \"vmovntps " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void add(std::string a, std::string b)
    {
        *this << "        \"addq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void sub(std::string a, std::string b)
    {
        *this << "        \"subq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void shl(std::string a, std::string b)
    {
        *this << "        \"shlq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void shr(std::string a, std::string b)
    {
        *this << "        \"shrq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void smart_sub(long_t a, std::string b)
    {
        if (a > 2000000000)
        {
            shr(val(4), b);
            sub(val(a >> 4), b);
            shl(val(4), b);
        }
        else
        {
            sub(val(a), b);
        }
    }

    inline void cmp(std::string a, std::string b)
    {
        *this << "        \"cmpq " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void jl(std::string a)
    {
        *this << "        \"jl " + a + "b\\n\\t\"\n";
    }

    inline void push(std::string a)
    {
        *this << "        \"pushq " + a + "\\n\\t\"\n";
    }

    inline void pop(std::string a)
    {
        *this << "        \"popq " + a + "\\n\\t\"\n";
    }

    inline void vbroadcastss(reg_t a, reg_t b)
    {
        *this << "        \"vbroadcastss " + a + ", " + b + "\\n\\t\"\n";
    }

    inline void prefetcht1(std::string a)
    {
        *this << "        \"prefetcht1 " + a + "\\n\\t\"\n";
    }

    inline void prefetcht0(std::string a)
    {
        *this << "        \"prefetcht0 " + a + "\\n\\t\"\n";
    }

    inline void call(std::string a)
    {
        *this << "        \"call " + a + "b\\n\\t\"\n";
    }

    inline void ret() { *this << "        \"ret\\n\\t\"\n"; }

    inline std::string label()
    {
        auto a = registers.get_label();
        *this << "        \"" + a + ":\\n\\t\"\n";
        return a;
    }

    inline void put_label(std::string l)
    {
        *this << "        \"" + l + ":\\n\\t\"\n";
    }

    inline void jmp(std::string l)
    {
        *this << "        \"jmp " + l + "\\n\\t\"\n";
    }

    inline void merge(assembler& a) { *this << a.str(); }

    inline void set0(std::string r)
    {
        if (r[2] == 'z')
        {
            *this << "        \"vpxord " << r << ", " << r << ", "
                  << r + "\\n\\t\"\n";
        }
        else
        {
            *this << "        \"vxorps " << r << ", " << r << ", "
                  << r + "\\n\\t\"\n";
        }
    }
};

} // namespace znn::assembler
