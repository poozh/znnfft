#pragma once

#include "znn/asm/assembler.hpp"
#include "znn/asm/frame.hpp"
#include "znn/types.hpp"

#include <cstddef>
#include <dlfcn.h>
#include <fstream>
#include <mutex>
#include <string>
#include <zi/utility/singleton.hpp>

namespace znn
{
namespace fft
{
namespace avx512
{

inline std::pair<long_t, long_t> cblocking(long_t n)
{
    long_t besta = std::min(static_cast<long_t>(15), n);
    long_t bestb = n % besta;

    for (long_t a = 15; a >= 8; --a)
    {
        if (n % a == 0)
        {
            return std::make_pair(a, a);
        }
        if (n % a > bestb)
        {
            besta = a;
            bestb = n % a;
        }
    }

    return std::make_pair(besta, bestb);
}

inline void cgemm_n_loop(frame& fr, long_t M, long_t N, long_t LDA, long_t LDB,
                         std::string A_reg, std::string B_reg,
                         std::string C_reg, std::string Bpf_reg, bool Apf0,
                         bool Bpf0, bool Cpf0)
{
    auto f = fr.spawn();
    auto a = f.asmb();

    f.mark_used(A_reg);
    f.mark_used(B_reg);

    if (Cpf0)
    {
        f.mark_used(C_reg);
    }

    if (Bpf_reg != no_reg)
    {
        f.mark_used(Bpf_reg);
    }

    reg_t r_a_1 = no_reg;
    if (M > 1)
    {
        r_a_1 = f.get_register();
        a->mov(val(LDA * 4), r_a_1);
    }

    reg_t r_a_3 = no_reg;
    if (M > 3)
    {
        r_a_3 = f.get_register();
        a->mov(val(LDA * 4 * 3), r_a_3);
    }

    reg_t r_a_5 = no_reg;
    if (M > 5)
    {
        r_a_5 = f.get_register();
        a->mov(val(LDA * 4 * 5), r_a_5);
    }

    reg_t r_a_7 = no_reg;
    if (M > 7)
    {
        r_a_7 = f.get_register();
        a->mov(val(LDA * 4 * 7), r_a_7);
    }

    reg_t A_reg_9 = no_reg;
    if (M > 9)
    {
        A_reg_9 = f.get_register();
        a->mov(A_reg, A_reg_9);
        a->add(val(LDA * 4 * 9), A_reg_9);
    }

    auto rz = zmm(0);
    auto iz = zmm(1);

#define W_BLOCK 16

    auto unrolled_real = [&](long_t i) {
        if (Bpf_reg != no_reg)
        {
            a->prefetcht1(ptr(i * LDB * 4, Bpf_reg));
        }
        if (M > 0)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg), rz, rzmm(0));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg), rz, izmm(0));
            if (Apf0 && i == 0)
            {
                a->prefetcht0(ptr(128 + i * 4, A_reg));
            }
        }
        if (M > 1)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 1), rz, rzmm(1));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 1), rz, izmm(1));
            if (Apf0 && i == 1)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_1, 1));
            }
        }

        if (Bpf0)
        {
            a->prefetcht0(ptr((i + W_BLOCK) * LDB * 4, B_reg));
        }

        if (M > 2)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 2), rz, rzmm(2));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 2), rz, izmm(2));
            if (Apf0 && i == 2)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_1, 2));
            }
        }
        if (M > 3)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_3, 1), rz, rzmm(3));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_3, 1), rz, izmm(3));
            if (Apf0 && i == 3)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_3, 1));
            }
        }

        if (Bpf0)
        {
            a->prefetcht0(ptr(i * LDB * 4 + 128, B_reg));
        }

        if (M > 4)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 4), rz, rzmm(4));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 4), rz, izmm(4));
            if (Apf0 && i == 4)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_1, 4));
            }
        }
        if (M > 5)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_5, 1), rz, rzmm(5));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_5, 1), rz, izmm(5));
            if (Apf0 && i == 5)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_5, 1));
            }
        }
        if (M > 6)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_3, 2), rz, rzmm(6));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_3, 2), rz, izmm(6));
            if (Apf0 && i == 6)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_3, 2));
            }
        }
        if (M > 7)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_7, 1), rz, rzmm(7));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_7, 1), rz, izmm(7));
            if (Apf0 && i == 7)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_7, 1));
            }
        }
        if (M > 8)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 8), rz, rzmm(8));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 8), rz, izmm(8));
            if (Apf0 && i == 8)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg, r_a_1, 8));
            }
        }
        if (M > 9)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9), rz, rzmm(9));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9), rz, izmm(9));
            if (Apf0 && i == 9)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9));
            }
        }
        if (M > 10)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 1), rz, rzmm(10));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 1), rz,
                           izmm(10));
            if (Apf0 && i == 10)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9, r_a_1, 1));
            }
        }
        if (M > 11)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 2), rz, rzmm(11));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 2), rz,
                           izmm(11));
            if (Apf0 && i == 11)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9, r_a_1, 2));
            }
        }
        if (M > 12)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_3, 1), rz, rzmm(12));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_3, 1), rz,
                           izmm(12));
            if (Apf0 && i == 12)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9, r_a_3, 1));
            }
        }
        if (M > 13)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 4), rz, rzmm(13));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 4), rz,
                           izmm(13));
            if (Apf0 && i == 13)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9, r_a_1, 4));
            }
        }
        if (M > 14)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_5, 1), rz, rzmm(14));
            a->vfmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_5, 1), rz,
                           izmm(14));
            if (Apf0 && i == 14)
            {
                a->prefetcht0(ptr(i * 4 + 128, A_reg_9, r_a_5, 1));
            }
        }
    };

    auto unrolled_imag = [&](long_t i) {
        if (Bpf_reg != no_reg)
        {
            a->prefetcht1(ptr(i * LDB * 4 + 64, Bpf_reg));
        }
        if (M > 0)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg), iz, izmm(0));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg), iz, rzmm(0));
            if (Apf0 && i == 0)
            {
                a->prefetcht0(ptr(192 + i * 4, A_reg));
            }
        }
        if (M > 1)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 1), iz, izmm(1));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 1), iz,
                            rzmm(1));
            if (Apf0 && i == 1)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_1, 1));
            }
        }

        if (Bpf0)
        {
            a->prefetcht0(ptr((i + W_BLOCK) * LDB * 4 + 64, B_reg));
        }

        if (M > 2)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 2), iz, izmm(2));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 2), iz,
                            rzmm(2));
            if (Apf0 && i == 2)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_1, 2));
            }
        }
        if (M > 3)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_3, 1), iz, izmm(3));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_3, 1), iz,
                            rzmm(3));
            if (Apf0 && i == 3)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_3, 1));
            }
        }

        if (Bpf0)
        {
            a->prefetcht0(ptr(i * LDB * 4 + 192, B_reg));
        }

        if (M > 4)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 4), iz, izmm(4));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 4), iz,
                            rzmm(4));
            if (Apf0 && i == 4)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_1, 4));
            }
        }
        if (M > 5)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_5, 1), iz, izmm(5));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_5, 1), iz,
                            rzmm(5));
            if (Apf0 && i == 5)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_5, 1));
            }
        }
        if (M > 6)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_3, 2), iz, izmm(6));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_3, 2), iz,
                            rzmm(6));
            if (Apf0 && i == 6)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_3, 2));
            }
        }
        if (M > 7)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_7, 1), iz, izmm(7));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_7, 1), iz,
                            rzmm(7));
            if (Apf0 && i == 7)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_7, 1));
            }
        }
        if (M > 8)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg, r_a_1, 8), iz, izmm(8));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg, r_a_1, 8), iz,
                            rzmm(8));
            if (Apf0 && i == 8)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg, r_a_1, 8));
            }
        }
        if (M > 9)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9), iz, izmm(9));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9), iz, rzmm(9));
            if (Apf0 && i == 9)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9));
            }
        }
        if (M > 10)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 1), iz, izmm(10));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 1), iz,
                            rzmm(10));
            if (Apf0 && i == 10)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9, r_a_1, 1));
            }
        }
        if (M > 11)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 2), iz, izmm(11));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 2), iz,
                            rzmm(11));
            if (Apf0 && i == 11)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9, r_a_1, 2));
            }
        }
        if (M > 12)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_3, 1), iz, izmm(12));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_3, 1), iz,
                            rzmm(12));
            if (Apf0 && i == 12)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9, r_a_3, 1));
            }
        }
        if (M > 13)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_1, 4), iz, izmm(13));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_1, 4), iz,
                            rzmm(13));
            if (Apf0 && i == 13)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9, r_a_1, 4));
            }
        }
        if (M > 14)
        {
            a->vfmadd231ps(ptr_1to16(i * 4, A_reg_9, r_a_5, 1), iz, izmm(14));
            a->vfnmadd231ps(ptr_1to16(i * 4 + 64, A_reg_9, r_a_5, 1), iz,
                            rzmm(14));
            if (Apf0 && i == 14)
            {
                a->prefetcht0(ptr(i * 4 + 192, A_reg_9, r_a_5, 1));
            }
        }
    };

    auto loop_reg = f.get_register();
    a->mov(val(0), loop_reg);
    auto loop_lab = a->label();
    a->add(val(1), loop_reg);

    // BODY

    // First unrolled
    a->vmov(ptr(0, B_reg), rz);
    a->vmov(ptr(64, B_reg), iz);
    unrolled_real(0);

    // Mid unrolled
    for (long_t u = 1; u < W_BLOCK; ++u)
    {
        // load real for u
        a->vmov(ptr(u * 4 * LDB, B_reg), rz);
        // process imaginary for u-1
        unrolled_imag(u - 1);
        // load imag for u
        a->vmov(ptr(u * 4 * LDB + 64, B_reg), iz);
        // process real for u
        unrolled_real(u);
    }

    // Last unrolled
    unrolled_imag(W_BLOCK - 1);

    a->add(val(W_BLOCK * 4 * 2), A_reg);
    if (M > 9)
    {
        a->add(val(W_BLOCK * 4 * 2), A_reg_9);
    }

    a->add(val(LDB * W_BLOCK * 4), B_reg);

    if (Bpf_reg != no_reg)
    {
        a->add(val(LDB * W_BLOCK * 4), Bpf_reg);
    }

    a->cmp(val(N / W_BLOCK), loop_reg);
    a->jl(loop_lab);

    // Revert A_reg, B_reg, Bpf_reg

    a->sub(val(N * 4 * 2), A_reg);
    a->sub(val(N * LDB * 4), B_reg);
    if (Bpf_reg != no_reg)
    {
        a->sub(val(N * LDB * 4), Bpf_reg);
    }
}

inline void cgemm_k_loop(frame& fr, long_t M, long_t N, long_t K, long_t LDA,
                         long_t LDB, long_t LDC, long_t beta, std::string A_reg,
                         std::string B_reg, std::string C_reg,
                         std::string Apf_reg, std::string Bpf_reg,
                         std::string Cpf_reg, bool Apf0, bool Bpf0, bool Cpf0,
                         std::string Cscatter_reg, long_t SC_LD0, long_t SC_LD1)
{
    STRONG_ASSERT(K % 16 == 0);

    auto f = fr.spawn();
    auto a = f.asmb();

    f.mark_used(A_reg);
    f.mark_used(B_reg);
    f.mark_used(C_reg);

    if (Apf_reg != "")
        f.mark_used(Apf_reg);
    if (Bpf_reg != "")
        f.mark_used(Bpf_reg);
    if (Cpf_reg != "")
        f.mark_used(Cpf_reg);
    if (Cscatter_reg != "")
        f.mark_used(Cscatter_reg);

    auto body = [&]() {
        if (beta)
        {
            for (long_t z = 0; z < M; ++z)
            {
                a->vmov(ptr(LDC * 4 * z, C_reg), rzmm(z));
                a->vmov(ptr(LDC * 4 * z + 64, C_reg), izmm(z));
            }
        }
        else
        {
            a->set0(rzmm(0));
            a->vmov(rzmm(0), izmm(0));
            for (long_t z = 1; z < M; ++z)
            {
                a->vmov(rzmm(0), rzmm(z));
                a->vmov(rzmm(0), izmm(z));
            }
        }

        cgemm_n_loop(f, M, N, LDA, LDB, A_reg, B_reg, C_reg, Bpf_reg, Apf0,
                     Bpf0, Cpf0);

        for (long_t z = 0; z < M; ++z)
        {
            // todo
            if (Cscatter_reg != no_reg)
            {
                a->vmovnt(rzmm(z), ptr(SC_LD0 * 4 * z, Cscatter_reg));
                a->vmovnt(izmm(z), ptr(SC_LD0 * 4 * z + 64, Cscatter_reg));
            }
            else
            {
                a->vmov(rzmm(z), ptr(LDC * 4 * z, C_reg));
                a->vmov(izmm(z), ptr(LDC * 4 * z + 64, C_reg));
            }
            if (Apf_reg != no_reg)
            {
                a->prefetcht1(ptr(LDA * z * 4, Apf_reg));
                a->prefetcht1(ptr(LDA * z * 4 + 64, Apf_reg));
            }
            if (Cpf_reg != no_reg)
            {
                a->prefetcht1(ptr(LDC * z * 4, Cpf_reg));
                a->prefetcht1(ptr(LDC * z * 4 + 64, Cpf_reg));
            }
        }
    };

    if (K == 16)
    {
        body();
    }
    else
    {
        auto loop_reg = f.get_register();
        a->mov(val(0), loop_reg);
        auto loop_lab = a->label();
        a->add(val(1), loop_reg);

        body();

        a->add(val(16 * 4 * 2), C_reg);
        a->add(val(16 * 4 * 2), B_reg);

        if (Apf_reg != no_reg)
        {
            a->add(val(16 * 4 * 2), Apf_reg);
        }

        if (Cpf_reg != no_reg)
        {
            a->add(val(16 * 4 * 2), Cpf_reg);
        }

        if (Cscatter_reg != no_reg)
        {
            a->add(val(SC_LD1 * 4), Cscatter_reg);
        }

        a->cmp(val(K / 16), loop_reg);
        a->jl(loop_lab);

        a->sub(val(K * 4 * 2), C_reg);
        a->sub(val(K * 4 * 2), B_reg);

        if (Apf_reg != no_reg)
        {
            a->sub(val(K * 4 * 2), Apf_reg);
        }

        if (Cpf_reg != no_reg)
        {
            a->sub(val(K * 4 * 2), Cpf_reg);
        }

        if (Cscatter_reg != no_reg)
        {
            a->smart_sub((K / 16) * SC_LD1 * 4, Cscatter_reg);
        }
    }
}

inline std::pair<std::string, std::string>
znn_cgemm(long_t M, long_t N, long_t K, long_t LDA, long_t LDB, long_t LDC,
          long_t beta, bool Apf, bool Bpf, bool Cpf, bool Apf0 = true,
          bool Bpf0 = true, bool Cpf0 = false, bool Cscatter = false,
          long_t SC_LD0 = 0, long_t SC_LD1 = 0)
{
    std::string name = "znn_cgemm_avx512_" + std::to_string(M) + "_" +
                       std::to_string(N) + "_" + std::to_string(K) + "_" +
                       std::to_string(LDA) + "_" + std::to_string(LDB) + "_" +
                       std::to_string(LDC) + "_" + std::to_string(beta) + "_" +
                       std::to_string(Apf) + "_" + std::to_string(Bpf) + "_" +
                       std::to_string(Cpf) + "_" + std::to_string(Apf0) + "_" +
                       std::to_string(Bpf0) + "_" + std::to_string(Cpf0) + "_" +
                       std::to_string(Cscatter) + "_" + std::to_string(SC_LD0) +
                       "_" + std::to_string(SC_LD1);

    std::map<std::string, std::shared_ptr<assembler>> functions;

    auto assm = std::make_shared<assembler>();

    // assm->jmp(name);
    // assm->put_label(name);

    frame main_frame(assm);

    STRONG_ASSERT(LDA % 16 == 0);
    STRONG_ASSERT(LDB % 16 == 0);
    STRONG_ASSERT(LDC % 16 == 0);
    STRONG_ASSERT(N % 16 == 0);
    STRONG_ASSERT(K % 16 == 0);

    reg_t A_reg = main_frame.get_register();
    reg_t B_reg = main_frame.get_register();
    reg_t C_reg = main_frame.get_register();

    assm->mov("%0", A_reg);
    assm->mov("%1", B_reg);
    assm->mov("%2", C_reg);

    reg_t Apf_reg = no_reg;
    reg_t Bpf_reg = no_reg;
    reg_t Cpf_reg = no_reg;

    if (Apf)
    {
        Apf_reg = main_frame.get_register();
        assm->mov("%3", Apf_reg);
    }

    if (Bpf)
    {
        Bpf_reg = main_frame.get_register();
        assm->mov("%4", Bpf_reg);
    }

    if (Cpf)
    {
        Cpf_reg = main_frame.get_register();
        assm->mov("%5", Cpf_reg);
    }

    reg_t Cscatter_reg = no_reg;
    if (Cscatter)
    {
        Cscatter_reg = main_frame.get_register();
        assm->mov("%6", Cscatter_reg);
    }

    auto blocks = cblocking(M);

    long_t whole_loops = (M - blocks.second) / blocks.first;

    if (whole_loops > 1)
    {
        auto loop_reg = main_frame.get_register();
        assm->mov(val(0), loop_reg);
        auto loop_lab = assm->label();

        assm->add(val(1), loop_reg);

        cgemm_k_loop(main_frame, blocks.first, N, K, LDA, LDB, LDC, beta, A_reg,
                     B_reg, C_reg, Apf_reg, no_reg, Cpf_reg, Apf0, Bpf0, Cpf0,
                     Cscatter_reg, SC_LD0, SC_LD1);

        assm->add(val(blocks.first * LDA * 4), A_reg);
        assm->add(val(blocks.first * LDC * 4), C_reg);

        if (Apf)
        {
            assm->add(val(blocks.first * LDA * 4), Apf_reg);
        }

        if (Cpf)
        {
            assm->add(val(blocks.first * LDC * 4), Cpf_reg);
        }

        if (Cscatter)
        {
            assm->add(val(blocks.first * SC_LD0 * 4), Cscatter_reg);
        }

        assm->cmp(val(whole_loops), loop_reg);
        assm->jl(loop_lab);
        main_frame.return_register(loop_reg);
    }
    else if (whole_loops == 1)
    {
        cgemm_k_loop(main_frame, blocks.first, N, K, LDA, LDB, LDC, beta, A_reg,
                     B_reg, C_reg, Apf_reg, no_reg, Cpf_reg, Apf0, Bpf0, Cpf0,
                     Cscatter_reg, SC_LD0, SC_LD1);

        assm->add(val(blocks.first * LDA * 4), A_reg);
        assm->add(val(blocks.first * LDC * 4), C_reg);

        if (Apf)
        {
            assm->add(val(blocks.first * LDA * 4), Apf_reg);
        }

        if (Cpf)
        {
            assm->add(val(blocks.first * LDC * 4), Cpf_reg);
        }

        if (Cscatter)
        {
            assm->add(val(blocks.first * SC_LD0 * 4), Cscatter_reg);
        }
    }

    cgemm_k_loop(main_frame, blocks.second, N, K, LDA, LDB, LDC, beta, A_reg,
                 B_reg, C_reg, Apf_reg, Bpf_reg, Cpf_reg, Apf0, Bpf0, Cpf0,
                 Cscatter_reg, SC_LD0, SC_LD1);

    return std::make_pair(name, assm->str());
}

extern "C" {
typedef void (*znn_cgemm_t)(const float*, const float*, float*, const float*,
                            const float*, const float*, float*);
}

struct znn_cgemm_cache_t
{
    std::mutex                                           lock;
    std::map<std::string, std::pair<znn_cgemm_t, void*>> cache;
};

namespace
{
znn_cgemm_cache_t& znn_cgemm_cache =
    zi::singleton<znn_cgemm_cache_t>::instance();
}

inline void clear_znn_cgemms()
{
    std::unique_lock<std::mutex> guard(znn_cgemm_cache.lock);
    for (auto& x : znn_cgemm_cache.cache)
    {
        dlclose(x.second.second);
    }
    znn_cgemm_cache.cache.clear();
}

inline znn_cgemm_t get_znn_cgemm(long_t M, long_t N, long_t K, long_t LDA,
                                 long_t LDB, long_t LDC, long_t beta, bool Apf,
                                 bool Bpf, bool Cpf, bool Apf0 = true,
                                 bool Bpf0 = true, bool Cpf0 = false,
                                 bool Cscatter = false, long_t SC_LD0 = 0,
                                 long_t SC_LD1 = 0)
{
    if (!Cscatter)
    {
        SC_LD0 = 0;
        SC_LD1 = 0;
    }

    std::unique_lock<std::mutex> guard(znn_cgemm_cache.lock);

    std::string name = "znn_cgemm_avx512_" + std::to_string(M) + "_" +
                       std::to_string(N) + "_" + std::to_string(K) + "_" +
                       std::to_string(LDA) + "_" + std::to_string(LDB) + "_" +
                       std::to_string(LDC) + "_" + std::to_string(beta) + "_" +
                       std::to_string(Apf) + "_" + std::to_string(Bpf) + "_" +
                       std::to_string(Cpf) + "_" + std::to_string(Apf0) + "_" +
                       std::to_string(Bpf0) + "_" + std::to_string(Cpf0) + "_" +
                       std::to_string(Cscatter) + "_" + std::to_string(SC_LD0) +
                       "_" + std::to_string(SC_LD1);

    if (znn_cgemm_cache.cache.count(name))
    {
        return znn_cgemm_cache.cache[name].first;
    }

    std::system("mkdir -p gen");

    auto fn = znn_cgemm(M, N, K, LDA, LDB, LDC, beta, Apf, Bpf, Cpf, Apf0, Bpf0,
                        Cpf0, Cscatter, SC_LD0, SC_LD1);

    if (znn_cgemm_cache.cache.count(fn.first))
    {
        return znn_cgemm_cache.cache[fn.first].first;
    }

    std::string fname = std::string("./gen/") + fn.first + ".c";

    std::ofstream ofs(fname.c_str());

    ofs << "void " + fn.first +
               "(const float* A, const float* B, float* C, const "
               "float* A_prefetch, const float* B_prefetch, const float* "
               "C_prefetch, const float* C_scatter) { \n    __asm__ "
               "__volatile__ (\n";

    ofs << fn.second;

    ofs << "        :\n"
           "        : \"m\"(A), \"m\"(B), \"m\"(C), \"m\"(A_prefetch), "
           "\"m\"(B_prefetch), \"m\"(C_prefetch), \"m\"(C_scatter)\n"
           "        : \"rax\", \"rbx\", \"rcx\", \"rdx\", "
           "\"rdi\", \"rsi\", \"r8\", \"r9\", \"r10\",\n"
           "          \"r11\", \"r12\", \"r13\", \"r14\", \"r15\", "
           "\"zmm0\", \"zmm1\", \"zmm2\", \"zmm3\",\n"
           "          \"zmm4\", \"zmm5\", \"zmm6\", \"zmm7\", \"zmm8\", "
           "\"zmm9\", \"zmm10\", \"zmm11\",\n"
           "          \"zmm12\", \"zmm13\", \"zmm14\", \"zmm15\", "
           "\"zmm16\", \"zmm17\", \"zmm18\",\n"
           "          \"zmm19\", \"zmm20\", \"zmm21\", \"zmm22\", "
           "\"zmm23\", \"zmm24\", \"zmm25\",\n"
           "          \"zmm26\", \"zmm27\", \"zmm28\", \"zmm29\", "
           "\"zmm30\", \"zmm31\");\n}\n";

    std::string compile_command = std::string("gcc -shared -Wl,-soname,") +
                                  fn.first + ".so -O3 -DNDEBUG -fPIC " + fname +
                                  " -mavx512f -mavx512pf -o ./gen/" + fn.first +
                                  ".so";

    // std::string compile_command = std::string("gcc -shared -Wl,-soname,") +
    //                               fn.first + ".so -O3 -DNDEBUG -fPIC " +
    //                               fname +
    //                               " -mavx512f -mavx512pf -o ./gen/" +
    //                               fn.first +
    //                               ".so";

    ofs.flush();

    system(compile_command.c_str());

    std::string so_name = std::string("./gen/") + fn.first + ".so";

    void* myso = dlopen(so_name.c_str(), RTLD_NOW);

    if (!myso)
    {
        std::printf("%s\n", dlerror());
        exit(EXIT_FAILURE);
    }
    STRONG_ASSERT(myso);

    void* myfn = dlsym(myso, fn.first.c_str());

    STRONG_ASSERT(myfn);

    znn_cgemm_cache.cache[fn.first] =
        std::make_pair(reinterpret_cast<znn_cgemm_t>(myfn), myso);

    return reinterpret_cast<znn_cgemm_t>(myfn);
}

} // namespace avx512
} // namespace win
} // namespace znn
