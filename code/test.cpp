#include "znn/fft3/verify.hpp"

using namespace znn::fft3;

int main()
{
    do_verify<10, 10, 64, 64, 1, 51, 31, vek<1, 21, 21>,
              vek<1, 3, 3>, true, true>();

    // do_verify<64, 4, 256, 256, 1, 58, 58, vek<1, 21, 21>,
    //           vek<1, 3, 3>, true, false>();


//do_verify<ZNN_NUM_CORES, 1, 64, 64, 20, 20, 20, vek<4, 4, 4>, vek<3, 3, 3>,
    //          true>("test");
    //do_verify<ZNN_NUM_CORES, 1, 16, 16, 1, 1, 4, vek<1, 1, 4>,
    //          vek<1, 1, 1>, true>();
}
