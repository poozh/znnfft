#include "znn/fft/codelets/codelets.hpp"
#include "znn/vec.hpp"
#include <iostream>
#include <string>
#include <vector>

using zi2::vl::vec;
using znn::long_t;

struct complexity
{
    long_t flops           = 0;
    long_t operations      = 0;
    long_t memory_accesses = 0;
    long_t stack_vars      = 0;
    long_t constants       = 0;
};

std::vector<std::vector<complexity>> r2cf_complexities(33);
std::vector<std::vector<complexity>> r2cb_complexities(33);
std::vector<std::vector<complexity>> c2cf_complexities(33);
std::vector<std::vector<complexity>> c2cb_complexities(33);

std::vector<complexity>              win_complexities(13);
std::vector<std::vector<complexity>> wker_complexities(10);
std::vector<std::vector<complexity>> wout_complexities(10);

inline void load_win()
{
    for (long_t i = 0; i < 10; ++i)
    {
        wker_complexities[i].resize(10);
        wout_complexities[i].resize(10);
    }

    win_complexities[4]  = {4, 4, 0, 0, 0};
    win_complexities[6]  = {20, 13, 0, 0, 3};
    win_complexities[8]  = {44, 26, 0, 0, 14};
    win_complexities[10] = {75, 43, 0, 0, 26};
    win_complexities[12] = {114, 64, 0, 0, 43};

    wout_complexities[2][3] = {4, 4, 0, 0, 0};
    wout_complexities[4][3] = {20, 14, 0, 0, 3};
    wout_complexities[6][3] = {52, 32, 0, 0, 10};
    wout_complexities[8][3] = {100, 58, 0, 0, 18};
    wout_complexities[2][5] = {10, 8, 0, 0, 1};
    wout_complexities[4][5] = {34, 22, 0, 0, 6};
    wout_complexities[6][5] = {74, 44, 0, 0, 13};
    wout_complexities[8][5] = {130, 74, 0, 0, 25};

    wker_complexities[2][3] = {7, 4, 0, 0, 1};
    wker_complexities[4][3] = {15, 9, 0, 0, 4};
    wker_complexities[6][3] = {22, 13, 0, 0, 8};
    wker_complexities[8][3] = {29, 17, 0, 0, 10};
    wker_complexities[2][5] = {21, 15, 0, 0, 6};
    wker_complexities[4][5] = {31, 22, 0, 0, 12};
    wker_complexities[6][5] = {41, 29, 0, 0, 16};
    wker_complexities[8][5] = {51, 36, 0, 0, 22};
}

template <long_t N, long_t X>
inline void load_fwd()
{

    if
        constexpr(X == 1)
        {
            r2cf_complexities[N].resize(33);
            c2cf_complexities[N].resize(33);
        }

    using r = znn::fft::r2cf_traits<N, X>;
    using c = znn::fft::c2cf_traits<N, X>;

    r2cf_complexities[N][X] = {r::flops, r::operations, r::memory_accesses,
                               r::stack_vars, r::constants};

    c2cf_complexities[N][X] = {c::flops, c::operations, c::memory_accesses,
                               c::stack_vars, c::constants};

    if
        constexpr(X < N) { load_fwd<N, X + 1>(); }
    else
    {
        if
            constexpr(N < 32) { load_fwd<N + 1, 1>(); }
    }
}

template <long_t N, long_t X>
inline void load_bwd()
{

    if
        constexpr(X == 0)
        {
            r2cb_complexities[N].resize(33);
            c2cb_complexities[N].resize(33);
        }

    using r = znn::fft::r2cb_traits<N, X>;
    using c = znn::fft::c2cb_traits<N, X>;

    r2cb_complexities[N][X] = {r::flops, r::operations, r::memory_accesses,
                               r::stack_vars, r::constants};

    c2cb_complexities[N][X] = {c::flops, c::operations, c::memory_accesses,
                               c::stack_vars, c::constants};

    if
        constexpr(X < N - 1) { load_bwd<N, X + 1>(); }
    else
    {
        if
            constexpr(N < 32) { load_bwd<N + 1, 0>(); }
    }
}

struct layer_complexity
{
    long_t input_transform_flops;
    long_t input_transform_memory;

    long_t kernel_transform_flops;
    long_t kernel_transform_memory;

    long_t cgemm_flops;
    long_t cgemm_memory;

    long_t output_transform_flops;
    long_t output_transform_memory;
};

inline constexpr long_t ceil_div(long_t a, long_t b) { return (a + b - 1) / b; }

inline constexpr vec<long_t, 3> ceil_div(vec<long_t, 3> a, vec<long_t, 3> b)
{
    return {(a[0] + b[0] - 1) / b[0], (a[1] + b[1] - 1) / b[1],
            (a[2] + b[2] - 1) / b[2]};
}

inline constexpr vec<long_t, 3>
    fft_padded_size(vec<long_t, 3> L, vec<long_t, 3> T, vec<long_t, 3> K)
{
    return ceil_div(L - K + 1, T - K + 1) * (T - K + 1) + K - 1;
}

inline constexpr vec<long_t, 3>
    win_padded_size(vec<long_t, 3> L, vec<long_t, 3> T, vec<long_t, 3> K)
{
    return ceil_div(L - K + 1, T) * T + K - 1;
}

inline layer_complexity get_fft_layer_complexities(long_t B, long_t C1,
                                                   long_t C2, vec<long_t, 3> Ix,
                                                   vec<long_t, 3> K,
                                                   vec<long_t, 3> F)
{

    auto I = fft_padded_size(Ix, F, K);

    std::cout << "Padded size: " << I << "\n";

    auto Tiles = (I - K + vec<long_t, 3>::one) / (F - K + vec<long_t, 3>::one);

    std::cout << "Per image tiles: " << Tiles << "\n";

    long_t FFT2 = F[2] / 2 + 1;

    long_t in_tile_flops = F[0] * F[1] * r2cf_complexities[F[2]][F[2]].flops +
                           F[0] * FFT2 * c2cf_complexities[F[1]][F[1]].flops +
                           F[1] * FFT2 * c2cf_complexities[F[0]][F[0]].flops;

    // std::cout << "IN TILE FLOPS: " << in_tile_flops << std::endl;

    long_t out_tile_flops =
        F[0] * F[1] * r2cb_complexities[F[2]][K[2] - 1].flops +
        F[0] * FFT2 * c2cb_complexities[F[1]][K[1] - 1].flops +
        F[1] * FFT2 * c2cb_complexities[F[0]][K[0] - 1].flops;

    // std::cout << "OUT TILE FLOPS: " << out_tile_flops << std::endl;

    long_t ker_tile_flops = F[0] * F[1] * r2cf_complexities[F[2]][K[2]].flops +
                            F[0] * FFT2 * c2cf_complexities[F[1]][K[1]].flops +
                            F[1] * FFT2 * c2cf_complexities[F[0]][K[0]].flops;

    long_t input_transform_flops  = Tiles.prod() * B * C1 * in_tile_flops;
    long_t output_transform_flops = Tiles.prod() * B * C2 * out_tile_flops;
    long_t kernel_transform_flops = C1 * C2 * ker_tile_flops;
    long_t cgemm_flops =
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C1 * C2 * 4 * 2;

    long_t input_transform_memory =
        I.prod() * C1 * B +
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C1 * 2;

    auto O = I + vec<long_t, 3>::one - K;

    long_t output_transform_memory =
        O.prod() * C2 * B +
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C2 * 2;

    long_t kernel_transform_memory =
        K.prod() * C1 * C2 + C1 * C2 * F[0] * F[1] * (F[2] / 2 + 1) * 2;

    long_t cgemm_memory =
        F[0] * F[1] * (F[2] / 2 + 1) *
        (Tiles.prod() * B * C1 * 2 + Tiles.prod() * B * C2 * 2 + C1 * C2 * 2);

    return {input_transform_flops,
            input_transform_memory,
            kernel_transform_flops,
            kernel_transform_memory,
            cgemm_flops,
            cgemm_memory,
            output_transform_flops,
            output_transform_memory};
}

inline layer_complexity
get_fft3_layer_complexities(long_t B, long_t C1, long_t C2, vec<long_t, 3> Ix,
                            vec<long_t, 3> K, vec<long_t, 3> F)
{
    auto I = fft_padded_size(Ix, F, K);

    auto Tiles = (I - K + vec<long_t, 3>::one) / (F - K + vec<long_t, 3>::one);

    long_t FFT2 = F[2] / 2 + 1;

    long_t in_tile_flops = F[0] * F[1] * r2cf_complexities[F[2]][F[2]].flops +
                           F[0] * FFT2 * c2cf_complexities[F[1]][F[1]].flops +
                           F[1] * FFT2 * c2cf_complexities[F[0]][F[0]].flops;

    in_tile_flops += F[0] * F[1] * FFT2;

    // std::cout << "IN TILE FLOPS: " << in_tile_flops << std::endl;

    long_t out_tile_flops =
        F[0] * F[1] * r2cb_complexities[F[2]][K[2] - 1].flops +
        F[0] * FFT2 * c2cb_complexities[F[1]][K[1] - 1].flops +
        F[1] * FFT2 * c2cb_complexities[F[0]][K[0] - 1].flops;

    out_tile_flops += F[0] * F[1] * FFT2 * 2;

    // std::cout << "OUT TILE FLOPS: " << out_tile_flops << std::endl;

    long_t ker_tile_flops = F[0] * F[1] * r2cf_complexities[F[2]][K[2]].flops +
                            F[0] * FFT2 * c2cf_complexities[F[1]][K[1]].flops +
                            F[1] * FFT2 * c2cf_complexities[F[0]][K[0]].flops;

    ker_tile_flops += F[0] * F[1] * FFT2 * 2;

    long_t input_transform_flops  = Tiles.prod() * B * C1 * in_tile_flops;
    long_t output_transform_flops = Tiles.prod() * B * C2 * out_tile_flops;
    long_t kernel_transform_flops = C1 * C2 * ker_tile_flops;
    long_t cgemm_flops =
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C1 * C2 * 3 * 2;

    long_t input_transform_memory =
        I.prod() * C1 * B +
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C1 * 3;

    auto O = I + vec<long_t, 3>::one - K;

    long_t output_transform_memory =
        O.prod() * C2 * B +
        F[0] * F[1] * (F[2] / 2 + 1) * Tiles.prod() * B * C2 * 3;

    long_t kernel_transform_memory =
        K.prod() * C1 * C2 + C1 * C2 * F[0] * F[1] * (F[2] / 2 + 1) * 3;

    long_t cgemm_memory =
        F[0] * F[1] * (F[2] / 2 + 1) *
        (Tiles.prod() * B * C1 * 3 + Tiles.prod() * B * C2 * 2 + C1 * C2 * 3);

    return {input_transform_flops,
            input_transform_memory,
            kernel_transform_flops,
            kernel_transform_memory,
            cgemm_flops,
            cgemm_memory,
            output_transform_flops,
            output_transform_memory};
}

inline void print(layer_complexity const& c)
{
    std::cout << "IN    FLOPS : "
              << static_cast<double>(c.input_transform_flops) << '\n'
              << "IN    MEMORY: " << c.input_transform_memory << '\n'
              << "KER   FLOPS : "
              << static_cast<double>(c.kernel_transform_flops) << '\n'
              << "KER   MEMORY: " << c.kernel_transform_memory << '\n'
              << "CGEMM FLOPS : " << static_cast<double>(c.cgemm_flops) << '\n'
              << "CGEMM MEMORY: " << c.cgemm_memory << '\n'
              << "OUT   FLOPS : "
              << static_cast<double>(c.output_transform_flops) << '\n'
              << "OUT   MEMORY: " << c.output_transform_memory << std::endl;

    std::cout << "TOTAL FLOPS: "
              << (c.input_transform_flops + c.output_transform_flops +
                  c.kernel_transform_flops + c.cgemm_flops)
              << '\n';

    std::cout << "IN    RATIO: "
              << static_cast<long double>(c.input_transform_flops) /
                     c.input_transform_memory
              << '\n'
              << "KER   RATIO: "
              << static_cast<long double>(c.kernel_transform_flops) /
                     c.kernel_transform_memory
              << '\n'
              << "CGEMM RATIO: "
              << static_cast<long double>(c.cgemm_flops) / c.cgemm_memory
              << '\n'
              << "OUT   RATIO: "
              << static_cast<long double>(c.output_transform_flops) /
                     c.output_transform_memory
              << '\n';
}

inline layer_complexity get_win_layer_complexities(long_t B, long_t C1,
                                                   long_t C2, vec<long_t, 3> Ix,
                                                   vec<long_t, 3> K,
                                                   vec<long_t, 3> M)
{
    auto I = win_padded_size(Ix, M, K);

    std::cout << "Padded size: " << I << "\n";

    auto Tiles = (I - K + vec<long_t, 3>::one) / M;

    std::cout << "Per image tiles: " << Tiles << "\n";

    auto F = M + K - vec<long_t, 3>::one;

    long_t in_tile_flops = F[0] * F[1] * win_complexities[F[2]].flops +
                           F[0] * F[2] * win_complexities[F[1]].flops +
                           F[1] * F[2] * win_complexities[F[0]].flops;

    // std::cout << "IN TILE FLOPS: " << in_tile_flops << std::endl;

    long_t out_tile_flops = F[0] * F[1] * wout_complexities[M[2]][K[2]].flops +
                            F[0] * F[2] * wout_complexities[M[1]][K[1]].flops +
                            F[1] * F[2] * wout_complexities[M[0]][K[0]].flops;

    // std::cout << "OUT TILE FLOPS: " << out_tile_flops << std::endl;

    long_t ker_tile_flops = F[0] * F[1] * wker_complexities[M[2]][K[2]].flops +
                            F[0] * F[2] * wker_complexities[M[1]][K[1]].flops +
                            F[1] * F[2] * wker_complexities[M[0]][K[0]].flops;

    long_t input_transform_flops  = Tiles.prod() * B * C1 * in_tile_flops;
    long_t output_transform_flops = Tiles.prod() * B * C2 * out_tile_flops;
    long_t kernel_transform_flops = C1 * C2 * ker_tile_flops;
    long_t cgemm_flops = F[0] * F[1] * F[2] * Tiles.prod() * B * C1 * C2 * 2;

    long_t input_transform_memory =
        I.prod() * C1 * B + F[0] * F[1] * F[2] * Tiles.prod() * B * C1;

    auto O = I + vec<long_t, 3>::one - K;

    long_t output_transform_memory =
        O.prod() * C2 * B + F[0] * F[1] * F[2] * Tiles.prod() * B * C2;

    long_t kernel_transform_memory =
        K.prod() * C1 * C2 + C1 * C2 * F[0] * F[1] * F[2];

    long_t cgemm_memory =
        F[0] * F[1] * F[2] *
        (Tiles.prod() * B * C1 + Tiles.prod() * B * C2 + C1 * C2);

    return {input_transform_flops,
            input_transform_memory,
            kernel_transform_flops,
            kernel_transform_memory,
            cgemm_flops,
            cgemm_memory,
            output_transform_flops,
            output_transform_memory};
}

inline long double get_time(layer_complexity const& l, long double r)
{
    return std::max(static_cast<long double>(l.input_transform_flops) / r,
                    static_cast<long double>(l.input_transform_memory)) +
           std::max(static_cast<long double>(l.kernel_transform_flops) / r,
                    static_cast<long double>(l.kernel_transform_memory)) +
           std::max(static_cast<long double>(l.cgemm_flops) / r,
                    static_cast<long double>(l.cgemm_memory)) +
           std::max(static_cast<long double>(l.output_transform_flops) / r,
                    static_cast<long double>(l.output_transform_memory));
}

inline void analyze(std::string const& name, long_t B, long_t C1, long_t C2,
                    vec<long_t, 3> I, vec<long_t, 3> K)
{
    auto w   = get_win_layer_complexities(B, C1, C2, I, K, {1, 4, 4});
    auto f16 = get_fft_layer_complexities(B, C1, C2, I, K, {1, 16, 16});
    auto f30 = get_fft_layer_complexities(B, C1, C2, I, K, {1, 30, 30});
    auto f32 = get_fft_layer_complexities(B, C1, C2, I, K, {1, 32, 32});
    auto z30 = get_fft3_layer_complexities(B, C1, C2, I, K, {1, 30, 30});

    for (long_t i : {1, 45, 88})
    {
        //        std::cout << name << "," << i << ",FFT16,FFT,16,"
        //                  << get_time(w, i) / get_time(f16, i) << "\n";
        std::cout << name << "," << i << ",FFT30,FFT,30,"
                  << get_time(w, i) / get_time(f30, i) << "\n";
        std::cout << name << "," << i << ",FFZ30,FFZ,30,"
                  << get_time(w, i) / get_time(z30, i) << "\n";
        //        std::cout << name << "," << i << ",FFT32,FFT,32,"
        //                  << get_time(w, i) / get_time(f32, i) << "\n";
    }
}

inline void analyze2(std::string const& name, long_t B, long_t C1, long_t C2,
                     vec<long_t, 3> I, vec<long_t, 3> K)
{
    auto w   = get_win_layer_complexities(B, C1, C2, I, K, {1, 4, 4});
    auto f16 = get_fft_layer_complexities(B, C1, C2, I, K, {1, 17, 17});
    auto z16 = get_fft3_layer_complexities(B, C1, C2, I, K, {1, 17, 17});

    for (auto i : {0.1e-33, 45.0, 88.0, 1e100})
    {
        std::cout << name << "," << i << ",FFT16,FFT,16,"
                  << get_time(w, i) / get_time(f16, i) << "\n";
        std::cout << name << "," << i << ",FFZ16,FFZ,16,"
                  << get_time(w, i) / get_time(z16, i) << "\n";
    }
}

int main()
{
    r2cf_complexities[0].resize(33);
    r2cf_complexities[1].resize(33);
    r2cb_complexities[0].resize(33);
    r2cb_complexities[1].resize(33);
    c2cf_complexities[0].resize(33);
    c2cf_complexities[1].resize(33);
    c2cb_complexities[0].resize(33);
    c2cb_complexities[1].resize(33);

    load_fwd<2, 1>();
    load_bwd<2, 0>();

    load_win();

    std::cout << "\n\nWIN 2\n\n";

    auto r2 = get_win_layer_complexities(1, 32, 32, {1, 224, 224}, {1, 4, 4},
                                         {1, 2, 2});
    print(r2);

    {
        auto r2 = get_win_layer_complexities(1, 32, 32, {1, 224, 224},
                                             {1, 4, 4}, {1, 3, 3});
        print(r2);
    }

    {
        auto r2 = get_win_layer_complexities(1, 32, 32, {1, 224, 224},
                                             {1, 4, 4}, {1, 4, 4});
        print(r2);
    }

    // std::cout << "\n\nWIN 4\n\n";

    //     r2 = get_win_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 4, 4});
    // print(r2);

    // std::cout << "\n\nWIN 6\n\n";

    // r2 = get_win_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 6, 6});
    // print(r2);

    // std::cout << "\n\nFFT 14\n\n";

    // r2 = get_fft_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 16, 16});
    // print(r2);

    // std::cout << "\n\nFFT 28\n\n";

    // r2 = get_fft_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 30, 30});
    // print(r2);

    // std::cout << "\n\nFFT3 14\n\n";

    // r2 = get_fft3_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 16, 16});
    // print(r2);

    // std::cout << "\n\nFFT3 28\n\n";

    // r2 = get_fft3_layer_complexities(64, 256, 256, {1, 58, 58}, {1, 3, 3},
    //                                 {1, 30, 30});
    // print(r2);

    // auto rf = get_fft_layer_complexities(128, 1024, 1024, {1, 17, 17},
    //                                      {1, 3, 3}, {1, 17, 17});
    // print(rf);

    // auto rf3 = get_fft3_layer_complexities(128, 1024, 1024, {1, 17, 17},
    //                                        {1, 3, 3}, {1, 17, 17});
    // print(rf3);

    // auto rf2 = get_fft_layer_complexities(64, 64, 64, {1, 226, 226}, {1, 3,
    // 3},
    //                                       {1, 16, 16});
    // print(rf2);

    // auto rw = get_win_layer_complexities(64, 64, 64, {1, 226, 226}, {1, 3,
    // 3},
    //                                      {1, 4, 4});

    // print(rw);

    // std::cout << "TIMES: " << get_time(rw, 88) / get_time(rf, 88) << ' '
    //           << get_time(rw, 88) << ' ' << get_time(rf, 88) << "\n";

    // std::cout << "TIMES: " << get_time(rw, 88) / get_time(rf2, 88) << ' '
    //           << get_time(rw, 88) << ' ' << get_time(rf2, 88) << "\n";

    // analyze("VGG 1.2", 64, 64, 64, {1, 226, 226}, {1, 3, 3});
    // analyze("VGG 2.1", 64, 64, 128, {1, 114, 114}, {1, 3, 3});
    // analyze("VGG 2.2", 64, 128, 128, {1, 114, 114}, {1, 3, 3});
    // analyze("VGG 3.1", 64, 128, 256, {1, 58, 58}, {1, 3, 3});
    // analyze("VGG 3.2", 64, 256, 256, {1, 58, 58}, {1, 3, 3});
    // analyze("VGG 4.1", 64, 256, 512, {1, 30, 30}, {1, 3, 3});
    // analyze("VGG 4.2", 64, 512, 512, {1, 30, 30}, {1, 3, 3});
    // analyze("VGG 5.x", 64, 512, 512, {1, 16, 16}, {1, 3, 3});

    // analyze2("OverFeat 3", 128, 256, 512, {1, 17, 17}, {1, 3, 3});
    // analyze2("OverFeat 4", 128, 512, 1204, {1, 17, 17}, {1, 3, 3});
    // analyze2("OverFeat 5", 128, 1024, 1024, {1, 17, 17}, {1, 3, 3});
}
