#!/bin/bash
for i in `seq $1 $2`;
do
    make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_fft/bench_$i.bin
done
