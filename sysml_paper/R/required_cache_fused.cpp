#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <vector>

inline double get_r(double E, double F, double G)
{
    return std::floor(F * G * 9 * 16 / (F * G * 2 / E - F - G / 4));
}

int main()
{
    double m, n;
    double FIN;
    std::cin >> m >> n >> FIN;

    double t = (m + n - 1);

    std::vector<double> max_ratio(2049);

    for (double F = 1; F <= FIN; F *= 2)
    {
        for (double G = 1; G <= 2048; G *= 2)
        {
            for (double R = 1; (R * F + F * G + R * G) * t * t * 4 < 2500000;
                 R        = R + 1)
            {
                double Cache = (R * F + F * G + R * G) * t * t * 4;

                int cache_kb = static_cast<int>(std::ceil(Cache / 1024));

                if (cache_kb <= 2048)
                {
                    double alpha = F < FIN ? 2 : 1;

                    double below = t * t * R * F + alpha * m * m * R * G;
                        // std::min({n * n * F * G + alpha * m * m * R * G,
                        //           t * t * R * F + alpha * m * m * R * G,
                        //           t * t * R * F + n * n * F * G});
                    double ratio = t * t * R * F * G * 2 / below;

                    max_ratio[cache_kb] = std::max(max_ratio[cache_kb], ratio);
                }
            }
        }
    }

    double max_so_far = 0;

    std::cout << "cache, ratio\n";

    for (int i = 0; i <= 1024; ++i)
    {
        max_so_far = std::max(max_so_far, max_ratio[i]);
        std::cout << i << ", " << max_so_far << "\n";
    }
}
