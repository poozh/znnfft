library(ggplot2)
library(wesanderson)
library(RColorBrewer)
library(gridExtra)
library(gtable)
library(grid)

d <- read.csv('ai.csv');

d$candr = reorder(d$candr, d$candr)

r <- subset(d, cgemm<1 & cache <= 1024);
c <- subset(d, cgemm>0 & cache <= 1024);

get_plot <- function(d, mytitle) {

    plot <- ggplot(data = d, aes(x = cache, y = ai, group = factor(candr),
                     color = factor(candr))) +
        geom_line() +
        scale_color_brewer("C & C'", palette="Set1") +
        labs(title=mytitle) +
        theme_minimal() +
        theme(axis.title.x=element_blank(),
              axis.title.y=element_blank());

    return (plot);}

rplot <- get_plot(r, "Real matrices");
cplot <- get_plot(c, "Complex matrices");

legend = gtable_filter(ggplotGrob(rplot), "guide-box")

allplots <- grid.arrange(arrangeGrob(rplot + theme(legend.position="none"),
                                     cplot + theme(legend.position="none"),
                                     nrow = 1),
                         nrow=1)


plotfname <- paste("/home/zlateski/Dropbox/MIT/ZNNfft/paper/__fig/__gemm_cgemm.pdf");
pdf(plotfname,width=6,height=3)

grid.arrange(allplots, legend,
             widths=c(0.85, 0.15),
             left = textGrob("Arithmetic Intensity",
                                         rot = 90,
                             gp = gpar(cex = 1.1)),
             bottom = textGrob("Cache size (kb)", gp = gpar(cex = 1.1)),
             nrow=1)

dev.off()
