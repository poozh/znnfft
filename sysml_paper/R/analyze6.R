library(ggplot2)


fft_speedup_fn <- function(b, fin, fout, d, h, w, kd, kh, kw, fd, fh, fw, wd, wh, ww) {
    return (function(ratio2) {
        ratio <- ratio2 * 4

        wtd <- wd + kd - 1
        wth <- wh + kh - 1
        wtw <- ww + kw - 1
        ftd <- fd + kd - 1
        fth <- fh + kh - 1
        ftw <- fw + kw - 1

        wactuald <- ceiling((d-kd+1)/wd) * wd + kd - 1
        wactualh <- ceiling((h-kh+1)/wh) * wh + kh - 1
        wactualw <- ceiling((w-kw+1)/ww) * ww + kw - 1
        factuald <- ceiling((d-kd+1)/fd) * fd + kd - 1
        factualh <- ceiling((h-kh+1)/fh) * fh + kh - 1
        factualw <- ceiling((w-kw+1)/fw) * fw + kw - 1

        wtiles <- b * ((wactuald-kd+1)/wd)*((wactualh-kh+1)/wh)*((wactualw-kw+1)/ww)
        ftiles <- b * ((factuald-kd+1)/fd)*((factualh-kh+1)/fh)*((factualw-kw+1)/fw)

        wmatrices <- wtd * wth * wtw
        fmatrices <- ftd * fth * (floor(ftw/2)+1)

        wgemmflops <- fin * fout * wtiles * wmatrices * 2
        fgemmflops <- fin * fout * ftiles * fmatrices * 2 * 4

        foutx <- pmin(fout, 128);
        finx <-  pmin(fin, 256);

        wmaxratio <-  pmin(2 * foutx * finx / (foutx + finx), ratio);

        fouty <- pmin(fout, 128);
        finy <-  pmin(fin, 128);

        fmaxratio <-  pmin(4 * fouty * finy / (fouty + finy), ratio);

        wmatmemory <- (fin + fout) * (wtiles * wmatrices) + wmatrices * fin * fout
        fmatmemory <- (fin + fout) * (ftiles * fmatrices) * 2 + fmatrices * fin * fout * 2

        wimagemem <- b * (fin + fout) * wactuald * wactualh * wactualw
        fimagemem <- b * (fin + fout) * factuald * factualh * factualw

        kermem <- fin * fout * kd * kh * kw

        wtranscompute <- wgemmflops / wmaxratio
        ftranscompute <- fgemmflops / fmaxratio

        wgemmtime <- pmax(wtranscompute, wmatmemory)
        fgemmtime <- pmax(ftranscompute, fmatmemory)

        return ((wgemmtime + wimagemem + kermem + wmatmemory)/(fgemmtime + fimagemem + kermem + fmatmemory))})}




fft_speedup_fn_find <- function(b, fin, fout, d, h, w, kd, kh, kw, fd, wd, wh, ww) {
    return (function (ratio) {
        res = fft_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, 8, 8, wd, wh, ww)(ratio)
        for (i in 9:27) {
            res = pmax(res, fft_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, i, i, wd, wh, ww)(ratio))}
        res2 = fft_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, 8, 8, wd, wh + 2, ww + 2)(ratio)
        for (i in 9:27) {
            res2 = pmax(res2, fft_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, i, i, wd, wh + 2, ww + 2)(ratio))}
        return (pmin(res,res2))})}


fft3_speedup_fn <- function(b, fin, fout, d, h, w, kd, kh, kw, fd, fh, fw, wd, wh, ww) {
    return (function(ratio2) {
        ratio <- ratio2 * 4;

        wtd <- wd + kd - 1
        wth <- wh + kh - 1
        wtw <- ww + kw - 1
        ftd <- fd + kd - 1
        fth <- fh + kh - 1
        ftw <- fw + kw - 1

        wactuald <- ceiling((d-kd+1)/wd) * wd + kd - 1
        wactualh <- ceiling((h-kh+1)/wh) * wh + kh - 1
        wactualw <- ceiling((w-kw+1)/ww) * ww + kw - 1
        factuald <- ceiling((d-kd+1)/fd) * fd + kd - 1
        factualh <- ceiling((h-kh+1)/fh) * fh + kh - 1
        factualw <- ceiling((w-kw+1)/fw) * fw + kw - 1

        woutd <- wactuald - kd + 1
        wouth <- wactualh - kh + 1
        woutw <- wactualw - kw + 1
        foutd <- factuald - kd + 1
        fouth <- factualh - kh + 1
        foutw <- factualw - kw + 1

        wtiles <- b * ((wactuald-kd+1)/wd)*((wactualh-kh+1)/wh)*((wactualw-kw+1)/ww)
        ftiles <- b * ((factuald-kd+1)/fd)*((factualh-kh+1)/fh)*((factualw-kw+1)/fw)

        wmatrices <- wtd * wth * wtw
        fmatrices <- ftd * fth * (floor(ftw/2)+1)

        wgemmflops <- fin * fout * wtiles * wmatrices * 2
        fgemmflops <- fin * fout * ftiles * fmatrices * 2 * 3

        foutx <- pmin(fout, 256);
        finx <-  pmin(fin, 256);

        maxratio <-  pmin(2 * foutx * finx / (foutx + finx), ratio);

        wmatmemory <- (fin + fout) * (wtiles * wmatrices) + wmatrices * fin * fout
        fmatmemory <- (fin + fout) * (ftiles * fmatrices) * 3 + fmatrices * fin * fout * 3

        wimagemem <- b * fin * wactuald * wactualh * wactualw + b * fout * woutd * wouth * woutw
        fimagemem <- b * fin * factuald * factualh * factualw + b * fout * foutd * fouth * foutw

        kermem <- fin * fout * kd * kh * kw

        wtranscompute <- wgemmflops / maxratio
        ftranscompute <- fgemmflops / maxratio

        wgemmtime <- pmax(wtranscompute, wmatmemory)
        fgemmtime <- pmax(ftranscompute, fmatmemory)

        return ((wgemmtime + wimagemem + kermem + wmatmemory)/(fgemmtime + fimagemem + kermem + fmatmemory))})}




fft3_speedup_fn_find <- function(b, fin, fout, d, h, w, kd, kh, kw, fd, wd, wh, ww) {
    return (function (ratio) {
        res = fft3_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, 8, 8, wd, wh, ww)(ratio)
        for (i in 9:27) {
            res = pmax(res, fft3_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, i, i, wd, wh, ww)(ratio))}
        res2 = fft3_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, 8, 8, wd, wh + 2, ww + 2)(ratio)
        for (i in 9:27) {
            res2 = pmax(res2, fft3_speedup_fn(b, fin, fout, d, h, w, kd, kh, kw, fd, i, i, wd, wh + 2, ww + 2)(ratio))}
        return (pmin(res,res2))})}


fft_speedup_fn_fwd<- function(b, fin, fout, d, h, w, kd, kh, kw, fd, fh, fw, wd, wh, ww) {
    return (function(ratio2) {
                ratio <- ratio2 * 4;

        wtd <- wd + kd - 1
        wth <- wh + kh - 1
        wtw <- ww + kw - 1
        ftd <- fd + kd - 1
        fth <- fh + kh - 1
        ftw <- fw + kw - 1

        wactuald <- ceiling((d-kd+1)/wd) * wd + kd - 1
        wactualh <- ceiling((h-kh+1)/wh) * wh + kh - 1
        wactualw <- ceiling((w-kw+1)/ww) * ww + kw - 1
        factuald <- ceiling((d-kd+1)/fd) * fd + kd - 1
        factualh <- ceiling((h-kh+1)/fh) * fh + kh - 1
        factualw <- ceiling((w-kw+1)/fw) * fw + kw - 1

        wtiles <- b * ((wactuald-kd+1)/wd)*((wactualh-kh+1)/wh)*((wactualw-kw+1)/ww)
        ftiles <- b * ((factuald-kd+1)/fd)*((factualh-kh+1)/fh)*((factualw-kw+1)/fw)

        wmatrices <- wtd * wth * wtw
        fmatrices <- ftd * fth * (floor(ftw/2)+1)

        wgemmflops <- fin * fout * wtiles * wmatrices * 2
        fgemmflops <- fin * fout * ftiles * fmatrices * 2 * 4

        wmatmemory <- (fin + fout) * (wtiles * wmatrices) + wmatrices * wtd * wth * wtw
        fmatmemory <- (fin + fout) * (ftiles * fmatrices) * 2 + fmatrices * ftd * fth * ftw * 2

        wmatmemory2 <- (fin + fout) * (wtiles * wmatrices)
        fmatmemory2 <- (fin + fout) * (ftiles * fmatrices)

        wimagemem <- b * (fin + fout) * wactuald * wactualh * wactualw
        fimagemem <- b * (fin + fout) * factuald * factualh * factualw

        kermem <- fin * fout * kd * kh * kw

        wtranscompute <- wgemmflops / ratio
        ftranscompute <- fgemmflops / ratio

        wgemmtime <- pmax(wtranscompute, wmatmemory)
        fgemmtime <- pmax(ftranscompute, fmatmemory)

        return ((wgemmtime + wimagemem + wmatmemory2)/(fgemmtime + fimagemem + fmatmemory2))})}

ratios=data.frame(ratio=c(11, 55/4, 14.06, 100/4, 22, 33), processor=c("KNL", "Titan X", "i7-6950X", "Titan X Pascal", "SKX", "Xeon 8180"), hgh=c(1.2,1.2,1.02,1.2,1.2,1.2))

colors = c("#e6194b", "#3cb44b", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#008080", "#d2f53c", "#fabebe",
           "#e6beff", "#aa6e28", "#fffac8", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000080", "#808080", "#ffe119", "#ffe119")

one=data.frame(on=c(1))


plotfname <- paste("/home/zlateski/Dropbox/MIT/ZNNfft/paper/fig/vgg_speedups.pdf");
pdf(plotfname,width=6,height=5)

ggplot(data.frame(x=c(1, 50)), aes(x)) +
    stat_function(fun=fft_speedup_fn_find(64, 64, 64, 1, 226, 226, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 1.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 64, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 256, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 128, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 256, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 512, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 512, 512, 1, 16, 16, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 5")) +
    stat_function(fun=fft3_speedup_fn_find(64, 64, 64, 1, 226, 226, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 1.2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 64, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.1"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 128, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 128, 256, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.1"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 128, 128, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 256, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.1"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 512, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 512, 512, 1, 16, 16, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 5"), linetype=2) +
    geom_vline(data=ratios, mapping=aes(xintercept=ratio), color="darkgray", linetype=4) +
    geom_text(data=ratios, mapping=aes(x=ratio, y=hgh, label=processor), size=4, angle=90, vjust=-0.4, hjust=0) +
    geom_hline(data=one, mapping=aes(yintercept=on), color="black") +
    scale_colour_manual("Layer", values = colors) +
    theme_minimal() +
    theme(legend.position="bottom") +
    labs(x = "Ratio of processors FLOPS to memory bandwidth", y = "Theoretical speedup of FFT vs Winograd")

dev.off()


e <- read.csv('empirical.csv')

plotfname <- paste("/home/zlateski/Dropbox/MIT/ZNNfft/paper/fig/vgg_speedups_with_empirical.pdf");
pdf(plotfname,width=6,height=5)

ggplot(data.frame(x=c(1, 50)), aes(x)) +
    stat_function(fun=fft_speedup_fn_find(64, 64, 64, 1, 226, 226, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 1.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 64, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 128, 1, 114, 114, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 2.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 256, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 128, 128, 1, 58, 58, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 3.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 256, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.1")) +
    stat_function(fun=fft_speedup_fn_find(64, 512, 512, 1, 30, 30, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 4.2")) +
    stat_function(fun=fft_speedup_fn_find(64, 512, 512, 1, 16, 16, 1, 3, 3, 1, 1, 4, 4), aes(colour="VGG 5")) +
    geom_line(data=e, aes(x=Ratio, y=VGG1.2, colour="VGG 1.2"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG1.2, colour="VGG 1.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG2.1, colour="VGG 2.1"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG2.1, colour="VGG 2.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG2.2, colour="VGG 2.2"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG2.2, colour="VGG 2.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG3.1, colour="VGG 3.1"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG3.1, colour="VGG 3.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG3.2, colour="VGG 3.2"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG3.2, colour="VGG 3.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG4.1, colour="VGG 4.1"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG4.1, colour="VGG 4.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG4.2, colour="VGG 4.2"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG4.2, colour="VGG 4.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG5, colour="VGG 5"), linetype=3) +
    geom_point(data=e, aes(x=Ratio, y=VGG5, colour="VGG 5"), size=2) +
    geom_vline(data=ratios, mapping=aes(xintercept=ratio), color="darkgray", linetype=4) +
    geom_text(data=ratios, mapping=aes(x=ratio, y=hgh, label=processor), size=4, angle=90, vjust=-0.4, hjust=0) +
    geom_hline(data=one, mapping=aes(yintercept=on), color="black") +
    scale_colour_manual("Layer", values = colors) +
    theme_minimal() +
    theme(legend.position="bottom") +
    labs(x = "Ratio of processors FLOPS to memory bandwidth", y = "Theoretical speedup of FFT vs Winograd")

dev.off()


plotfname <- paste("/home/zlateski/Dropbox/MIT/ZNNfft/paper/fig/vgg_speedups_empirical.pdf");
pdf(plotfname,width=6,height=5)

ggplot(data.frame(x=c(1, 50)), aes(x)) +
    geom_line(data=e, aes(x=Ratio, y=VGG1.2, colour="VGG 1.2"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG1.2, colour="VGG 1.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG2.1, colour="VGG 2.1"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG2.1, colour="VGG 2.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG2.2, colour="VGG 2.2"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG2.2, colour="VGG 2.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG3.1, colour="VGG 3.1"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG3.1, colour="VGG 3.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG3.2, colour="VGG 3.2"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG3.2, colour="VGG 3.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG4.1, colour="VGG 4.1"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG4.1, colour="VGG 4.1"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG4.2, colour="VGG 4.2"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG4.2, colour="VGG 4.2"), size=2) +
    geom_line(data=e, aes(x=Ratio, y=VGG5, colour="VGG 5"), linetype=1) +
    geom_point(data=e, aes(x=Ratio, y=VGG5, colour="VGG 5"), size=2) +
    geom_vline(data=ratios, mapping=aes(xintercept=ratio), color="darkgray", linetype=4) +
    geom_text(data=ratios, mapping=aes(x=ratio, y=hgh, label=processor), size=4, angle=90, vjust=-0.4, hjust=0) +
    geom_hline(data=one, mapping=aes(yintercept=on), color="black") +
    scale_colour_manual("Layer", values = colors) +
    theme_minimal() +
    theme(legend.position="bottom") +
    labs(x = "Ratio of processors FLOPS to memory bandwidth", y = "Theoretical speedup of FFT vs Winograd")

dev.off()



plotfname <- paste("/home/zlateski/Dropbox/MIT/ZNNfft/paper/fig/alexnet_overfeat_speedup.pdf");
pdf(plotfname,width=6,height=5)

ggplot(data.frame(x=c(1, 50)), aes(x)) +
    stat_function(fun=fft_speedup_fn_find(128, 96, 256, 1, 36, 36, 1, 7, 7, 1, 1, 2, 2), aes(colour="OverFeat 2")) +
    stat_function(fun=fft_speedup_fn_find(128, 256, 512, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 3")) +
    stat_function(fun=fft_speedup_fn_find(128, 512, 1024, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 4")) +
    stat_function(fun=fft_speedup_fn_find(128, 1024, 1024, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 5")) +
    stat_function(fun=fft3_speedup_fn_find(128, 96, 256, 1, 36, 36, 1, 7, 7, 1, 1, 2, 2), aes(colour="OverFeat 2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(128, 256, 512, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 3"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(128, 512, 1024, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 4"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(128, 1024, 1024, 1, 17, 17, 1, 3, 3, 1, 1, 4, 4), aes(colour="OverFeat 5"), linetype=2) +
    stat_function(fun=fft_speedup_fn_find(64, 64, 192, 1, 59, 59, 1, 5, 5, 1, 1, 2, 2), aes(colour="AlexNet 2")) +
    stat_function(fun=fft_speedup_fn_find(64, 192, 384, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 3")) +
    stat_function(fun=fft_speedup_fn_find(64, 384, 256, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 4")) +
    stat_function(fun=fft_speedup_fn_find(64, 256, 256, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 5")) +
    stat_function(fun=fft3_speedup_fn_find(64, 64, 192, 1, 59, 59, 1, 5, 5, 1, 1, 2, 2), aes(colour="AlexNet 2"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 192, 384, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 3"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 384, 256, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 4"), linetype=2) +
    stat_function(fun=fft3_speedup_fn_find(64, 256, 256, 1, 15, 15, 1, 3, 3, 1, 1, 4, 4), aes(colour="AlexNet 5"), linetype=2) +
    geom_vline(data=ratios, mapping=aes(xintercept=ratio), color="darkgray", linetype=4) +
    geom_text(data=ratios, mapping=aes(x=ratio, y=hgh, label=processor), size=4, angle=90, vjust=-0.4, hjust=0) +
    geom_hline(data=one, mapping=aes(yintercept=on), color="black") +
    scale_colour_manual("Layer", values = colors) +
    theme_minimal() +
    theme(legend.position="bottom") +
    labs(x = "Ratio of processors FLOPS to memory bandwidth", y = "Theoretical speedup of FFT vs Winograd")

dev.off()
