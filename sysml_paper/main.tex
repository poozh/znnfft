\documentclass[sigconf]{acmart}
\settopmatter{printacmref=false}
\renewcommand\footnotetextcopyrightpermission[1]{}
\pagestyle{plain}
\usepackage{booktabs} % For formal tables


\usepackage{nicefrac}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage{times}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{amssymb,amsmath}
\usepackage{booktabs}       % professional-quality tables
\usepackage{ifthen}
\usepackage{color}
\usepackage{xcolor}
\usepackage{eqnarray}
\usepackage{overpic}
\usepackage{xspace}
\usepackage[small,compact]{titlesec}
\usepackage[font={small}]{caption}  % small fonts in captions


% Copyright
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\setcopyright{usgov}
%\setcopyright{usgovmixed}
%\setcopyright{cagov}
%\setcopyright{cagovmixed}


\newboolean{showcomments}
\setboolean{showcomments}{true}
\ifthenelse{\boolean{showcomments}}
{ \newcommand{\mynote}[3]{
    \fbox{\bfseries\sffamily\scriptsize#1}
    {\small$\blacktriangleright$\textsf{\emph{\color{#3}{#2}}}$\blacktriangleleft$}}}
{ \newcommand{\mynote}[3]{}}


\newcommand{\ssymbol}[1]{^{#1}}
\newcommand{\stimes}{{\times}}
\newcommand*\rot[1]{\rotatebox[origin=c]{90}{#1}}


% One command per author:

\newcommand{\aleks}[1]{\mynote{Aleks}{#1}{red}}
\newcommand{\zhen}[1]{\mynote{Zhen}{#1}{blue}}
\newcommand{\kai}[1]{\mynote{Kai}{#1}{green}}
\newcommand{\fredo}[1]{\mynote{Fredo}{#1}{orange}}



\begin{document}
\title{A Deeper Look at FFT and Winograd Convolutions}

\author{ \normalsize \bf Aleksandar Zlateski$\ssymbol{*\dagger}$
  $\quad$Zhen Jia$\ssymbol{*\ddagger}\quad$Kai Li$\ssymbol{\ddagger}$
  $\quad$Fredo Durand$\ssymbol{\dagger}$\\ \normalsize
  $\ssymbol{\dagger}$Massachusetts Institute of
  Technology$\quad\ssymbol{\ddagger}$Princeton
  University\\ \normalsize $\ssymbol{*}${\bf Equally contributed}
}


%\begin{abstract}
%\end{abstract}

\maketitle

\subsection*{Motivation and Previous Work}

  Since convolutional layers are computationally expensive and
  dominate the total execution time of modern deep ConvNets
  \cite{szegedy2015going,
    krizhevsky2012imagenet,montufar2014number,simonyan2014very}, many
  efforts have been made to improve the performance of the
  convolutional primitives for CPUs~\cite{zlateski2016znn,
    vanhoucke2011improving, budden2016deep, falconlib,
    zlateski2017compile}, GPUs~\cite{chetlur2014cudnn,
    mathieu2013fast, vasilache2014fast, neon} or both
  \cite{zlateski2016znni}.  Initially, several approaches using
  FFT--based convolutions were proposed~\cite{vasilache2014fast,
    mathieu2013fast, zlateski2016znn, zlateski2016znni}.  Recent work
  by Lavin et al. on Winograd--based
  convolutions~\cite{lavin2016fast} demonstrated a great speedup,
  which shifted the focus from FFT--based to Winograd--based
  implementations, as it became widely accepted that the
  Winograd--based approach provides greater reduction in the number of
  operations required by the algorithm, especially for small kernels
  (e.g. $3 \times 3$).  A well optimized manycore CPU implementation~\cite{wConv,winograd} of the Winograd approach can improve the performances by more than 3X.

\begin{comment}
  However,
  while the traditional method of using time complexity for estimating
  run times suggests that reduction in the number of required
  operations should speedup the execution, it ignores many other
  factors, such as the memory bandwidth and cache sizes, which can
  have a great impact on running times on modern systems.
\end{comment}

  The main reduction in operations in the Winograd method, compared to
  FFT, comes from the fact that it works with real numbers.  However,
  due to its numerical instability, the Winograd method can only use
  small tile (transform) sizes~\cite{lavin2016fast,
    vincent2017improving, budden2016deep}, which result in a larger
  amount of required data movement to and from memory.  In contrast,
  the FFT--based method does not suffer from such instability, thus
  larger tile sizes can be used, which can partially reduce the number
  of required operations and greatly reduce the amount of data
  movements; these savings can, in certain cases, offset the increase
  in the number of operations due to complex arithmetic.

  These observations raise the question, under what conditions the
  Winograd-based approach performs better than the FFT--based approach
  and vice versa, and how to compare the two approaches.


  \subsection*{Our Contributions and Novelty}

  In this paper, we propose a performance model based on the idea of Roofline~\cite{williams2008roofline} that, in addition to the number of
  operations, accounts for the total amount of data movement (to
  and from memory), as well as the arithmetic intensity (operations
  per moved byte) to compare the Winograd-based and FFT-based approach.  To estimate the run times, alongside the processor's
  speed, our model also considers memory bandwidth and the cache
  sizes.  We have also compared optimized implementations of both approaches on the same hardware.

  Our model is suitable for modern processors, both CPUs and GPUs,
  which tend to have large compute--to--memory ratios, the ratio
  between the speed (in FLOPS) and memory bandwidth.  Both compute speed
  and memory bandwidth are improving exponentially. However, the
  exponent for speed is substantially larger than that for memory
  bandwidth~\cite{wulf1995hitting}, which results in an increasing
  compute--to--memory ratio.  For instance, the $4.5$ TFLOPS Intel
  Knights Landing processor~\cite{jeffers2016intel} has a compute--to--memory ratio of $11$,
  and the latest Skylake Xeon processor family has ratios in the
  range of $20$ to $30$. Titan Xp GPU from NVIDIA has a ratio of
  $20$, and Tesla V100 has a ratio of $16$ for single precision
  computation, and a ratio of $133$ for half--precision computation
  using its dedicated tensor cores.

  Our model suggests that whether the Winograd or FFT approach is
  faster depends on both the layer and hardware it is executed on.
  However, on average, the FFT--based approach outperforms the Winograd--based ones on most commonly used networks, with the margin
  increasing as the system's compute--to--memory ratio increases.

  Through a set of empirical experiments, we confirm the predictions of our model on modern CPUs.

  \subsection*{Performance Model}

  The Roofline model~\cite{williams2008roofline} estimates
  an application's upper bound performance as a function of its
  arithmetic intensity, derived by dividing the number
  of floating point operations by the total data movement.  The
  runtime ($T$) is estimated by:
  \begin{equation} \label{eq:runtime} \small %\scriptsize
    \begin{aligned}
      T &= \frac{\text{FPO}}{\min(\text{Peak FLOPS}, \text{~AI} \times \text{MB})}
    \end{aligned}
  \end{equation}
  Where $MB$ indicates the memory bandwidth; $AI$ the
  arithmetic intensity and $FPO$ is short for the total number of
  required floating point operations.  In our model, data movement is
  defined as movement between main memory and any of the caches.  We focus on
  32-bit floating point arithmetic, similar analysis can be performed for
  16-bit half-precision floats.

  Both FFT--based and Winograd--based approaches perform a change of
  basis of the inputs and kernels, in which convolution becomes a
  multiplication.  Both approaches contain four
  stages~\cite{vasilache2014fast, mathieu2013fast, lavin2016fast,
    winograd}: \textcircled{1} transforming input images, \textcircled{2} transforming
  kernels, \textcircled{3} performing element--wise products of the transformed
  images and kernels, which is an equivalent problem to a matrix
  multiplication (with real matrices for Winograd, and complex
  matrices for FFT), and \textcircled{4} transforming the results of \textcircled{3} back to
  the spatial domain.

  \begin{figure*}[t]
    \begin{minipage}[t][][b]{0.6\textwidth}
      \centering
      \includegraphics[width=\textwidth]{fig/__theory_4.pdf}
      \vspace{-4.2ex}
      \caption{Estimated and empirically obtained speedup of FFT--based
        convolutions versus Winograd--based ones on CPUs with various
        compute--to--memory ratios and cache sizes.}
      \label{fig:combined-2d}
    \end{minipage}
    \hspace{2ex}
    \begin{minipage}[t][][b]{0.37\textwidth}
      \resizebox{\textwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{fig/benchs.pdf}
      }
      \vspace{-3.1ex}
      \caption{Absolute performances of our Winograd-- and FFT-- based
        implementations compared to the latest version of MKL-DNN and
        LIBXSMM.}
      \label{fig:benchs}
    \end{minipage}
  \end{figure*}

  The time required for each stage can be estimated using
  Eqn.~\ref{eq:runtime}.  The transform stages
  (\textcircled{1},\textcircled{2},\textcircled{4})
  have very low arithmetic intensity -- lower than 2.5 for Winograd
  and lower than 3.5 for FFT, and will thus be memory bound on all
  modern systems.  In this case Eqn.~\ref{eq:runtime} can be
  reduced to $T = \frac{DM}{MB}$, where $DM$ represents data movement in
  bytes.

  The element--wise product stage will generally have a larger $AI$,
  which will depend on the number of layer's input and output channels
  and the amount of available cache.  However, the FFT--based approach
  will typically have the $AI$ twice as large as its Winograd counterpart,
  due to working in the complex domain.  In some cases, on machines with
  large compute--to--memory ratios, the Winograd method will be memory
  bound, while the FFT method is compute bound.

  \paragraph{Relative Performances}

  We are interested in the relative performances of the two approaches. We
  define the speedup $\alpha =
  \nicefrac{\sum_{S}T^W_s}{\sum_{S}T^F_s}$ to be the ratio of the time
  required for processing a layer using the Winograd approach and the
  time required using the FFT approach, each of which is estimated to be
  equal to the sum of times required for all four stages.  When
  $\alpha$ is greater than one, the FFT method is expected to run
  faster.

  The speed of both methods will depend on the tile sizes used.  For
  Winograd, due to its numerical instability, we allow tile sizes up
  to $6 \times 6$, as larger tile sizes can increase numerical errors by two or
  more orders of magnitude, when compared to direct
  convolution~\cite{lavin2016fast, winograd}, resulting in an unstable computation.  This limit is also enforced by the
  implementations, such as
  MKL-DNN~\cite{mkl-dnn}, LIBXSMM~\cite{heinecke2016libxsmm,
    libxsmmGit}, and cuDNN~\cite{chetlur2014cudnn}.  For the FFT
  method we allow for an arbitrary tile size, however, using tiles
  larger than $64^2$ is never optimal.  Using the methodology
  from~\cite{lavin2016fast}, we estimate that the FFT method has lower
  error than direct convolution for all tile sizes up to $64^2$.

  \subsection*{Implementation and Empirical Results}

  To empirically confirm the estimates of our model on modern systems, we
  used the fastest available implementation of the Winograd--based
  approach~\cite{wConv, winograd} which uses ``wincnn''~\cite{wincnn} to
  generate transform codelets and just--in--time (JIT) compiled
  primitives for fast matrix multiplication on AVX512 capable
  machines.  We extended the implementation to support AVX2, and
  implemented an FFT--based approach by replacing the ``wincnn''
  primitives, with ones generated by ``genfft'' from the fftw
  package~\cite{frigo1998fftw}, as well as implemented JIT primitives
  for complex matrix multiplication.

  In Fig.~\ref{fig:combined-2d} we show the empirically measured
  values for the speedup of the FFT--based approach over the Winograd--based one, together with the theoretical estimates of our model for
  forward propagation of the layers of VGG~\cite{simonyan2014very} network.  The color represents the amount
  of L2 cache available (note that the lines sometimes overlap).
  A total of $7$ system configurations were used, two of which were AVX2--based and four AVX512.  The slowest CPU was capable of approximately
  $1$ TFLOPS (Intel i7-6950X desktop CPU), while the fastest one was
  capable of $4.5$ TFLOPS (Intel Xeon Phi 7210).  The oldest
  generation of CPUs was Intel E7-8890v3 with $1.44$ TFLOPS and only
  $256$KB of L2 cache, whereas the other CPUs had either $512$ or
  $1024$ KB of cache per core.  The position on the $x$ axis was determined by dividing processors' speed
  (in FLOPS), by theoretically achievable memory bandwidth.

  As our model suggests, and our empirical results confirm, for
  some of the layers of VGG, the FFT approach is faster and for some
  the Winograd one is faster.  The model was also confirmed on
  AlexNet~\cite{krizhevsky2012imagenet} and OverFeat~\cite{sermanet2013overfeat}
  layers (not presented) where
  FFT was an optimal choice for all layers.

  For credibility, we compare absolute performances of our
  implementations against two state--of--the--art implementations of
  Winograd--based convolutions (latest MKL-DNN and LIBXSMM, as January
  2018) and the fastest implementation of direct based convolution
  (MKL-DNN).  Fig.~\ref{fig:benchs} shows the performances of our two
  implementations against other implementations on a $2$ TFLOPS Intel
  i9-7900X processors based system (compute--to--memory ratio of 22).
  Our Winograd
  implementation outperformed on $5$ out of $7$ CPUs that
  supported AVX512 (MKL-DNN and LIBXSMM do not provide an AVX2 implementation).
  The relative performances of our FFT
  implementation was consistent with our model.

  \subsection*{Conclusion and Discussion}

  While on popular neural networks, the FFT approach outperforms on average.
  The choice of the algorithm should, perhaps, be decided on a per layer
  basis.

  Generally, the relative performance of FFT over Winograd
  increases in the following scenarios: (1) as the compute--to--memory ratio increases, (2) when the cache size is limited, (3) when the image
  sizes are large, (4) when the number of input/output channels is relatively small.

  Another important finding is that no specific sizes of FFT are
  required, in contrast to the popular belief that
  power--of--two sizes should be used.  The arithmetic intensity of
  FFT transforms are much lower than the compute--to--memory ratios of
  modern processors, even for large prime numbers, and are thus memory
  bound.  Using non--typical FFT sizes, such as large prime numbers
  can, in certain cases, greatly improve the performances, as it can
  minimize the amount of required padding, and thus unnecessary
  computation, when dividing images into tiles.

  Another FFT--based approach, using Gauss' complex number multiplication,
  as proposed in~\cite{lavin2016fast}, was analyzed, implemented and
  empirically confirmed.  This approach reduced the amount of
  operations with the expense of more data movement, and for certain
  layers and on certain systems it can outperform both the Winograd
  and regular FFT--based approach.


\bibliographystyle{ACM-Reference-Format}
\bibliography{znnfft}

\section*{Acknowledgments}

  We thank Sebastian Seung for helpful discussions, and Nir Shavit for providing
  an access to a Skylake Xeon Gold Server.  We are grateful to Toyota Research
  Institute for supporting the Toyota - CSAIL Joint Research Center at MIT, and Intel
  Corporation for supporting the Intel Parallel Computing Center at Princeton University.
  Zhen Jia was partially supported by IARPA (D16PC00005).

\end{document}
