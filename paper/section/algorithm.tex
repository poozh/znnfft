\section{Implementations} \label{sec:implement}

 The basis for our implementation is the fastest publicly available implementation for Winograd
  convolution~\cite{wConv, winograd}, optimized for
  many--core CPUs.  We modified the implementation to support both
  AVX2 and AVX512 CPUs.  It also served as a basis for the
  implementations of our FFT--based methods, in order to reuse code and leverage the same optimization methods.
	We outline the major differences below.


  We adopted the data layout proposed in~\cite{winograd,
    jeffers2016intel, zlateski2017compile} for input images, kernels
  and output, where $S$ images are interleaved in memory for easy
  vectorization.  In~\cite{winograd} $S$ was set to $16$ due to the
  size of the AVX512 vector register, we keep $S$ as $16$ regardless
  of the vector register size, as $16$ is the cache--line width (16
  32--bit floats), to facilitate efficient utilization of the memory
  subsystem.

  For data hand--off between the four stages of the algorithm,
  non--temporal, streaming stores, to main memory were used, as the
  data will not be used in near future.  This saves memory bandwidth
  and avoids cache pollution.  Non--temporal stores are counted as
  data moments in out model.

  \paragraph{Transforms}

  To perform transforms of the input images and kernels, as well as
  the output images, the implementation of~\cite{winograd} provides
  c++ codelets that perform $S = 16$ tiles at the same time.  The
  codelets are created by generating Winograd transforms using
  Wincnn~\cite{wincnn}, after which a computation graph is created and
  optimized, yielding codelets that utilize AVX512 instructions to
  transform 16 tiles at the time.

  For the FFT--based implementations, the codelets were replaced by
  c++ codelets generated using ``genfft'' supplied with
  FFTW~\cite{frigo1998fftw}.  ``Genfft'' was modified so that it can
  generate codelets that perform implicitly zero--padded FFT
  transforms, as well as computing only a subset of elements of the
  inverse transform.  Multidimensional (forward) transforms were
  implemented by combining codelets performing implicitly zero--padded
  real--to--complex transforms along one dimension, and ones
  performing complex--to--complex transforms along other dimensions.
  Backward transforms combined complex--to--complex transform codelets
  and complex--to--real ones.

  To support AVX2 for both Winograd and FFT transforms, we have
  replaced each AVX512 instruction that operates on $16$ floating
  point numbers with two AVX2 instructions that operate on $8$
  numbers.

  \paragraph{Element--wise Multiplications}

  For the element--wise stage, where matrix--matrix multiplications
  are performed, the implementation of~\cite{winograd} provides JIT
  routines for real--matrix multiplications optimized for AVX512
  instruction set.  Following the same principles of~\cite{winograd}
  we implemented JIT real--matrix multiplication routines optimized
  for the AVX2 instruction set, as well as complex--valued
  multiplication routines for both AVX512 and AVX2 instruction sets,
  which are required for the Gouss--FFT method.

  \paragraph{Parallelization Through Static Scheduling}

  Each of the stages of our algorithm is parallelized using static
  scheduling originally proposed in~\cite{zlateski2017compile}, using
  the generalized implementation provided by~\cite{winograd}. To
  achieve optimal performance, each core is assigned roughly the same
  amount of computation.  The work is then executed using a single
  fork--join routine.
