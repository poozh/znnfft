\section{Experiments}

  \begin{table}
    \centering
    \scriptsize
    \caption{Machine configurations.} %
    \begin{tabular}{p{63pt} |p{10pt}p{18pt}p{9pt}p{20pt}p{24pt}p{8pt}}
      \toprule
      CPU & Cores & GFLOPS & AVX & Cache & MB{\tiny(GB/s)} & CMR \\
      \midrule
      Xeon Phi 7210 & 64 & 4506 & 512 & 512 KB & 409.6 & 11 \\
      i7-6950X & 10 & 960 & 2 & 1 MB & 68.3  & 14.06 \\
      i9-7900X & 10 & 2122 & 512 & 1 MB & 96  & 22 \\
      Xeon Gold 6148 & 20 & 3072 & 512 & 1 MB & 128 & 24 \\
      E7-8890v3 & 18 & 1440 & 2 & 256 KB & 51.2 & 28.13 \\
      Xeon Platinum 8124M & 18 & 3456 & 512 & 1MB & 115.2 & 30 \\
      i9-7900X & 10 & 2122 & 512 & 1 MB & 68.3  & 31 \\
      Xeon Phi 7210 & 48 & 4506 & 512 & 512 KB & 102.4 & 33 \\
      Xeon Phi 7210 & 64 & 4506 & 512 & 512 KB & 102.4  & 39.11 \\
      i9-7900X & 10 & 2122 & 512 & 1 MB & 51.2  & 41.25 \\
      \bottomrule
    \end{tabular}
    \label{table:machines}
  \end{table}

  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=1\linewidth]{__fig/vs_6.pdf}
    \end{center}
    \caption{Theoretical estimates of our model, and empirical
      measurements for the speedup of Regular-- and Gauss--FFT methods
      over the Winograd method on VGG and AlexNet.}
    \label{fig:model}
  \end{figure*}

  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=1\linewidth]{__fig/__benchs.pdf}
    \end{center}
    \caption{Convolution layers' runtime with different
      implementations.}
    \label{fig:combined-2d}
  \end{figure*}


  Through empirical experiments, in this section, we address the
  following questions:

  \begin{itemize}[noitemsep,leftmargin=*]
  \item How accurate is our model?
  \item What is the performance of our implementations comparing with
    other state--of--the--art implementations?
  \item What is the precision loss of the Winograd convolution for
    various $\WINs(m^2,r^2)$, and of the Regular--/Gauss--FFT
    convolution for various $\FFTs(m^2,r^2)$ and $\FFTGs(m^2,r^2)$?
  \end{itemize}

  To answer these questions, we benchmarked our implementation on a
  total of $6$ different CPUs, and total of $10$ system configurations
  (Tbl.~\ref{table:machines}).  Some configurations used the same CPU
  but memory with different bandwidths.

  \begin{table} \centering
    \scriptsize
    \setlength\tabcolsep{2.5pt}
    \caption{Benchmarked convolutional layers.}
    \begin{tabular}{cr | cccccc  }
      \toprule
      &  & B & C & C' & Image Size & Padding & Kernel Size  \\
      \midrule
      \multirow{5}{*}{\rotatebox{90}{\textbf{VGG}}}
      & 1.2 & 64  & 64  &  64  & $\angled{224,224}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 2.1 & 64  & 64  &  128 & $\angled{112,112}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 2.2 & 64  & 128 &  128 & $\angled{112,112}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 3.1 & 64  & 128 &  256 & $\angled{56,56}$   & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 3.2 & 64  & 256 &  256 & $\angled{56,56}$   & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 4.1 & 64  & 256 &  512 & $\angled{28,28}$   & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 4.2 & 64  & 512 &  512 & $\angled{28,28}$   & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 5.x & 64  & 512 &  512 & $\angled{14,14}$   & $\angled{1,1}$ & $\angled{3,3}$ \\
      \midrule
      \multirow{4}{*}{\rotatebox{90}{\textbf{AlexNet}}}
      & 2 & 128 & 64  &  192 & $\angled{31,31}$ & $\angled{0,0}$ & $\angled{5,5}$ \\
      & 3 & 128 & 192 &  384 & $\angled{13,13}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 4 & 128 & 384 &  256 & $\angled{13,13}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      & 5 & 128 & 256 &  256 & $\angled{13,13}$ & $\angled{1,1}$ & $\angled{3,3}$ \\
      \bottomrule
    \end{tabular}
    \label{table:layers}
  \end{table}


 \subsection{Model Validation}

  We have validated the accuracy of our model with two popular ConvNets, VGG~\cite{simonyan2014very} and
  AlexNet~\cite{krizhevsky2012imagenet} which are frequently used for
  benchmarking~\cite{imagenetBench}.  All unique layers of the two
  networks, whose details are shown in Tbl~\ref{table:layers}, were
  benchmarked, by measuring the time required for computing the result
  of the forward propagation, on all of our systems.
	
  Figure~\ref{fig:model} shows the results of our empirical
  measurements, overlaid over the theoretical prediction of our model.
  The relative performances of the Regular--FFT and Gauss--FFT
  convolutions against Winograd convolution are shown.  The relative
  performance, is computed as the ratio of the time required for
  processing the layer using the Winograd and an FFT approach.  The
  colors of the lines represent the cache size of the target system,
  which is one of the parameters of our model.  The three lines are
  drown with semi--translucent colors, as at many locations they
  overlap.

  The solid line shows the theoretical prediction derived by our
  model, whereas the points (crosshairs) represent the empirically
  measured values - the ratios of the time spent by the Winograd
  implementation and the appropriate FFT implementation.  The position
  on the $x$ axis of each point is determined by the target system's
  compute--to--memory ratio, and the color is determined by the target
  system's cache size.

  The results of both Regular-- and Gauss--FFT show that our empirical
  results are consistent with the theoretical prediction of our model.
  The overall relative root mean square error (\textbf{rRMSE}) was
  0.079 for Regular--FFT vs Winograd and 0.1 for Gauss--FFT vs
  Winograd.  The fitness\footnote{$\text{fitness} = \frac{100}{1 +
      rRMSE}$} was $92.68\%$ for Regular--FFT vs Winograd and $90\%$
  for Gauss--FFT vs Winograd, indicating that our model is very
  good/excellent.

  The discrepancies are the largest close to where the prediction has
  higher slope.  This is reasonable, as our model assumes perfect
  utilization.  In practice, the utilization varied slightly among
  methods, layers and machines.

  In the presented results, the maximal tile size for the Winograd
  method was set to $6 \times 6$, as it is the size used in all
  industry libraries \cite{mkl-dnn, libxsmmGit, falconlib,
    chetlur2014cudnn}.  Additional results, with tile sizes allowed to
  be larger, as well as additional comparisons of Regular--FFT vs
  Gauss--FFT, are provided in the appendix.  The tile of the FFT
  methods were allowed to take an arbitrary size, however only tile
  sizes up to $32 \times 32$ were benchmarked, as our model predicts
  the larger sizes to be much less efficient.

  \subsection{Performances Against State--of--the--art}

  We have compared our implementation with other publicly available
  Winograd implementations for CPUs, including
  LIBXSMM~\cite{libxsmmGit}, and MKL-DNN~\cite{mkl-dnn}. As well as a
  direct--convolution implementations provided by MKL-DNN.  No
  efficient implementations of FFT--based approaches for CPUs, other
  than ours, are currently available.  The benchmarks of the popular
  VGG network were performed on a i9-7900X Skylake X CPU with memory
  bandwidth of $96 GB/s$, the third one in Tbl.~\ref{table:machines}.

  The results are shown on Fig.~\ref{fig:combined-2d}.  Additional
  benchmarks of other and on other processors are provided in the
  appendix.  Our implementations consistently outperformed other
  publicly available ones across different layers and different
  systems.  This confirms that we have used the fastest available
  Winograd implementation, described and implemented
  by~\cite{winograd}, as well as that our FFT implementations are as
  efficient.

  \subsection{Accuracy} \label{sub_sec_accuracy}

  We have estimated the numerical errors for various sizes of
  $\WINs(m^2,r^2)$, $\FFTs(m^2,r^2)$, and $\FFTGs(m^2,r^2)$.
  Tbl.~\ref{table:accuracy-train} shows the maximal and average
  measured errors of the VGG network, using the method proposed
  in~\cite{winograd}.  The ground truth was estimated using a direct
  convolution algorithm and ``long doubles'' (extended precision
  floating point numbers).  Other methods used 32--bit floating point
  numbers.  The input image pixel values were drawn from an uniform
  distribution [-1,1].  The kernels were initialized using the
  standard Xavier initialization method.~\cite{Xavier}

  \begin{table}
    \begin{center}
      \setlength\tabcolsep{2pt}
      \scriptsize
      \caption{Maximum and average element errors of convolution
        layers for the three methods.  The maximal error of direct
        convolution is 1.11E-06 and average 3.32E-08.}
      \label{table:accuracy-train}
      \begin{tabular}{c|cccccc}
        \toprule
        Winograd & F($2^2$,$3^2$) & F($4^2$,$3^2$) & F($5^2$,$3^2$) & F($6^2$,$3^2$) & F($7^2$,$3^2$) & F($8^2$,$3^2$) \\
        \midrule
        Max     & 3.42e-07  &  7.03e-06  &  2.72e-04  &  1.24e-03  &  1.52e-01  &  1.05e+00  \\
        Average & 2.44e-07  &  4.55e-06  &  1.74e-04  &  4.97e-04  &  2.43e-04  &  4.55e-01 \\
        \midrule
        Regular--FFT & $\FFTs$($6^2$,$3^2$) & $\FFTs$($14^2$,$3^2$) &$\FFTs$($30^2$,$3^2$) & $\FFTs$($62^2$,$3^2$) & $\FFTs$($15^2$,$3^2$)  & $\FFTs$($27^2$,$3^2$) \\
        \midrule
        Max     & 2.30e-07  &  2.30e-07  &  2.37e-07  &  2.56e-07  &  2.48e-07  &  2.88e-07 \\
        Average & 1.92e-08  &  2.02e-08  &  2.21e-08  &  2.62e-08  &  2.21e-08  &  2.53e-08 \\
        \midrule
        Gauss--FFT & $\FFTGs$($6^2$,$3^2$) & $\FFTGs$($14^2$,$3^2$) &$\FFTGs$($30^2$,$3^2$) & $\FFTGs$($62^2$,$3^2$) & $\FFTGs$($15^2$,$3^2$)  & $\FFTGs$($27^2$,$3^2$) \\
        \midrule
        Max     & 2.27e-07  &  2.25e-07  &  2.31e-07  &  2.51e-07  &  2.38e-07  &  2.48e-07 \\
        Average & 1.85e-08  &  1.96e-08  &  2.18e-08  &  2.57e-08  &  2.18e-08  &  2.49e-08 \\
        \bottomrule
      \end{tabular}
    \end{center}
  \end{table}

Our measurements show that the FFT approaches do not suffer from the
  precision loss, even for very big tile sizes. They are
  slightly more accurate compared to direct convolution.

  The precision of the Wiongrad method degrades exponentially with
  the tile sizes increase.  Industry standard libraries seem to use the tile
  sizes up to $6 \times 6$ ($\WINs(4^2,3^2)$ when the kernel size is
  $3 \times 3$), as the numerical errors are comparable to the one of
  direct convolution.  As evidence suggests~\cite{courbariaux2014low,
    gupta2015deep} that, in certain cases, errors under e-02 do not
  affect stability, we provide additional results, with larger
  Winograd tiles allowed, in the appendix.
