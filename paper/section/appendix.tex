\appendix

\section{Fused Methods} \label{app-fused-non-fused}

  \begin{table*} \centering
    \footnotesize
    \setlength{\extrarowheight}{15pt}%
    \caption{Awesome caption }
    \begin{tabular}{l l l}
      \toprule
      Method & Runtime & Subject to \\
      \midrule
      Winograd & $ \dfrac{1}{\text{MB}}BN \dfrac{CC'}{cc'} \Bigg[ \max \Bigg\{ \dfrac{c\rho \WINs^I(m^2,r^2) }{\text{CMR}}, 4c\rho t^2  \Bigg\} + \dfrac{2t^2\rho cc'}{\text{CMR}} + \max \Bigg\{ \dfrac{c'\rho \WINs^O(m^2,r^2)}{\text{CMR}}, 4\alpha c' \rho m^2  \Bigg\}  \Bigg]$ & $4Tcc' \le \dfrac{\text{Cache}}{2} ~\text{,}~ 1 \le t \le 6 (8)$ \\
      %\bottomrule
      Regular--FFT & $ \dfrac{1}{\text{MB}}BN \dfrac{CC'}{cc'} \Bigg[ \max \Bigg\{ \dfrac{c\rho \FFTs^I(m^2,r^2) }{\text{CMR}}, 4c\rho t^2  \Bigg\} + \dfrac{8t\ceil{(t+1)/2}\rho cc'}{\text{CMR}} + \max \Bigg\{ \dfrac{c'\rho \FFTs^O(m^2,r^2)}{\text{CMR}}, 4\alpha c' \rho m^2  \Bigg\}  \Bigg]$ & $8Tcc' \le \dfrac{\text{Cache}}{2} ~\text{and}~ t$ \\
      Gauss--FFT & $ \dfrac{1}{\text{MB}}BN \dfrac{CC'}{cc'} \Bigg[ \max \Bigg\{ \dfrac{c\rho \FFTGs^I(m^2,r^2) }{\text{CMR}}, 4c\rho t^2  \Bigg\} + \dfrac{6t\ceil{(t+1)/2}\rho cc'}{\text{CMR}} + \max \Bigg\{ \dfrac{c'\rho \FFTGs^O(m^2,r^2)}{\text{CMR}}, 4\alpha c' \rho m^2  \Bigg\}  \Bigg]$ & $12Tcc' \le \dfrac{\text{Cache}}{2} ~\text{and}~ t$ \\
      \bottomrule
    \end{tabular}
    \label{table:fused-stuff}
  \end{table*}

  In both the Winograd and the FFT methods, matrices $U_i$ and $V_i$
  are created by transforming input tiles and kernels, respectively.
  The matrices $X_i = U_i \times V_i$ are computed, and finally, the
  result of the convolutional layer is obtained by performing the
  inverse transforms on the elements of the matrices $X_i$.

  For the rest of the section, we will drop the index $_i$ in the
  notation of $U_i$, $V_i$ and $X_i$.

  We focus on analyzing of the runtime of the fused approach for the
  Winograd method.  Analysis of the FFT methods should be straight
  forward.  The final runtimes for all methods are given in
  Tbl.~\ref{table:fused-stuff}.

  The number of matrices $U$, $V$, and $X$ depend on the tile size.
  For the Winograd method, total of $T = t^2$ real matrices are
  created.  The dimension of each matrix $U$ is $BCN \times C$, where
  $N$ is the total number of tiles per image $N = \ceil{(x-r+1)/m}^2$
  (with $x^2$ being the image size).  The dimensions of $V$ and $X$
  are $C \times C'$ and $BCN \times C'$ respectively.

  In the non--fused case, the matrices $U$ and $V$ were pre--computed
  and stored in the main memory.  As these matrices are large, the
  products $X = U \times V$ were computed using standard cache
  blocking techniques in order to maximize the arithmetic intensity,
  and minimize the computation time.

  In the fused approach, similar cache blocking strategies are
  employed, however, the sub--matrices of $U$ and $V$ are computed
  on--demand, and the results of their products are inverse
  transformed before storing back to main memory.  Unlike the
  non--fused approach, where the $T$ matrix--matrix product are
  computed one at a time, in the fused approach, all $T$ products have
  to be computed at the same time.

  The input image tile transforms have $T$ elements, corresponding to
  elements of $T = t^2$ matrices $U$, one element for each matrix, at
  the same location.  Similarly, the kernel transforms produce the
  same number of elements ($T$), one for each matrix $V$ at a
  particular location.  Finally, the inverse transforms consider
  elements at the same location of the matrices $X$ in order to
  perform the inverse transform.

  The matrices $V$ are subdivided into equally sized matrices
  $v_{i,j}$ of size $c \times c'$, and $U$ into sub--matrices $u_k$ of
  size $BN \times c$.  However, none of the full matrices $U$, $V$, or
  $X$ are explicitly computed.  Instead, $T$ sub--matrices $v_{i,j}$
  are computed by performing transforms on $cc'$ kernels, and the
  result is kept in cache.  A few ($\rho$) rows of each of the $T$
  matrices $u_i$ are then computed by transforming appropriate $\rho
  c$ input tiles.  The $T$ matrix products are then computed
  in--cache, yielding $T$ matrices of size $rho \times c'$.  The
  inverse transform is then performed, producing $\rho c'$ tiles of
  the output.  When $c = C$ this tiles represent the final result, and
  when $c < C$, they have to be accumulated to the previously computed
  tiles at the same location of the output images.

  Such multiplications with $T$ matrices $v_{i,j}$ stored in cache is
  performed total of $BN/\rho$ times, which is typically very large.
  Thus, we neglect the time required to create the $T$ matrices
  $v_{i,j}$.

  Transforming the $\rho c$ input tiles require $\rho c
  \WINs^I(m^2,r^2)$ operations, and at least $4\rho c m^2$ bytes of
  data movement (The input image tiles have size of $t^2$, but due to
  the overlap, at least $m^2$ unique elements for adjacent tiles need
  to be transferred to cache).  The runtime required for transforming
  $\rho$ rows of $u_i$ can be computed, using our Roofline model, via:

  \begin{equation} \label{eq:fused_prob}
    \small
    \begin{aligned}
      \max \Bigg\{ \dfrac{\rho c \WINs^I(m^2,r^2) }{\text{Peak FLOPS}}, \dfrac{4 \rho c m^2}{\text{MB}}  \Bigg\}
    \end{aligned}
  \end{equation}

  The $T$ matrix--matrix multiplications are then performed in--cache.
  The total runtime is then:

  \begin{equation} \label{eq:fused_prob}
    \small
    \begin{aligned}
      \frac{2 T \rho cc'}{\text{Peak FLOPS}}
    \end{aligned}
  \end{equation}

  Performing the inverse transforms requires $\rho c'
  \WINs^O(m^2,r^2)$ operations.  Total of $4\rho c' m^2$ bytes of the
  result, have to be then transferred to main memory.  However, when
  $c < C$, these results have to be accumulated to the previously
  computed ones, resulting in twice the amount of data movements.  The
  time required for the output transforms is then computed via:

  \begin{equation} \label{eq:fused_prob}
    \small
    \begin{aligned}
      \max \Bigg\{ \dfrac{\rho c' \WINs^O(m^2,r^2) }{\text{Peak FLOPS}}, \dfrac{4\alpha \rho c' m^2}{\text{MB}}  \Bigg\}
    \end{aligned}
  \end{equation}

  Where $\alpha = 1$ when $c = C$, and $\alpha = 2$ otherwise.

  This computation is repeated for each $BN / \rho$ sub--matrices of
  $u_i$, and for each of $CC'/cc'$ sub--matrices of $v_{i,j}$.  The
  total runtime is given in Tbl.~\ref{table:fused-stuff}.  The runtime
  depends on the choices of $c$, $c'$ and $m$ ($r$ is fixed to the
  kernel size), however the choices of $c$, $c'$ and $m$ are subject
  to the following constraints:

  \begin{equation} \label{eq:fused_prob}
    \small
    \begin{aligned}
      4Tcc' = 4t^2cc' = 4(m-r+1)^2cc' & \le & \frac{\text{Cache size}}{2} \\
      t = m + r - 1 & \le & 6 ~\text{(High accuracy)} \\
      t = m + r - 1 & \le & 8 ~\text{(Low accuracy)} \\
    \end{aligned}
  \end{equation}

  The $T$ sub--matrices $v_{i,j}$ need to fit in half the cache size,
  allowing for some space for the $\rho$ rows of $T$ sub--matrices
  $u_i$, as well as to avoid evictions due to hardware pre--fetches.

  The tile size is limited to $6$ when high accuracy (comparable to
  the one of the FFT and direct methods is required), and $8$ when
  lower accuracy is allowed.

  \subsection{Comparing to Non--Fused Approach}

  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=0.95\linewidth]{__fig/win6_vs_fused.pdf}\\
      \includegraphics[width=0.95\linewidth]{__fig/win8_vs_fused.pdf}\\
      \includegraphics[width=0.95\linewidth]{__fig/fft_vs_fused.pdf}\\
      \includegraphics[width=0.95\linewidth]{__fig/fft3_vs_fused.pdf}\\
    \end{center}
    \caption{Model predictions}
    \label{fig:fused-predictions}
  \end{figure*}

  The relative performances of the fused and non--fused approaches
  depend only on the CMR and the cache size, and not the absolute
  values of the FLOPS and MB.  Note that the MB in formula A and B
  cancel out.  On Fig.~\ref{fig:fused-predictions} we show the
  relative performances on a sample layer by varying different layer
  parameters.

  Again, we start from a sample layer, the VGG layer 3.2, which has
  the batch size $B = 64$, 256 input and output channels ($C = C' =
  256$).  The input image sizes are $58 \times 58$, and kernel sizes
  are $3 \times 3$.  This can be considered an average layer.  Earlier
  layers in VGG have larger images, but less channels, whereas the
  latter layers have smaller images and more channels.  To understand
  the effect of each layer parameter, as well as cache size and
  \CMR~to the relative performances of fused and non--fused methods,
  we vary each of the parameters and show the estimated speedup of the
  non--fused methods over the fused ones.

  Fig.~\ref{fig:fused-predictions} shows the predictions when
  different parameters of the layer or system are varied.  We assume
  $256$ KB of cache per core is available, except for the plot where
  the cache size is varied.  In all cases the non--fused approach is
  expected to significantly outperform the equivalent fused approach.
  Based on the estimates, we could design a case when the fused
  approach is expected to outperform, such as when all of the
  following is satisfied:

  \begin{enumerate}
  \item Large amount of cache is available.
  \item The number of channels is low.
  \item Image sizes are small.
  \item Kernel sizes are very small.
  \end{enumerate}

  None of the commonly used ConvNets satisfies all of the above.
  However, we expect that in some edge cases for very specific
  ConvNets, and specific hardware, the fused approach might be
  beneficial.

  \section{Model Lookup Tables}


  \input{tables/winograd-tile-flops-ais.tex}

  \input{tables/regular-fft-tile-flops.tex}
  \input{tables/regular-fft-tile-ais.tex}

  \input{tables/gauss-fft-tile-flops.tex}
  \input{tables/gauss-fft-tile-ais.tex}


  \section{VS others}


  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_knl_mcdram.pdf}\\
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_skx_3000.pdf}\\
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_xeonsp.pdf}
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_skx_2133.pdf}
    \end{center}
    \caption{Vs Others}
  \end{figure*}


  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_knl_48_dram.pdf}
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_knl_64_dram.pdf}
      \includegraphics[width=0.95\linewidth]{__fig/__benchs_skx_1600.pdf}
    \end{center}
    \caption{Vs Others}
  \end{figure*}
