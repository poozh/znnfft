\section{Introduction}

 Convolutional neural networks (ConvNets) have emerged as a
  widespread machine learning method for many application domains,
  soon after the demonstration of its superior performance for
  classification and localization tasks in the
  ImageNet\cite{imagenet_cvpr09} competition in
  2012~\cite{krizhevsky2012imagenet}.  Since convolutional layers are
  computationally intensive and dominate the total execution time of
  modern deep ConvNets~\cite{szegedy2015going, krizhevsky2012imagenet,
    montufar2014number, simonyan2014very}, many efforts have been made
  to improve the performance of the convolutional primitives for
  CPUs~\cite{zlateski2016znn, vanhoucke2011improving, budden2016deep,
    falconlib, zlateski2017compile}, GPUs~\cite{chetlur2014cudnn,
    mathieu2013fast, vasilache2014fast, neon} or both
  \cite{zlateski2016znni}.

  An important class of optimization is to reduce the number of
  operations required for a convolution.  Initially, several efforts
  used FFT--based convolutions to reduce the required computations for
  GPUs~\cite{mathieu2013fast, vasilache2014fast} and
  CPUs~\cite{zlateski2016znn, zlateski2016znni}.  Recently Lavin et
  al.~\cite{lavin2016fast} proposed a Winograd--based convolution, and
  demonstrated that it can save more operations than FFT, especially
  for small 2D kernels (e.g. $3 \times 3$), the focus from FFTs
  switched to Winograd--based convolutions.  Nervana~\cite{neon} and
  Nvidia's cuDNN~\cite{chetlur2014cudnn} had implemented Winograd
  based convolution for GPUs.  CPU implementations were also provided
  by FALCON~\cite{falconlib}, LIBXSMM\cite{libxsmmGit} and Intel
  MKL-DNN~\cite{mkl-dnn}.  Budden et al.~\cite{budden2016deep} further
  generalized the algorithm to arbitrary dimensions and kernel sizes,
  and proposed a CPU implementation.  A highly optimized algorithm for
  modern many--core CPUs was proposed by~\cite{winograd}, which also
  provides an open--source implementation~\cite{wConv}.

  Comparing with direct convolution, Winograd or FFT can use fewer
  operations.  However, while the traditional method of using time
  complexity for estimating run times suggests that reduction in the
  number of operations should speedup the execution, it
  ignores many other factors, such as the cost of data movements
  required by the algorithm, the memory bandwidth and
  cache size of the target machine, all of which can affect the running times
  on modern hardware.

  The main reduction in operations in the Winograd method, compared to
  FFT, comes from the fact that it works with real numbers.  However,
  due to its numerical instability, the Winograd method can only use
  small tile (transform) sizes~\cite{lavin2016fast,
    vincent2017improving, budden2016deep}, which result in a larger
  amount of required data movement to and from memory.  In contrast,
  the FFT--based method does not suffer from such instability, thus
  larger tile sizes can be used, which can partially reduce the number
  of required operations and greatly reduce the amount of data
  movements; these savings can, in certain cases, offset the cost for its relatively large number of operations.

  These observations raise the question, under what conditions the
  Winograd--based approach performs better than the FFT--based
  approach and vice versa. 

  \subsection*{Contributions and Novelty}

  In this paper, we propose a performance model based on the idea of
  Roofline~\cite{williams2008roofline} that, in addition to the number
  of operations, accounts for the total amount of data movement (to
  and from memory), as well as the arithmetic intensity (operations
  per moved byte) to compare the Winograd--based and FFT--based
  approach.  To estimate the running times, alongside the processor's
  speed, our model also considers memory bandwidth and the cache
  sizes.

  Our model is suitable for systems with modern CPUs and GPUs, whose compute speeds (in terms of FLOPS) and memory bandwidths improve rapidly.  However, the 
  speeds for computations typically improve faster than  memory bandwidths~\cite{wulf1995hitting},  resulting in an increasing change in terms of 
  compute--to--memory ratios.  For instance, the $4.5$ TFLOPS Intel
  Knights Landing processor~\cite{jeffers2016intel} has a
  compute--to--memory ratio of $11$, whereasthe latest Skylake Xeon
  processor family has increased the ratio to the range of $20$ to $30$. Titan Xp
  GPU from NVIDIA has a compute--to--memory ratio of $20$, and Tesla V100 has a ratio of
  $16$ for single precision computation, and a ratio of $133$ for
  half--precision computation, when using its dedicated tensor cores.

  Our model suggests whether a Winograd-based or a FFT-based approach is faster depends on the specific convolution layer and the specific hardware it is executed on.
  However, on average, the FFT--based approaches outperform the
  Winograd--based one on most commonly used networks, with the margin
  increasing as the system's compute--to--memory ratio increases.
  This suggests, that the popular trend, which focuses on providing
  efficient Winograd--based implementations is sub--optimal.  
  
  % Both Intel, the major CPU vendor, and NVIDIA cuDNN, the major GPU vendor, focus on optimizing Winograd--based implementations.  While cuDNN provides old, and not well optimized FFT implementations, Intel doesn't provide any FFT implementation at all.

  To support our theoretical findings, we have designed and implemented two
  FFT--based convolution algorithms (Regular--FFT and
  Gauss--FFT -- using Gauss's multiplication method) for modern CPUs.  We have compared them with an optimized implementation for a Winograd--based algorithm~\cite{winograd} using similar optimization techniques for the same hardware.  The
  experiments confirmed the predictions of our model.  Whether Winograd
  or FFT can achieve better performance depends on the
  configuration of the convolution layer and the 
  computation--to--memory ratio and the cache size of the target hardware.  By analyzing the
  effect of each control variable in our model, we have provided rule-of-thumbs for when the FFT-based and Winograd-based approaches outperform each other.
