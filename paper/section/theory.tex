\section{Performance Comparison Model}

 In this paper, we introduce a new Roofline model, which is a hybrid
  of the basic and the hierarchical model.  Our model is ``cache aware'' in the sense
  that it considers both the bandwidth and the size of each level of
  caches, but unlike the Cache--aware Roofline, can be used for
  predicting performances.
	
 In our custom Roofline model, we define \DM~, as the data
  movements between highest level of on--chip, core--exclusive cache
  (typically L2 for CPUs and L1/Register file for GPUs) and off--chip,
  shared memory~\footnote{The term off--chip refers to
    the main memory of the system, typically large,
    but much lower throughput and higher latency than the
    on--chip caches.  The main memory on a GPU board would be
    considered off--chip, as well as the HBW MCDRAM of the
    Knights Landing processor.}.  In the systems where the L2 is shared among a small
  number of cores (e.g. Intel Xeon Phi series share L2 cache
    between two cores.), the L2 is assumed to be equally divided for
  exclusive usage among the cores.

  \DM~ then accounts for all of regular and non--temporal stores
  to main memory, regular loads from main memory, and pre--fetches
  from main memory to either L1 or L2.

  Our model does not consider the presence of higher level, shared
  caches, as on modern processors, the sizes of such caches are very
  limited, and can not fit the working set of convolutional layers.
  Also, even division of such caches among many cores is not feasible
  due to limited cache--associativity, and frequent evictions due to
  traffic between the main memory and lower levels of cache.

  Additional performance ceilings, for lower levels of cache, as those in the Hierarchical Roofline model, are not necessary, since in 
  Winograd-based and FFT-based convolutional algorithms presented below, their
  computations are either bounded by transfers to and from main memories, or their
  the processor peak FLOPS numbers, as shown in the next section.

  As the performance ceiling is set by algorithm's arithmetic
  intensity (Eqn.~\ref{eq:roofline}), its running time can be estimated by:

  \begin{equation}
    \label{eq:running time}
    \footnotesize
    \begin{aligned}
      RunningTime & =  \frac{\FPO}{Attainable~FLOPS} \\
      & =  \frac{\FPO}{\min(\FLOPS, ~\mathsf{\MB} \times \AI )} = \frac{\FPO/\MB}{\min(\CMR,\AI)} \\
      & =  \begin{cases}
        \frac{\FPO}{\mathsf{Peak~ FLOPS}} ,& \mathsf{\CMR}  <= \AI\\
        \\
        \frac{\DM}{\mathsf{\MB}}  ,& \mathsf{\CMR}  > \AI
      \end{cases}
    \end{aligned}
  \end{equation}
	
  Where \CMR~ is the system's compute--to--memory ratio, defined as
  the ratio of it's \FLOPS~ and \MB~ -- the memory bandwidth.  \FPO~
  represents the total number of floating point operations required,
  and \DM~ (the total amount of data moments in bytes), as defined
  above.  The running time is \emph{compute bound} when $\CMR <= \AI$, in
  which case it can be computed as $\frac{\FPO}{\FLOPS}$, otherwise
  the running time is \emph{memory bound}, and can be computed as
  $\frac{\DM}{\MB}$.

  For algorithms composed of several ($S$) stages, each with a unique
  arithmetic intensity (\AI), the runtime is calculated by
  accumulating the runtimes of each stage $s \in S$:

  \begin{equation}
    \label{eq:running time_total}
    \small
    \begin{aligned}
      running time_{Total} &= \displaystyle\sum_{}^{S}running time_{s} &= \displaystyle\sum_{}^{S} \frac{FPO_s/MB}{min(CMR, AI_s)}
    \end{aligned}
  \end{equation}

  Both the Winograd and FFT convolutions are such
  algorithms, composed of four distinct stages.

  Here, we are interested in the relative performance between Winograd
  and FFT methods.  We define $Speedup(\mathcal{A},\mathcal{B})$ as
  the ratio of the running times of an algorithm $\mathcal{B}$ and an
  algorithm $\mathcal{A}$.

  \begin{equation}\label{eq:ratio_total} \small %\scriptsize
    \begin{aligned}
      Speedup(\mathcal{A},\mathcal{B}) &=
      \frac{running time^{\mathcal{B}}_{Total}}{running time^{\mathcal{A}}_{Total}}
    \end{aligned}
  \end{equation}

  A speedup greater than one indicates that the algorithm
  $\mathcal{A}$ is faster, and smaller than one means that the
  algorithm $\mathcal{B}$ is faster.

  While the Eqn.~\ref{eq:running time_total} estimates the running time of an
  algorithm assuming perfect utilization of the hardware, which is
  rarely possible in practice, the Eqn.~\ref{eq:ratio_total} is valid
  when the compared algorithms are equally optimized -- utilize the
  same percentage of the hardware.

  The Eqn.~\ref{eq:ratio_total} can be applied to Winograd,
  Regular--FFT and Gauss--FFT convolutional layer algorithms, in order
  to decide which one to use on a given system.  The \AI s of all
  convolutional layer algorithms will depend on the amount of
  available cache size, as detailed in the following section.
  Therefore, when comparing performance of two algorithms, the
  relative performance on a given system will depend on the \CMR~
  ratio and the amount of available cache, and not the absolute values
  of the system's compute speed or memory throughput.
