\section{Arithmetic Intensity Analysis}

  \begin{table*} \centering
    \small
    \setlength\tabcolsep{2.5pt}
    \caption{FLOPS, DM and AI for different stages of the Winograd
      ($\WINs(m^2,r^2)$), Regular--FFT ($\FFTs(m^2,r^2)$) and
      Gauss--FFT ($\FFTGs(m^2,r^2)$) methods.  $N$ is the number of
      tiles per single image, the tiles have size of $t^2$ ($t =
      m+r-1$). }
    \begin{tabular}{cc | ccc}
      \toprule
      & Stage & $\WINs(m^2,r^2)$  & $\FFTs(m^2,r^2)$ & $\FFTGs(m^2,r^2)$ \\
      \midrule
      \multirow{3}{*}{\rotatebox{90}{\textbf{FLOPS}}}
      & Input image transform & $BCN\WINs^{I}(m^2,r^2)$ & $BCN\FFTs^{I}(m^2,r^2)$ & $BCN\FFTGs^{I}(m^2,r^2)$ \\
      & Kernel transform & $CC'\WINs^{K}(m^2,r^2)$ & $CC'\FFTs^{K}(m^2,r^2)$ & $CC'\FFTGs^{K}(m^2,r^2)$ \\
			& Element--wise Product&$2t^2BNCC'$ &$8t\ceil{(t+1)/2}BNCC'$ & $6t\ceil{(t+1)/2}BNCC'$ \\
      & Output transform & $BC'N\WINs^{O}(m^2,r^2)$ & $BC'N\FFTs^{O}(m^2,r^2)$ & $BC'N\FFTGs^{O}(m^2,r^2)$ \\
      \midrule
      \multirow{3}{*}{\rotatebox{90}{\textbf{DM}}}
      & Input image transform & $4BCx^2 + 4BCNt^2$ & $4BCx^2 + 8BCNt\ceil{(t+1)/2}$ & $4BCx^2 + 12BCNt\ceil{(t+1)/2}$ \\
      & Kernel transform & $4CC'\big (r^2 + t^2 \big)$ & $4CC' \big ( r^2 + 2t\ceil{(t+1)/2} \big )$ & $4CC' \big ( r^2 + 3t\ceil{(t+1)/2} \big )$ \\
			& Element--wise Product&$4t^2BN(c + \alpha c')\dfrac{CC'}{cc'}$ &  $8t\ceil{(t+1)/2}BN(c + \alpha c')\dfrac{CC'}{cc'}$ & $12t\ceil{(t+1)/2}BN(c + \alpha c')\dfrac{CC'}{cc'}$ \\
      & Output transform & $4BC'N \big ( t^2 + m^2 \big)$ & $4BC'N \big( 2t\ceil{(t+1)/2} + m^2 \big )$ & $4BC'N \big ( 3t\ceil{(t+1)/2} + m^2 \big )$ \\
      \midrule
      \multirow{3}{*}{\rotatebox{90}{\textbf{AI}}}
      & Input image transform & $\dfrac{N\WINs^{I}(m^2,r^2)}{4x^2 + 4Nt^2}$ & $\dfrac{N\FFTs^{I}(m^2,r^2)}{4x^2 + 8Nt\ceil{(t+1)/2}}$ & $\dfrac{N\FFTGs^{I}(m^2,r^2)}{4x^2 + 12Nt\ceil{(t+1)/2}}$ \\
      & Kernel transform & $\dfrac{\WINs^{K}(m^2,r^2)}{4r^2 + 4t^2}$ & $\dfrac{\FFTs^{K}(m^2,r^2)}{4r^2 + 8t\ceil{(t+1)/2}}$ & $\dfrac{\FFTGs^{K}(m^2,r^2)}{4r^2 + 12t\ceil{(t+1)/2}}$ \\
		& Element--wise Product& $\dfrac{cc'}{2(c + \alpha c')}$ &  $\dfrac{cc'}{c + \alpha c'}$ & $\dfrac{cc'}{2(c + \alpha c')}$ \\
      & Output transform & $\dfrac{\WINs^{O}(m^2,r^2)}{4t^2 + 4m^2}$ & $\dfrac{\FFTs^{O}(m^2,r^2)}{8t\ceil{(t+1)/2} + 4m^2}$ & $\dfrac{\FFTGs^{O}(m^2,r^2)}{12t\ceil{(t+1)/2} + 4m^2}$ \\
      \bottomrule
    \end{tabular}
    \label{table:layer-stuff}
  \end{table*}


  Both Winograd and FFT approaches perform the computation in four
  distinct stages: the first two are the input transform and the
  kernel transform stages, which convert the images/kernels from the
  spatial domain into Wingorad or FFT domain; in the third stage
  element--wise computation is performed, which is equivalent to
  matrix multiplications; finally, the inverse transform stage
  converts the results back to the spatial domain.

   We focus on ``pipelined'' algorithms, where the four stages are
  performed in a pipelined fashion.  This means that each stage reads the data
  from main memory, and stores the results back to main memory, which
  is then used as an input to the next stage.

  In contrast, ``fused'' approaches, as the one originally suggested
  by Lavin et al.~\cite{lavin2016fast}, interleave computation of the
  four stages, in order to reuse more data in cache; however this
  comes with the price of increased number of operations and lower
  \AI.  In nearly all practical scenarios the ``pipelined'' approach
  is much more efficient than the ``fused'' one.  The analysis of
  ``fused'' vs ``pipelined'' is provided in the
  appendix. %~\ref{app-fused-pipelined}.

  All public CPU implementations use the ``pipelined'' approach.
  CuDNN provides both ``fused'' and ``pipelined'' implementations for
  the NVIDIA GPUs, with the ``pipelined'' being faster in nearly all
  scenarios.

  In the rest of this section, we present the details of the
  ``pipelined'' algorithm and estimate the total number of operations
  and data movement required, from which we calculate the arithmetic
  intensity.


  For simplicity, we assume 2D and isotropic images and kernels in the
  rest of the paper.  Extension to non--isotropic and $N$--dimensional
  images and kernels is straight forward.  Our implementations
  described in Section~\ref{sec:implement} support an arbitrary number
  of dimensions, as well as non--isotropic kernels.  The computation
  is performed and data is kept in 32--bit floating point numbers.

  We follow the same notation as in Section~\ref{sec:background}, and
  for an arbitrary layer, use Winograd convolution $\WINs(m^2, r^2)$,
  Regular--FFT convolution $\FFTs(m^2, r^2)$ and Gauss--FFT
  convolution $\FFTGs(m^2, r^2)$.  Here, $m$ can take an arbitrary
  value, whereas $r^2$ has to equal to the sizes of the kernels in the
  layer.

  \subsection{Input Transform Stage}

  In Winograd and both FFT approaches, each of the $B \cdot C$ images
  ($B$ batches, each with $C$ input channels) is divided into
  overlapping tiles, which are then transformed.  Each tile has the
  size of $t^2$ with $t = (m + r - 1)$, and there is an overlap of $r
  - 1$ pixels between adjacent tiles along both dimensions.

  Each image of size $x \times x$ will thus be divided into $N =
  \ceil{(x-r+1)/m}^2$ tiles.  If necessary, the image will be
  implicitly zero--padded.  This yields a total of
  $BC\ceil{(x-r+1)/m}^2 = BCN$ image tiles.

  \paragraph{Number of required operations}

  Each of the $BCN$ tiles ($I_{b,c,n}$) is transformed via
  $\tilde{I}_{b,c,n} = \mathbb{B}I_{b,c,n}\mathbb{B}^T$, which is a
  composition of two matrix--matrix multiplications.  Here $I_{b,c,n}$
  is the $n$--th tile of the $c$--th channel in batch $b$, and
  $\tilde{I}_{b,c,n}$ is its transform.  Both $\mathbb{B}$ and
  $I_{b,c,n}$ have the size of $t^2$, requiring total of $4t^3$
  operations ($2t^3$ per matrix--matrix multiplication).  Operations
  are on real numbers for the Winograd approach, and on complex
  numbers for the FFT approaches.

  The transforms can, however, be performed with much fewer
  operations.  In the Winograd method, the matrices $\mathbb{B}$ are
  sparse, and contain pairs of columns with similar numbers, allowing
  for reuse of many intermediate results.  In the FFT methods, instead
  of matrix multiplications, 2D real--to--complex DFT transforms are
  performed, which require much fewer operations ($\mu n^2\log n$
  instead of $\mathcal{O}(n^3)$).  Here, the constant $\mu$ can vary a
  lot based on the size of the transform
  performed~\cite{frigo1998fftw}.  It takes small values, when the
  size of the transform is a power of two, and larger values when the
  transform size contains large prime factors.

  In our model, we pre--computed the number of required operations for
  transforming a tile of a given size, and stored them into a lookup
  table.  For the Winograd case, we used Winograd matrix
  generator~\cite{wincnn}, as proposed in~\cite{winograd,
    budden2016deep} and further reduced the number of operations using
  the simple optimizer provided by~\cite{winograd}.  For the
  Regular--FFT method, we used the FFT codelet generator ``genfft''
  from the FFTW library~\cite{frigo1998fftw} to generate transforms,
  and also count the total number of floating point operations
  required for each transform.  We denote the number of operations
  required for transforming an image tile as $\WINs^{I}(m^2,r^2)$ for
  the Winograd, $\FFTs^{I}(m^2,r^2)$ for Regular--FFT, and
  $\FFTGs^{I}(m^2,r^2)$ for Gauss--FFT.  The Gauss--FFT transforms
  require one extra floating operation per element of the transform
  tile, as they compute and store the sum of the real and imaginary
  part (Sec.~\ref{sub_sec_gauss}): $\FFTGs^{I}(m^2,r^2) =
  \FFTs^{I}(m^2,r^2) + (m+r-1)\ceil{(m+r)/2}$.  The pre--computed
  lookup tables used in our model are provided in the appendix.

  Total number of operations required for performing all the
  transforms, for all three methods are given in
  Tbl.~\ref{table:layer-stuff}.

  \paragraph{Data movement}

  The tile sizes are relatively small, much smaller than available
  cache, thus, once a tile is fetched from main memory all the
  computation is done in--cache.  Additionally, the overlapping
  regions between tiles need to be fetched from memory only once, as
  they can be stored in cache and reused for adjacent tiles.

  The total data that has to be moved from main memory to cache is
  thus $BCx^2 \cdot 4$ bytes -- reading each of the $B \cdot C$ images
  of size $x^2$ only once.  Each of the $BCN$ transformed tiles is
  stored back to main memory.  The size for each transformed tile will
  depend on the method.

  In Winograd convolution, transformed tiles are real tensors, and
  require a single 32--bit float per element, thus total of $4t^2$
  bytes.

  The FFT methods perform 2D FFT transforms of each tile.  As the FFTs
  are performed on real tensors, the resulting transforms will be
  conjugate--symmetric (along one of the dimensions), thus only
  $t\ceil{(t+1)/2}$ complex numbers need to be stored.  The
  Regular--FFT requires two real numbers per complex number, total of
  $8t\ceil{(t+1)/2}$ bytes per tile, and the Gauss--FFT requires three
  reals per complex number, a total of $12t\ceil{(t+1)/2}$ bytes per
  tile.

  The total amount of data movement for the three methods, as well as
  their arithmetic intensities (\AI s), is shown in
  Tbl.~\ref{table:layer-stuff}

  \subsection{Kernel Transform Stage}

  In all methods, each of the $C \times C'$ kernels (with size of $r
  \times r$) is transformed via
  $\tilde{W}_{cc'}=\mathbb{G}W_{cc'}\mathbb{G}^T$.  The matrix
  $\mathbb{G}$ has size of $t \times r$.  As in the input stage, the
  kernel transforms can be computed with fewer operations compared to
  direct matrix--matrix multiplications.  For estimating the number of
  operations required per kernel transform, we used the similar method
  as in the case of input image tiles.  However, the ``genfft''
  generator was modified to perform implicitly zero padded 2D DFT
  transforms and count the number of operations.  Again, we
  pre--compute the total number of operations required for
  transforming kernels, and store them in lookup tables (given in the
  appendix): $\WINs^{K}(m^2,r^2)$ for the Winograd,
  $\FFTs^{K}(m^2,r^2)$ for Regular--FFT.  For Gauss--FFT method,
  $\FFTGs^{K}(m^2,r^2) = \FFTs^{K}(m^2,r^2) + 2t\ceil{(t+1)/2}$, as we
  need two extra operations per complex number as described in
  Sec.~\ref{sub_sec_gauss}.

  All three methods need to fetch all kernel data from memory -- total
  of $4CC'r^2$ bytes, and store $CC'$ transformed kernels back to main
  memory.  The transformed kernels have the size of $t \times t$, and
  are stored in the same fashion as the transformed input tiles,
  requiring total of $4t^2$, $8t\ceil{(t+1)/2}$, and
  $12t\ceil{(t+1)/2}$ bytes for the Winograd, Regular--FFT and
  Gauss--FFT methods respectively.

  The total number of operations, data movements, and \AI s for
  transforming all kernels of a particular layer, using any of the
  three methods, are given in Tbl.~\ref{table:layer-stuff}.

  \subsection{Element--wise Products} \label{sec_element_wise}

  Having all transformed input and kernel tiles, the pre--transformed
  output tiles $\tilde{I'}_{b,c',n}$ are computed via

  \begin{equation}
    \small %\scriptsize
    \begin{aligned}
      \tilde{I}'_{b,c',n} & = \sum_{c=1}^C \tilde{I}_{b,c,n} \odot \tilde{W}_{c,c'}
    \end{aligned}
    \label{eqn-ew-comp}
  \end{equation}

  Here, all of the pre--transformed output tiles
  ($\tilde{I}'_{b,c',n}$), transformed input tiles
  $\tilde{I}_{b,c,n}$, and transformed kernels $\tilde{W}_{c,c'}$ have
  the same size of $t \times t$.  Computing an element of the
  pre--transformed output tile at a location $\vec{e}$ depends only on
  elements of the transformed input tiles and transformed kernels at
  the same location, and is computed via:

  \begin{equation}
    \small %\scriptsize
    \begin{aligned}
      \tilde{I}'_{b,c',n}(\vec{e}) & = \sum_{c=1}^C \tilde{I}(\vec{e})_{b,c,n} \cdot \tilde{W}(\vec{e})_{c,c'}
    \end{aligned}
    \label{eqn-ew-comp}
  \end{equation}

  Note that the equation above can be interpreted as multiplication of
  a matrix $U_{\vec{e}}$ of size $BN \times C$ with a matrix
  $V_{\vec{e}}$ of size $C \times C'$ resulting in a matrix
  $X_{\vec{e}}$ of size $BN \times C'$.  For layers of modern
  ConvNets, values of $BN$ are much larger than $C$, which results in
  multiplication of tall and skinny
  matrices~\cite{winograd,lavin2016fast}. Such matrix multiplications
  have to be performed for each element location of
  $\tilde{I}'_{b,c',n}$.

  \subsubsection{Operations Required} \label{sub_sec_gemm_op}

  In the Winograd method, $t^2$ real matrices are multiplied,
  requiring total of $2t^2BNCC'$ operations for the whole layer.
  Where $N$ is the number of tiles per image.

  For the FFT based methods, complex matrices are multiplied.
  However, only $t \ceil{(t+1)/2}$ locations of the pre--transformed
  output tiles need to be computed.  As both the transformed input
  tiles and transformed kernels are conjugate symmetric, the
  pre--transformed output tiles will also be conjugate symmetric.

  In the Regular--FFT method, $4$ real multiply--adds are required for
  performing a complex multiply--add.  This gives us total of $8t
  \ceil{(t+1)/2}BNCC'$ FLOPs required for the whole layer.  Gauss--FFT
  reduces complex matrix multiplications to 3 real matrix
  multiplications of matrices of the same size, totaling $6t
  \ceil{(t+1)/2}BNCC'$ FLOPs required for the whole layer.

  \subsubsection{Data Movement}

  In the Winograd method, $t^2$ real matrix multiplications $X = U
  \times V$ are performed (for the rest of the section we will omit
  the subscript $_{\vec{e}}$, for clarity).  In the Regular--FFT
  method, $t\ceil{(t+1)/2}$ complex matrix multiplications are
  performed, with the same sizes.  The Gauss--FFT method replaces one
  complex matrix multiplication with three real matrix
  multiplications, thus total of $3t\ceil{(t+1)/2}$ real matrix
  multiplications are performed.

  For modern ConvNets, the matrices $X$, $U$ and $V$ may not fit in
  cache, in which case, standard cache--blocking approaches are
  employed~\cite{Gannon:1987:SCL:647970.761024, heinecke2015libxsmm,
    heinecke2016libxsmm, winograd, li2015input}.  Which might require
  some data to be moved to and/or from the main memory multiple times.

  In the standard cache--blocking technique, $V$ (of size $C \times
  C'$) is subdivided into equally sized matrices $v_{i,j}$ of size $c
  \times c'$, and each of $C'/c'$ sub--matrices of X ($x_{j}$) of size
  $BN \times c'$ is computed via $x_{j} = \sum_{k=1}^{C/c} u_k \times
  v_{k,j}$, where $u_k$ is the $k$--th (out of $C/c$) sub--matrix of
  $U$ (with size of $BN \times c$).

  To minimize transfers to and from main memory, the matrix $v_{i,j}$
  is kept in cache while performing the computation $x_j = x_j + u_i
  \times v_{i,j}$.

  A few ($\rho$) rows of $u_i$ are fetched from main memory,
  multiplied with the in--cache $v_{i,j}$ and accumulated to $\rho$
  rows in $x_j$.  This requires transferring total of $\rho c$ numbers
  of $u_i$ from memory, and $\rho c'$ numbers of $x_j$ from and then
  transferred back to the main memory.  The total of $\rho(c + 2c')$
  numbers need to be transferred.

  Here, multiplying $\rho$ rows of $u_i$ with $v_{i,j}$ in--cache is
  always compute limited when another level of blocking (register
  blocking) is utilized.~\cite{winograd, heinecke2015libxsmm,
    heinecke2016libxsmm} For that reason, no additional performance
  ceilings of the Hierarchical Roofline model are necessary.

  When $c = C$, there is a single element to the sum above: $x_{j} =
  u_1 \times v_{1,j}$.  Thus when the rows of $x_j$ are computed,
  there is no need to fetch previous values to which the sum is
  accumulated, hence requiring only $\rho(c + c')$ number transfers.
  Multiplying $\rho$ rows thus requires $\rho(c + \alpha c')$ numbers
  to be transferred, where $\alpha = 1$ if $c = C$, or $\alpha = 2$
  otherwise.  Computing all the rows of $x_j$ then requires total of
  $BN(c + \alpha c')$ numbers.

  Each matrix multiplication $X = U \times V$ is decomposed into
  $\nicefrac{CC'}{cc'}$ multiplications of smaller matrices ($x_j =
  u_i \times v_{i,j}$), which then require total of $BN(c + \alpha
  c')\nicefrac{CC'}{cc'}$ transfers of numbers (either real or complex
  depending on the method).

  In the Winograd method $t^2$ real matrix multiplications ($X = U
  \times V$) are performed, thus transferring total of $4t^2BN(c +
  \alpha c')\nicefrac{CC'}{cc'}$ bytes to be moved.

  In the Regular--FFT method $t\ceil{(t+1)/2}$ complex matrix
  multiplications are performed, requiring total of
  $8t\ceil{(t+1)/2}BN(c + \alpha c')\nicefrac{CC'}{cc'}$ bytes to be
  moved.

  The Gauss--FFT replaces each of the $t\ceil{(t+1)/2}$ complex matrix
  multiplications with 3 real matrix multiplications.  Total of
  $12t\ceil{(t+1)/2}BN(c + \alpha c')\nicefrac{CC'}{cc'}$ bytes need
  to be transferred.

  The goal is to choose optimal values for $c$ and $c'$ such that the
  data movement is minimized, while allowing for in--cache computation
  (matrices $v_{i,j}$ and $\rho$ rows of $x_j$ and $u_i$ need to fit
  inside the cache).

  As the values of $t$, $B$, $C$, $C'$ and $N$ are constant, the
  optimal values of $c$ and $c'$ can be chosen by solving the
  following optimization problem:

  \begin{equation} \label{eq:gemm_AI}
    \small
    \begin{aligned}
      & \underset{\mathbf{c,c'}}{\text{minimize}} &&
      \frac{(c + \alpha c')}{cc'} \\
      & \text{subject to}
      & & \mathbf{c} \mid C & \text{($C$ is divisible by $\mathbf{c}$)}\\
      & & & \mathbf{c'} \mid C'& \text{($C'$ is divisible by $\mathbf{c'}$)} \\
      & & & \mathbf{4\beta cc'} \le \frac{\text{Cache Size}}{2} & \text{(fits in half cache)}
    \end{aligned}
  \end{equation}

  Where $\alpha$ equals $1$ when $c = C$ and $2$ when $c < C$; $\beta$
  is $1$ for real valued matrices, and $2$ for complex valued ones.
  Half the cache is allowed for matrices $v_{i,j}$.  This is typical
  practice, to ensure enough space for $\rho$ rows of matrices $u_i$
  and $x_j$, as well as pre--fetching the next $\rho$ rows.  It also
  ensures that the hardware pre--fetched does not evict the data from
  cache.

  \begin{figure}
    \begin{center}
      \includegraphics[width=0.98\linewidth]{__fig/__gemm_cgemm.pdf}
    \end{center}
    \caption{Arithmetic intensities for the element--wise stage given
      various amounts of cache.  In the Winograd and Gauss--FFT
      method, real matrix multiplications are performed, whereas, in
      the Regular--FFT complex matrix multiplications are performed.}
    \label{fig:gemm-cgemm-ratios}
  \end{figure}

  For given layer parameters, and cache size, we can find optimal
  decomposition ($c$ and $c'$) efficiently by using exhaustive search.

  The arithmetic intensities can be then computed by dividing the
  number of required operations with the amount of required data
  movements for optimal values of $c$ and $c'$, which can easily be
  obtained using an exhaustive search.  This results in the \AI s of
  $cc' / 2(c + \alpha c')$ for the Winograd and Gauss--FFT methods,
  and $cc' / (c + \alpha c')$ for the Regular--FFT method, which equal
  to the \AI s of real matrix, and complex matrix multiplications,
  respectively~\cite{Gannon:1987:SCL:647970.761024,
    heinecke2015libxsmm, heinecke2016libxsmm, winograd}

  On Fig.~\ref{fig:gemm-cgemm-ratios}, we show the arithmetic
  intensities for layers with different number of channels as a
  function of cache size.  The \AI s of both complex (used in
  Regular--FFT method) and real matrix multiplication (used in
  Winograd and Gauss--FFT) increase with the cache size and the number
  of channels (C and C').  For a fixed cache size, the complex matrix
  multiplication allows for a higher arithmetic intensity.  This
  indicates that the element--wise stage of the Regular--FFT method
  may achieve better hardware utilization than the one of Winograd or
  Gauss--FFT convolution, when the cache size is a limiting resource.

  \subsection{Output Transform} \label{subsec_inverse_T}

  In the output transform stage, each of the $BNC'$ pre--transformed
  output tiles $\tilde{I'}_{b,c',n}$ is transformed from Winograd/FFT
  domain back to the spatial domain via $I'_{b,c,n} =
  \mathbb{A}^T\tilde{I}'_{b,c,n}\mathbb{A}$.  The matrix $\mathbb{A}$
  has size of $t \times m$ ($t = m + r -1$), resulting in
  $I'_{b,c',n}$ having a size of $m \times m$.  Here $I'_{b,c',n}$ is
  the $n$--th (non--overlapping) tile of the $c'$-th image in batch
  $b$ of the final result of the convolutional layer.

  Yet again, we use the Winograd matrix generator~\cite{wincnn}, and
  the optimizer provided by~\cite{winograd}, as well
  ``genfft''~\cite{frigo1998fftw} to generate transforms, and count
  the total number of floating point operations. ``Genfft'' generator
  was modified to compute only the relevant subset (for ``valid''
  convolution) of the inverse complex--to--real 2D DFT transform.

  We store the FLOPs required for various values of $m$ and $r$ in
  lookup tables (given in the appendix): $\WINs^{O}(m^2,r^2)$ for the
  Winograd, $\FFTs^{O}(m^2,r^2)$ for Regular--FFT.  For Gauss--FFT
  method, $\FFTGs^{O}(m^2,r^2) = \FFTs^{O}(m^2,r^2) +
  2t\ceil{(t+1)/2}$, two operations are required for converting the
  three real--number representation into the standard (two number)
  representation of complex numbers (Sec.~\ref{sub_sec_gauss}).

  All three methods need to store $BC'N$ transformed output tiles
  (final results) to main memory, which requires total of $4BC'Nm^2$
  bytes.

  Also, each of the methods needs to fetch $BC'N$ pre--transformed
  tiles from main memory.  In the Winograd method, each
  pre--transformed tile is stored using $t^2$ real numbers, thus
  requiring total of $4t^2BC'N$ bytes to be transferred from main
  memory.  The pre--transformed tiles in the FFT methods are conjugate
  symmetric, thus requiring only $t\ceil{(t+1)/2}$ complex numbers for
  storage.  The Regular--FFT stores complex numbers as two real
  numbers, thus requires fetching total of $8t\ceil{(t+1)/2}BC'N$
  bytes from main memory, while Gauss--FFT stores complex numbers with
  three real ones -- requiring total of $12t\ceil{(t+1)/2}BC'N$ bytes
  to be transferred from main memory.

  The total number of operations, data movements, and \AI s for
  transforming output tiles of a particular layer, using any of the
  three methods, are given in Tbl.~\ref{table:layer-stuff}.

  \paragraph{Optimality of tile transforms}

  Both ``wincnn''~\cite{wincnn} and ``genfft''~\cite{frigo1998fftw}
  use heuristics to minimize the number of operations for performing
  transforms, thus they might not be optimal.  However, as their \AI
  s, (also provided in the appendix) are much smaller than the \CMR s
  of modern systems, the computation is memory bound, and the runtime
  of the transform stages will depend solely on the amount of data
  movement.  Hence, our model produces identical estimates as if
  optimal number of operations were provided.
