\subsection{Performance Comparison}

  For a given layer, when using the Winograd or FFT methods, the total
  number of operations, \DM, \AI~and thus the runtime, will depend on
  the choice of $m$ ($r$ is determined by the layer's kernel sizes).

  \paragraph{Maximal Allowed Tile Sizes}

  Generally, larger values of $m$ decrease both the number of required
  operations and the amount of data movements (\DM) in all the
  methods, and thus, are more efficient.  However, in some cases, when
  the images need a substantial amount of zero--padding, larger $m$
  can yield an increase in both the number of operations and data
  movements~\cite{winograd, lavin2016fast}.

  The numerical inaccuracy of Winograd convolutions increases
  exponentially with tile sizes, and thus with $m$~\cite{pan2016bad,
    winograd, lavin2016fast}.  In practice, there is an upper bound on
  the tile size for which the Winograd produces accurate enough
  results.  All major vendors, such as FALCON, MKL-DNN, LIBXSMM,
  cuDNN~\cite{falconlib,libxsmmGit,mkl-dnn,cudnn} implement the
  Winograd algorithm with the tile size less or equal to $6\times 6$,
  which produces results with accuracy comparable to the one of direct
  convolution~\footnote{Applying the formula for convolution
    directly}.  For the performance comparison in this paper, we
  follow this convention, and consider only Winograd algorithms with
  tile size of at most $6 \times 6$.  In the appendix, we repeat some
  of the analysis and experiments with tile sizes up to $8 \times 8$
  as some recent research~\cite{vincent2017improving} has demonstrated
  that lower accuracy convolutions, in particular cases, can produce
  similar results.

  The FFT methods, on the other side, have extremely high accuracy,
  higher than direct convolution, even for very large tile sizes (very
  large $m$).  Thus, we allow for an arbitrary tile sizes.  We analyze
  the impact of tile sizes to numerical accuracy of all the methods in
  detail in Sec. \label{sub_sec_accuracy}.

  The accuracy of the numerical computation for each individual layer
  should not be confused with the prediction accuracy of a ConvNet.
  Higher numerical accuracy does not imply faster training or better
  predictions.  However, a certain level of numerical accuracy is
  necessary (and sufficient)~\cite{courbariaux2014low, gupta2015deep}
  for ConvNet stability, above which the ConvNets are expected to
  perform the same.

  When comparing two methods, we compare the runtimes of each method
  using $m$ for which the runtime is minimized:

  \begin{equation}
    \small
    \begin{aligned}
      Speedup^{\FFTs} &= \frac{\underset{m_W}{\min}\big \{Runtime^{\WINs(m_W^2,r^2)}\big \}}
      {\underset{m_F}{\min}\big \{Runtime^{\FFTs(m_F^2,r^2)} \big \}}
    \end{aligned}\label{eq_new_speedup}
  \end{equation}

  where $m_W$ is the $m$ used for the Winograd method, and $m_F$ used
  for the Regular--FFT.  Similar equation can be derived for the
  Gauss--FFT method.

  \subsection{Model Predictions}

  We explore the impact of various parameters of convolutional layers
  to the relative performances of the three methods.

  We start from a sample layer, the VGG layer 3.2, which has the batch
  size $B = 64$, 256 input and output channels ($C = C' = 256$).  The
  input image sizes are $58 \times 58$, and kernel sizes are $3 \times
  3$.  This can be considered an average layer.  Earlier layers in VGG
  have larger images, but less channels, whereas the latter layers
  have smaller images and more channels.  To understand the effect of
  each layer parameter, as well as cache size and \CMR~to the relative
  performances of FFT methods and Winograd method, we vary each of the
  parameters and show the estimated speedup of the FFT methods over
  Winograd, as well as Regular--FFT over Gauss--FFT.

  \begin{figure*}[ht]
    \begin{center}
      \includegraphics[width=0.95\linewidth]{__fig/vary_params.pdf}\\
    \end{center}
    \caption{Model predictions}
    \label{fig:model-predictions}
  \end{figure*}

  Fig.~\ref{fig:model-predictions} shows the model's predictions when
  different parameters of the layer or system are varied.  We assume
  $256$ KB of cache per core is available, except for the plot where
  the cache size is varied.

  Generally, the expected speedup of Regular--FFT over both Winograd
  and Gauss--FFT increases as the system's \CMR~ increases, whereas
  the relative speedup of Gauss--FFT over Winograd decreases.
  However, in certain cases, where the image sizes are relatively
  small, for a range of \CMR~, the relative speedup of Regular--FFT
  over Winograd first decreases until a certain \CMR, after which it
  starts to increase.

  To give some basic rules of thumb for how different parameters
  affect the relative performances of the three methods, we examine
  the differences of the differently colored lines.  When comparing
  the Regular--FFT and Winograd, when the L2 cache is varied (top left
  plot on Fig.~\ref{fig:model-predictions}), the red line,
  representing the mode predictions on systems with $64 KB$ cache is
  above the line representing systems with $128 KB$ cache, etc.  Thus,
  the relative performances of Regular--FFT method vs the Winograd
  method increase as the available cache size decreases.  Same
  conclusion can be made for varying layer's image sizes.

  When either of the {\bf cache size} or {\bf input/output channels}
  decreases the relative performances of both FFT methods over
  Winograd, as well as the relative performances of Regular--FFT over
  Gauss--FFT, increase.  The relative performances of both FFT methods
  over the Winograd method increase as either the {\bf kernel size},
  or the {\bf image sizes}, increase.  However, the kernel and the
  image sizes have no effect on the relative performances between the
  two FFT methods.
