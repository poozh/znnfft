\section{Background} \label{sec:background}

  \subsection{Winograd-- and FFT-- Based Convolution}

  As recently illustrated by Lavin et al.~\cite{lavin2016fast},
  ``valid'' convolution of discrete signals, the main operation used
  in modern convolutional neural networks, can be performed using
  Winograd's minimal filtering algorithm \cite{winograd1980arithmetic}
  via

  {\small
    \begin{equation} \label{eq:winograd}
      f \Conv_{\texttt{Valid}} g = \mathbb{A}^T[(\mathbb{G}g) \odot (\mathbb{B}^Tf)]
    \end{equation}
  }

  Where $f$ and $g$ are 1D signals; $\odot$ represents element--wise
  multiplication; $\mathbb{A}^T$, $\mathbb{B}^T$ and $\mathbb{G}$ are
  special matrices, derived from Vandermonde matrices for Homogenous
  Coordinate polynomials~\cite{vincent2017improving}.  By convention,
  Winograd convolutions have the matrices $\mathbb{A}^T$,
  $\mathbb{B}^T$ and $\mathbb{G}$ in real--space.  In ``Valid''
  convolution the filter slides across every ``valid'' location in the
  filtered images -- such that the filter is fully contained inside
  the image.  When the size of the filter is $|g| = r$, $f
  \displaystyle{\scriptsize{\Conv_{\texttt{\scriptsize{Valid}}}}}g$
  will have a length of $|f| - |g| + 1 = m$, and we refer to the
  method above as Winograd convolution $\WINs(m,r)$.

  On the other side, the convolution theorem states that a convolution
  can be performed using Fourier transforms via

  {\small
    \begin{equation}
      f \Conv_{\texttt{Circ}} g = \mathcal{F}^{-1} \big( \mathcal{F}(f) \cdot
      \mathcal{F}(g) \big)
    \end{equation}
  }

  Here, $\mathcal{F}$ and $\mathcal{F}^{-1}$ are Fourier and inverse
  Fourier transforms.  In the discrete case, $f$ and $g$ need to have
  the same number of elements, which can be accomplished by padding
  zeros to the shorter signal.

  Discrete Fourier transform (DFT) results in a circular (also known
  as cyclic) convolution.  The result of the ``valid'' convolution can
  be extracted from the last $|f| - |g| + 1 = m$ elements of the
  circular convolution.

  ``Valid'' convolution using decrete FFTs can also be regarded as a
  special case of the Eq~\ref{eq:winograd}, where the matrices
  $\mathbb{A}^T$, $\mathbb{B}^T$ and $\mathbb{G}$ are in complex space
  and are derived from Vandermonde matrices with polynomial points
  being the roots of unity~\cite{vincent2017improving}.
  $\mathbb{B}^T$ and $\mathbb{G}$ perform, implicitly zero--padded (to
  size of $m + r - 1 = |f|$), DFT transforms of $f$ and $g$, and
  $\mathbb{A}^T$ computes the last $m$ elements of the inverse DFT
  transform.  Using the FFT algorithm allows for efficient computation
  of matrix--vector products with matrices $\mathbb{A}^T$,
  $\mathbb{B}^T$ and $\mathbb{G}$.  We refer to this special case as
  Regular--FFT $\FFTs(m,r)$.

  \subsubsection{Multi--Dimensional Convolution}

  Both Winograd and FFT convolution can easily be extended to an
  arbitrary number of dimensions~\cite{budden2016deep}.
  $N$--dimensional convolution is performed via

  {\small
    \begin{equation}
      f \Conv_{\texttt{Valid}} g = \Big[ (g \stimes_{n=1}^{N}
        \mathbb{G}) \odot (f \stimes_{n=1}^{N} \mathbb{B})
        \Big]\stimes_{n=1}^{N} \mathbb{A}_n^T
    \end{equation}
  }

  Here, the operation $x \times_n^N \mathbb{Y}$ is short for $x
  \times_1 \times_2 \dots \times_n \mathbb{Y}$, where $\times_n$
  represents tensor--matrix mode--n multiplication as defined
  in~\cite{kolda2009tensor, budden2016deep}.  For the 2D case, $x
  \stimes_1 \mathbb{M} = \mathbb{M}x$, and $x \stimes_2 \mathbb{M} =
  x\mathbb{M}^T$.  The formula above reduces to

  {\small
    \begin{equation}
      f \Conv_{\texttt{Valid}} g = \mathbb{A}^T \Big[ (\mathbb{G}g\mathbb{G}^T) \odot (\mathbb{B}f\mathbb{B}^T)
        \Big] \mathbb{A}
    \end{equation}
  }

  Which is consistent with Lavin et al.~\cite{lavin2016fast}.

  \subsection{Winograd-- and FFT-- Based Layers} \label{subsec-WandFLayer}

  A convolutional layer transforms an input tuple of $C$ images into
  an output tuple of $C'$ images.  A batch of $B$ inputs yielding a
  batch of $B$ outputs is processed at the time via

  {
    \small
    \begin{equation}\label{eq:forward}
      I'_{b,c'} = \sum_{c=1}^C I_{b,c}\Conv W_{c'c}
    \end{equation}
  }

  Where $C$ and $C'$ denote the number of input/output images (also
  called channels or feature--maps).  $I_{b,c}$ and $I'_{b,c'}$ are
  the (arbitrary dimensional) input and output images of the $b$--th
  batch.  In total, $B \cdot C \cdot C'$ convolutions are performed.

  Using Winograd or Regular FFT convolution, the output images are
  computed via

  \begin{equation}  \small %\scriptsize
    \begin{aligned}
      I'_{b,c'} & = \sum_{c=1}^C \Big[ (W_{c,c'} \stimes_{n=1}^{N}
        \mathbb{G}_n) \odot (I _{b,c} \stimes_{n=1}^{N} \mathbb{B}_n)
        \Big]\stimes_{n=1}^{N} \mathbb{A}_n^T \\ & = \Big[
        \sum_{c=1}^C (W_{c,c'} \stimes_{n=1}^{N} \mathbb{G}_n) \odot
        (I_{b,c} \stimes_{n=1}^{N} \mathbb{B}_n)
        \Big]\stimes_{n=1}^{N} \mathbb{A}_n^T \\
    \end{aligned}
    \label{eqn-layer-comp}
  \end{equation}

  Note that we can have different sizes for matrices $\mathbb{A}_n$,
  $\mathbb{B}_n$ and $\mathbb{G}_n$ for each dimension.

  $\WINs_n(m_n,r_n)$ and $\FFTs_n(m_n,r_n)$ assume a particular size
  of $I$ ($m_n + r_n - 1$) and $I'$ ($m_n$) along $n$--th dimension.
  For larger image sizes, the convolution is performed using the
  overlap--add method (OLA)~\cite{rabiner1975theory}.  In OLA, the
  input images are divided into tiles with sizes of $m_n + r_n - 1$,
  and an overlap of $r_n - 1$ along the $n$--th dimension.
  Considering tiles at the same location from all the input images,
  tiles of size $m_n$ of the output images are computed using the
  formula above.

  The main savings in computation in both Winograd and FFT methods
  comes from the fact that both the kernel transforms $(W_{c,c'}
  \times_{n=1}^{N} \mathbb{G}_n)$, and image (tiles) transforms
  $(I_{b,c} \times_{n=1}^{N} \mathbb{B}_n)$ can be precomputed and
  reused many times.  The computation is dominated by computing the
  dot products -- accumulation of element--wise product inside the
  square brackets in Eqn.~\ref{eqn-layer-comp}.  Computing the results
  of all the dot products in Eqn.~\ref{eqn-layer-comp} is an
  equivalent problem to matrix multiplications, with real matrices for
  the case of Winograd and complex matrices for the case of Regular
  FFT convolution.

  \subsection{Gauss' Multiplication of Complex Numbers} \label{sub_sec_gauss}

  In the Regular FFT convolution, the computation is dominated by
  complex matrix multiplications, where each complex number pair
  multiplication requires 4 real multiplications, when computed
  directly, and 3 real multiplication when using Gauss' multiplication
  algorithm~\cite{maclaren1970art,lavin2016fast}.

  Using Gauss' multiplication algorithm, the product of two complex
  numbers $u_r+u_ii$ and $v_r+v_ii$ is computed by first computing
  $tmp_1 = v_r·(u_r+u_i)$, $tmp_2 = u_r·(v_i-v_r)$ and $tmp_3 =
  u_i·(v_r+v_i)$. The real part of the result then equals to $tmp_1 -
  tmp_3$ and the imaginary part to $tmp_1 + tmp_2$.  Similarly, an
  element--wise product of two complex tensors $U \odot V$ ($U = U_r +
  U_ii$ and $V = V_r + V_ii$) can be performed using three
  element--wise products of real--valued tensors.

  For the Regular FFT convlutional layer, element--wise product of
  complex tensors representing the image (tile) transforms and kernel
  transforms are performed, and each tensor is reused many times
  (Eqn.~\ref{eqn-layer-comp}).  After performing a transform of an
  image tile, that yields a complex tensor $U = U_r + U_ii$, a
  real--valued tensor $U_r + U_i$ can be computed and stored alongside
  $U_r$ and $U_i$.  Similarly, tensors $V_i-V_r$ and $V_r+V_i$ can be
  computed during kernel transforms and stored alongside $V_r$ ($V_i$
  does not have to be stored).  Each element--wise products of complex
  tensors can then be replaced with three independent element--wise
  products of real--valued tensors.

  The resulting three real tensors are implicitly converted back to a
  single complex tensors during the computation of inverse transform
  ($\stimes_{n=1}^{N} \mathbb{A}_n^T$).

  Computing all the dot products in Eqn.~\ref{eqn-layer-comp} is then
  performed using three real--valued matrix multiplications instead of
  a single complex matrix multiplication, reducing the number of
  required operations by 25\%.

  We refer the FFT method using Gauss's multiplication as Gauss--FFT
  ($\FFTGs(m,r)$)

  \subsection{Roofline Performance Model(s)} \label{sec:roofline}

  Performance models have been widely used to identify performance
  bottlenecks, motivate software optimizations, assess performance
  relative to machine capabilities, and more importantly, predict
  performances on different machines/architectures, which is exploited
  in this paper.

  Historically, many performance models were focused on latencies to
  predict performances.  However, modern hardware had introduced a
  number of latency--hiding techniques, including, but not limited to:
  out--of--order execution, data pre--fetching, and massive thread
  parallelism.  This effectively resulted in a shift from
  latency--limited computation to a throughput limited one.

  The Roofline model~\cite{williams2008roofline} is a throughput
  oriented performance model that tracks rates and not times. It is
  not restricted to ISA and architecture, hence it can be applied to
  CPUs, GPUs, TPUs (Tensor Processing Units), etc.

  The main idea behind the Roofline model~\cite{williams2008roofline}
  is the memory bandwidth will often be the constraining resource in
  system performance. Thus, it gives a performance bound as a function
  of arithmetic intensity (\AI), which is defined as the ratio of
  total floating--point operations (\FPO) to the total data movement
  (\DM) in bytes (\AI = \FPO/\DM). The performance ceiling (attainable
  FLOPS) is determined by:

  \begin{equation}
    \label{eq:roofline}
    \small
    \begin{aligned}
      &Attainable~ FLOPS = \min(\FLOPS, ~\MB \times  \AI)
    \end{aligned}
  \end{equation}

  Where \MB~means the peak memory bandwidth.  When plotted, the
  performance ceiling line resembles a roofline.

  The basic Roofline model~\cite{williams2008roofline} defines \DM~as
  data transferred between the main memory and the lowest level cache
  (L1), however the modern processors have multiple levels of memory:
  L1, L2, L3 and even L4.  To account for transfers between different
  levels of cache, a more sophisticated, Hierarchical Roofline
  model~\cite{williams2009roofline} was introduced, which defines
  multiple bandwidth ceilings and multiple {\AI}s, one for each level
  of the memory hierarchy.  The model represents a superposition of
  basic Roofline models, each with its own performance ceiling. The
  overall performance bound is limited to the minimum of each basic
  model.

  Cache--aware Roofline~\cite{ilic2014cache} uses a single \AI~
  defined as data movement between L1 and the register file, but
  defines multiple bandwidth ceilings.  While the basic, and
  Hierarchical Roofline models can be used for predicting performances
  of a certain algorithms with known cache--locality, the Cache--aware
  Roofline is used to determine the cache--locality of a particular
  implementation by observing performances.

  %In this paper, we introduce a new Roofline model, which is a hybrid
  %of the basic and the hierarchical model.  Both ours, and the
  %Hierarchical Roofline model are also ``cache aware'' in the sense
  %that they consider both the bandwidth and the size of each level of
  %caches, but unlike the Cache--aware Roofline, can be used for
  %predicting performances.
