#include "znn/fft3/propagation.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
#include "znn/direct_conv/direct.hpp"
#include <bitset>
#include <chrono>
#include <fstream>
#include <iostream>
#include <limits.h>
#include <string>

using namespace znn::fft3;
using znn::vek;

template <typename T1, typename T2>
void cast(T1* From, T2* To, long_t length)
{
    for (long_t i = 0; i < length; i++)
    {
        To[i] = static_cast<T2>(From[i]);
    }
}
template <long_t Cores, long_t B, long_t C1, long_t C2, long_t D, long_t H,
          long_t W, class t_size, class k_size>
void do_bench(const std::string out_file1)
{

    static constexpr long_t OD = D - k_size::value[0] + 1;
    static constexpr long_t OH = H - k_size::value[1] + 1;
    static constexpr long_t OW = W - k_size::value[2] + 1;

    static constexpr long_t RowBlock = 30;
   
   	using idim = vek<B, C1, D, H, W>;
    using odim = vek<B, C2, OD, OH, OW>;

    using istrides = vek<C1 * D * H * W, D * H * W * SIMD_WIDTH,
                         H * W * SIMD_WIDTH, W * SIMD_WIDTH, SIMD_WIDTH>;

    using ostrides = vek<C2 * OD * OH * OW, OD * OH * OW * SIMD_WIDTH,
                         OH * OW * SIMD_WIDTH, OW * SIMD_WIDTH, SIMD_WIDTH>;

    using layer =
        layer_t<idim, istrides, odim, ostrides, t_size, k_size, RowBlock>;

    using transform_t = propagation<Cores, 1, layer, true>;

    transform_t tt(true, true);

    long_t ker_memory =  C1 * C2 * k_size::value.prod();

//    hbw_array<float> a(one_init, B * C1 * D * H * W);
//    hbw_array<float> b(one_init, B * C2 * OD * OH * OW);
    hbw_array<float> buffer(one_init, transform_t::buffer_floats);
  //  hbw_array<float> ker(one_init, ker_memory);


    hbw_array<long double> dker(zero_init, ker_memory);
    //hbw_array<float>       fker(rand_init, ker_memory);
    hbw_array<float> fker(
        norm_init, 0.0f, std::sqrt(2.f / (C1 * k_size::value[0] *
                                          k_size::value[1] * k_size::value[2])),
        ker_memory);

    cast<float, long double>(fker.data(), dker.data(), ker_memory);


    // rand_init
    hbw_array<long double> da(rand_init, B * C1 * D * H * W);
    hbw_array<float>       fa(rand_init, B * C1 * D * H * W);
    cast<float, long double>(fa.data(), da.data(), B * C1 * D * H * W);

    hbw_array<float>       fb(zero_init, B * C2 * OD * OH * OW);
    hbw_array<long double> db(zero_init, B * C2 * OD * OH * OW);
   // hbw_array<float>       buffer(one_init, transform_t::buffer_floats);
    hbw_array<float>       test_out(zero_init, B * C2 * OD * OH * OW);
    // std::cout<< ker[0] <<std::endl;
    direct_conv<B, C1, D, H, W, C2, k_size::value[0], k_size::value[1],
                k_size::value[2], float>(fa.data(), fker.data(),
                                         test_out.data());
    tt.execute(fa.data(), fker.data(), fb.data(), buffer.data());

    direct_conv<B, C1, D, H, W, C2, k_size::value[0], k_size::value[1],
                k_size::value[2], long double>(da.data(), dker.data(),
                                               db.data());
    // test_result(b.data(), test_out.data(), B * C2 * oD * oH * oW);
    // test_accuracy(b.data(), test_out.data(), B * C2 * oD * oH * oW,
    // "result/vgg-c6-T2");
    // test_accuracy(b.data(), test_out.data(), B * C2 * oD * oH * oW,
    // out_file);
    std::string win_out = out_file1 + "-win";
    FILE*       f       = fopen(win_out.data(), "w");
    fwrite(fb.data(), sizeof(float), fb.num_elements(), f);
    fclose(f);

    std::string direct_f = out_file1 + "-direct_f";
    f                    = fopen(direct_f.data(), "w");
    fwrite(test_out.data(), sizeof(float), test_out.num_elements(), f);
    fclose(f);

    std::string direct_d = out_file1 + "-direct_d";
    f                    = fopen(direct_d.data(), "w");
    fwrite(db.data(), sizeof(long double), test_out.num_elements(), f);
    fclose(f);
}

inline constexpr long_t ceil_div(long_t a, long_t b) { return (a + b - 1) / b; }

inline constexpr long_t padded_size(long_t L, long_t T, long_t K)
{
	    return ceil_div(L - K + 1, T - K + 1) * (T - K + 1) + K - 1;
}

template <long_t Cores, long_t B, long_t C1, long_t C2, long_t D, long_t H,
          long_t W, long_t KD, long_t KH, long_t KW, long_t TD, long_t TH,
          long_t TW>
void bench(std::string const outfile)
{
    using t_size                    = vek<TD, TH, TW>;
    using k_size                    = vek<KD, KH, KW>;
    static constexpr long_t PADDED_D =
        padded_size(D, t_size::value[0], k_size::value[0]);
    static constexpr long_t PADDED_H =
        padded_size(H, t_size::value[1], k_size::value[1]);
    static constexpr long_t PADDED_W =
        padded_size(W, t_size::value[2], k_size::value[2]);

    std::cout << "accuracy checking of: " << B << ' ' << C1 << ' ' << C2 << ' ' << PADDED_D
              << ' ' << PADDED_H << ' ' << PADDED_W << std::endl;

    do_bench<Cores, B, C1, C2, PADDED_D, PADDED_H, PADDED_W, t_size, k_size>(
        outfile);
}


int main()
{
	bench<64, 1, 64, 64, 1, 226, 226, 1, 3, 3, 1, 64, 64>("uresult-3/vgg-block1-conv2-T64");
    bench<64, 1, 64, 128, 1, 114, 114, 1, 3, 3, 1, 64, 64>("uresult-3/vgg-block2-conv1-T64");
    bench<64, 1, 128, 128, 1, 114, 114, 1, 3, 3, 1, 64, 64>("uresult-3/vgg-block2-conv2-T64");
    bench<64, 1, 128, 256, 1, 58, 58, 1, 3, 3, 1, 64, 64>("uresult-3/vgg-block3-conv1-T64");
    bench<64, 1, 256, 256, 1, 58, 58, 1, 3, 3, 1, 64, 64>("uresult-3/vgg-block3-conv2-T64");


	//    bench<64, 1, 64, 128, 1, 114, 114, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block2-conv1-T16");
//    bench<64, 1, 64, 128, 1, 114, 114, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block2-conv1-T32");

//    bench<64, 1, 128, 256, 1, 58, 58, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block3-conv1-T16");
//    bench<64, 1, 128, 256, 1, 58, 58, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block3-conv1-T32");

   // bench<1, 2, 256, 256, 1, 2, 2, 1, 1, 1, 1, 2, 2>("uresult-3/test");
   //   bench<1, 1, 256, 256, 1, 58, 58, 1, 3, 3, 1, 16, 16>("uresult-3/test");
//    bench<64, 1, 256, 256, 1, 58, 58, 1, 3, 3, 1, 16, 16>("uresult-3/T-vgg-block3-conv2-T16");
//    bench<64, 1, 256, 256, 1, 58, 58, 1, 3, 3, 1, 32, 32>("uresult-3/T-vgg-block3-conv2-T32");

//    bench<64, 1, 256, 512, 1, 30, 30, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block4-conv1-T16");
//    bench<64, 1, 256, 512, 1, 30, 30, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block4-conv1-T32");
//
//    bench<64, 1, 512, 512, 1, 30, 30, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block4-conv2-T16");
//    bench<64, 1, 512, 512, 1, 30, 30, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block4-conv2-T32");
//
//	bench<64, 1, 64, 64, 1, 226, 226, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block1-conv2-T16");
//	bench<64, 1, 64, 64, 1, 226, 226, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block1-conv2-T32");
//
//    bench<64, 1, 128, 128, 1, 114, 114, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block2-conv2-T16");
//    bench<64, 1, 128, 128, 1, 114, 114, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block2-conv2-T32");
//
//    bench<64, 1, 512, 512, 1, 16, 16, 1, 3, 3, 1, 16, 16>("uresult-3/vgg-block5-conv1-T16");
//    bench<64, 1, 512, 512, 1, 16, 16, 1, 3, 3, 1, 32, 32>("uresult-3/vgg-block5-conv1-T32");

// C3d
  // bench<64, 1, 64, 128, 18, 58, 58, 3, 3, 3, 16, 16, 16>("uresult-3/c2a-T16");
  // bench<64, 1, 64, 128, 18, 58, 58, 3, 3, 3, 16, 32, 32>("uresult-3/c2a-T32");

  // bench<64, 1, 128, 256, 10, 30, 30, 3, 3, 3, 16, 16, 16>("uresult-3/c3a-T16");
  // bench<64, 1, 128, 256, 10, 30, 30, 3, 3, 3, 16, 32, 32>("uresult-3/c3a-T32");

  // bench<64, 1, 256, 256, 10, 30, 30, 3, 3, 3, 16, 16, 16>("uresult-3/c3b-T16");
  // bench<64, 1, 256, 256, 10, 30, 30, 3, 3, 3, 16, 32, 32>("uresult-3/c3b-T32");

  // bench<64, 1, 256, 512, 6, 16, 16, 3, 3, 3, 16, 16, 16>("uresult-3/c4a-T16");
  // //bench<64, 1, 256, 512, 6, 16, 16, 3, 3, 3, 32, 32, 32>("uresult/c4a-T32");

  // bench<64, 1, 512, 512, 6, 16, 16, 3, 3, 3, 16, 16, 16>("uresult-3/c4b-T16");
  // bench<64, 1, 512, 512, 6, 16, 16, 3, 3, 3, 32, 32, 32>("uresult/c4b-T32");

}
