(*
 * Copyright (c) 1997-1999 Massachusetts Institute of Technology
 * Copyright (c) 2003, 2007-14 Matteo Frigo
 * Copyright (c) 2003, 2007-14 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *)

open Util
open Genutil
open C


let usage = "Usage: " ^ Sys.argv.(0) ^ " -n <number>"

let urs = ref Stride_variable
let ucs = ref Stride_variable
let uivs = ref Stride_variable
let uovs = ref Stride_variable
let dftIII_flag = ref false

let skip_k = ref 0

let speclist = [
  "-with-rs",
  Arg.String(fun x -> urs := arg_to_stride x),
  " specialize for given real-array stride";

  "-with-cs",
  Arg.String(fun x -> ucs := arg_to_stride x),
  " specialize for given complex-array real stride";

  "-with-ivs",
  Arg.String(fun x -> uivs := arg_to_stride x),
  " specialize for given input vector stride";

  "-with-ovs",
  Arg.String(fun x -> uovs := arg_to_stride x),
  " specialize for given output vector stride";

  "-skip-k",
  Arg.Int(fun x -> skip_k := x),
  " skips first k outputs";

  "-dft-III",
  Arg.Unit(fun () -> dftIII_flag := true),
  " produce shifted dftIII-style codelets"
]

let hcdftIII sign n input =
  let input' i =
    if (i mod 2 == 0) then
      Complex.zero
    else
      let i' = (i - 1) / 2 in
      if (2 * i' < n - 1) then (input i')
      else if (2 * i' == n - 1) then
	Complex.real (input i')
      else
	Complex.conj (input (n - 1 - i'))
  in Fft.dft sign (2 * n) input'

let generate n =
  let ar0 = "R0" and ar1 = "R1" and acr = "Cr" and aci = "Ci"
      and rs = "rs" and cs = "cs"
      and i = "i" and v = "v"
      and transform = if !dftIII_flag then hcdftIII else Trig.hdft
  in

  let sign = !Genutil.sign
  and name = !Magic.codelet_name in

  let vrs = either_stride (!urs) (C.SVar rs)
  and vcs = either_stride (!ucs) (C.SVar cs)
  in

  let sovs = stride_to_string "ovs" !uovs in
  let sivs = stride_to_string "ivs" !uivs in

  let locations = unique_array_c n in
  let input =
    locative_array_c n
                     (C.array_subscript acr vcs)
                     (C.array_subscript aci vcs)
                     locations sivs in
  let output = transform sign n (load_array_hc n input) in
  let oloc =
    locative_array_c n
                     (C.array_subscript ar0 vrs)
                     (C.array_subscript "BUG" vrs)
                     locations sovs
  in
  let odag = store_array_r_skip n !skip_k oloc output in
  let annot = standard_optimizer odag in

  let body = Asch annot
  in

  let template_type = Printf.sprintf "template <long_t TransformSize, long_t SkippedOutputs, long_t rs, long_t cs>\ninline __attribute__((always_inline)) typename std::enable_if<TransformSize == %d && SkippedOutputs == %d>::type\n" n !skip_k
  in

  let tree =
    Fcn ((if !Magic.standalone then template_type else "static void"), name,
         ([Decl (C.realtypep, "__restrict R0");
	   Decl (C.constrealtypep, "__restrict Cr");
	   Decl (C.constrealtypep, "__restrict Ci")]),
	 finalize_fcn body)

  in let zzzz = arith_complexity_constants name n !skip_k tree

  in let desc =
    Printf.sprintf
      "static const kr2c_desc desc = { %d, \"%s\", %s, &GENUS };\n\n"
      n name (flops_of tree)

  and init =
    (declare_register_fcn name) ^
    "{" ^
    "  X(kr2c_register)(p, " ^ name ^ ", &desc);\n" ^
    "}\n"

  in
  zzzz ^ (unparse tree) ^ "\n" ^ (if !Magic.standalone then "" else desc ^ init)


let main () =
  begin
    parse speclist usage;
    print_string (generate (check_size ()));
  end

let _ = main()
