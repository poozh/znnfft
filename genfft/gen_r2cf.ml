(*
 * Copyright (c) 1997-1999 Massachusetts Institute of Technology
 * Copyright (c) 2003, 2007-14 Matteo Frigo
 * Copyright (c) 2003, 2007-14 Massachusetts Institute of Technology
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *)

open Util
open Genutil
open C


let usage = "Usage: " ^ Sys.argv.(0) ^ " -n <number>"

let urs = ref Stride_variable
let ucs = ref Stride_variable
let uivs = ref Stride_variable
let uovs = ref Stride_variable
let dftII_flag = ref false

let first_k = ref 0

let speclist = [
  "-with-rs",
  Arg.String(fun x -> urs := arg_to_stride x),
  " specialize for given real-array stride";

  "-with-cs",
  Arg.String(fun x -> ucs := arg_to_stride x),
  " specialize for given complex-array stride";

  "-first-k",
  Arg.Int(fun x -> first_k := x),
  " specialize for given output vector stride";

  "-with-ivs",
  Arg.String(fun x -> uivs := arg_to_stride x),
  " specialize for given input vector stride";

  "-with-ovs",
  Arg.String(fun x -> uovs := arg_to_stride x),
  " specialize for given output vector stride";

  "-dft-II",
  Arg.Unit(fun () -> dftII_flag := true),
  " produce shifted dftII-style codelets"
]


let rdftII sign n input =
  let input' i = if i < n then input i else Complex.zero in
  let f = Fft.dft sign (2 * n) input' in
  let g i = f (2 * i + 1)
  in fun i ->
    if (i < n - i) then g i
    else if (2 * i + 1 == n) then Complex.real (g i)
    else Complex.zero

let generate n =
  let ar0 = "R0" and ar1 = "R1" and acr = "Cr" and aci = "Ci"
  and rs = "rs" and cs = "cs"
  and v = "v"
  and transform = if !dftII_flag then rdftII else Trig.rdft
  in

  let sign = !Genutil.sign
  and name = !Magic.codelet_name in

  let vrs = either_stride (!urs) (C.SVar rs)
  and vcs = either_stride (!ucs) (C.SVar cs)
  in

  let sovs = stride_to_string "ovs" !uovs in
  let sivs = stride_to_string "ivs" !uivs in

  let locations = unique_array_c n in
  let preinput =
    locative_array_c n
      (C.array_subscript ar0 vrs)
      (C.array_subscript "BUG" vrs)
      locations sivs
  in
  let loaded_input =
    load_array_r n preinput in

  let input ix =
    if ix < !first_k then loaded_input ix else Complex.zero
  in


  let output = transform sign n input in
  let oloc =
    locative_array_c n
      (C.array_subscript acr vcs)
      (C.array_subscript aci vcs)
      locations sovs in
  let odag = store_array_hc n oloc output in
  let annot = standard_optimizer odag in

  let body = Asch annot
  in


  let template_type = Printf.sprintf "template <long_t TransformSize, long_t ProvidedElements, long_t rs, long_t cs>\ninline __attribute__((always_inline)) typename std::enable_if<TransformSize == %d && ProvidedElements == %d>::type\n" n !first_k
  in

  let tree =
    Fcn ((if !Magic.standalone then template_type else "static void"), name,
	 ([Decl (C.constrealtypep, "__restrict R0");
	   Decl (C.realtypep, "__restrict Cr");
	   Decl (C.realtypep, "__restrict Ci")]),
	 finalize_fcn body)

  in let zzzz = arith_complexity_constants name n !first_k tree

  in let desc =
    Printf.sprintf
      "static const kr2c_desc desc = { %d, \"%s\", %s, &GENUS };\n\n"
      n name (flops_of tree)

  and init =
    (declare_register_fcn name) ^
    "{" ^
    "  X(kr2c_register)(p, " ^ name ^ ", &desc);\n" ^
    "}\n"

  in
  zzzz ^ (unparse tree) ^ "\n" ^ (if !Magic.standalone then "" else desc ^ init)


let main () =
  begin
    parse speclist usage;
    print_string (generate (check_size ()));
  end

let _ = main()
