#!/bin/bash

rm -rf all_gen;
mkdir all_gen;
mkdir all_gen/detail

echo "#pragma once" > all_gen/codelets.hpp

for n in $(seq 2 32; seq 64 64)
do
    echo "#pragma once" >  ./all_gen/detail/_r2cf_$n.hpp;
    echo "#pragma once" >  ./all_gen/detail/_c2cf_$n.hpp;

    for k in $(seq 1 $n)
    do
        ./gen_r2cf.native  -fma -reorder-insns -schedule-for-pipeline -compact -variables 30 -pipeline-latency 12 -n $n -first-k $k -name r2cf -standalone >> ./all_gen/detail/_r2cf_$n.hpp
        ./gen_notwf.native -fma -reorder-insns -schedule-for-pipeline -compact -variables 30 -pipeline-latency 12 -n $n -first-k $k -name c2cf -standalone >> ./all_gen/detail/_c2cf_$n.hpp
    done

    echo "#pragma once" >  ./all_gen/detail/_r2cb_$n.hpp;
    echo "#pragma once" >  ./all_gen/detail/_c2cb_$n.hpp;

    for k in $(seq 0 $(perl -e "print $n-1"))
    do
        ./gen_r2cb.native  -fma -reorder-insns -schedule-for-pipeline -compact -variables 30 -pipeline-latency 12 -sign 1 -n $n -skip-k $k -name r2cb -standalone >> ./all_gen/detail/_r2cb_$n.hpp
        ./gen_notwb.native -fma -reorder-insns -schedule-for-pipeline -compact -variables 30 -pipeline-latency 12 -n $n -skip-k $k -name c2cb -standalone >> ./all_gen/detail/_c2cb_$n.hpp
    done

    echo "\n// Size $n" >> all_gen/codelets.hpp
    echo "#include \"detail/r2cf_$n.hpp\"" >> all_gen/codelets.hpp
    echo "#include \"detail/r2cb_$n.hpp\"" >> all_gen/codelets.hpp
    echo "#include \"detail/c2cf_$n.hpp\"" >> all_gen/codelets.hpp
    echo "#include \"detail/c2cb_$n.hpp\"" >> all_gen/codelets.hpp

    clang-format all_gen/detail/_r2cf_$n.hpp > all_gen/detail/r2cf_$n.hpp
    clang-format all_gen/detail/_r2cb_$n.hpp > all_gen/detail/r2cb_$n.hpp
    clang-format all_gen/detail/_c2cf_$n.hpp > all_gen/detail/c2cf_$n.hpp
    clang-format all_gen/detail/_c2cb_$n.hpp > all_gen/detail/c2cb_$n.hpp

    rm -rf all_gen/detail/_r2cf_$n.hpp
    rm -rf all_gen/detail/_r2cb_$n.hpp
    rm -rf all_gen/detail/_c2cf_$n.hpp
    rm -rf all_gen/detail/_c2cb_$n.hpp
done
