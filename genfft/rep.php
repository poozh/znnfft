<?php
$all = "";

for ( $dim = 2; $dim <= 32; $dim++ )
{

    $string = file_get_contents('./all_gen/r2c_' . $dim . '.cpp');

    $fn_pattern = 'void r2c(R * R0, R * R1, R * Cr, R * Ci, stride rs, stride csr, stride csi, INT v, INT ivs, INT ovs)';

    $fn_replace = 'template <long_t Size, long_t rs, long_t cs>
inline __attribute__((always_inline)) typename std::enable_if<Size == '.$dim.'>::type
FFTr2c(SIMD_FLOAT const* __restrict R0, SIMD_FLOAT* __restrict Cr,
       SIMD_FLOAT* __restrict Ci)';

    $string = str_replace($fn_pattern, $fn_replace, $string);
    $string = str_replace("INT i;\n", "\n", $string);
    $string = preg_replace("/for(.*?)\)\)/", "", $string);
    $string = str_replace("\nE ", "\nSIMD_FLOAT ", $string);
    $string = str_replace("FMA(", "SIMD_FMADD(", $string);
    $string = str_replace("FMS(", "SIMD_FMSUB(", $string);
    $string = str_replace("FNMS(", "SIMD_FNMADD(", $string);
    $string = str_replace("R0[WS(rs, ", "R0[WS(rs, 2*", $string);
    $string = str_replace("R1[WS(rs, ", "R0[WS(rs, 1+2*", $string);
    $string = str_replace("R1[0]", "R0[WS(rs, 1)]", $string);
    $string = str_replace("(csr", "(cs", $string);
    $string = str_replace("(csi", "(cs", $string);

    $all = $all . "\n\n" . $string;

    $string = file_get_contents('./all_gen/c2r_' . $dim . '.cpp');

    $fn_pattern = 'void c2r(R * R0, R * R1, R * Cr, R * Ci, stride rs, stride csr, stride csi, INT v, INT ivs, INT ovs)';

    $fn_replace = 'template <long_t Size, long_t rs, long_t cs>
inline __attribute__((always_inline)) typename std::enable_if<Size == '.$dim.'>::type
FFTc2r(SIMD_FLOAT* __restrict R0, SIMD_FLOAT const* __restrict Cr,
       SIMD_FLOAT const* __restrict Ci)';

    $string = str_replace($fn_pattern, $fn_replace, $string);
    $string = str_replace("INT i;\n", "\n", $string);
    $string = preg_replace("/for(.*?)\)\)/", "", $string);
    $string = str_replace("\nE ", "\nSIMD_FLOAT ", $string);
    $string = str_replace("FMA(", "SIMD_FMADD(", $string);
    $string = str_replace("FMS(", "SIMD_FMSUB(", $string);
    $string = str_replace("FNMS(", "SIMD_FNMADD(", $string);
    $string = str_replace("R0[WS(rs, ", "R0[WS(rs, 2*", $string);
    $string = str_replace("R1[WS(rs, ", "R0[WS(rs, 1+2*", $string);
    $string = str_replace("R1[0]", "R0[WS(rs, 1)]", $string);
    $string = str_replace("(csr", "(cs", $string);
    $string = str_replace("(csi", "(cs", $string);

    $all = $all . "\n\n" . $string;

    $string = file_get_contents('./all_gen/c2c_' . $dim . '.cpp');

    $fn_pattern = 'void c2c(const R * ri, const R * ii, R * ro, R * io, stride is, stride os, INT v, INT ivs, INT ovs)';

    $fn_replace = 'template <long_t Size, long_t alls>
inline __attribute__((always_inline)) typename std::enable_if<Size == '.$dim.'>::type
FFTc2c(SIMD_FLOAT* __restrict ro, SIMD_FLOAT* __restrict io)';

    $string = str_replace($fn_pattern, $fn_replace, $string);
    $string = str_replace("INT i;\n", "\n", $string);
    $string = preg_replace("/for(.*?)\)\)/", "", $string);
    $string = str_replace("\nE ", "\nSIMD_FLOAT ", $string);
    $string = str_replace("FMA(", "SIMD_FMADD(", $string);
    $string = str_replace("FMS(", "SIMD_FMSUB(", $string);
    $string = str_replace("FNMS(", "SIMD_FNMADD(", $string);
    $string = str_replace("ri[", "ro[", $string);
    $string = str_replace("ii[", "io[", $string);
    $string = str_replace("(is", "(alls", $string);
    $string = str_replace("(os", "(alls", $string);

    $all = $all . "\n\n" . $string;

}

print $all . "\n";
